<GameFile>
  <PropertyGroup Name="AttrView" Type="Node" ID="03e2f14a-60e0-42ce-9756-6726ab869cb3" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="25" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Grp_AttrView" ActionTag="1550793001" Tag="57" IconVisible="False" LeftMargin="-100.0000" RightMargin="-100.0000" TopMargin="-100.0000" BottomMargin="-100.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="-33" Scale9OriginY="-33" Scale9Width="66" Scale9Height="66" ctype="PanelObjectData">
            <Size X="200.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="-1663193596" Tag="110" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" TopMargin="5.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="200.0000" Y="195.0000" />
                <AnchorPoint ScaleX="0.5000" />
                <Position X="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" />
                <PreSize X="1.0000" Y="0.9750" />
                <FileData Type="Normal" Path="res/com/img/grey_panel.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="name" ActionTag="1061754792" Tag="58" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="9.2453" RightMargin="154.7547" TopMargin="9.3856" BottomMargin="154.6144" FontSize="28" LabelText="n:  " OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="36.0000" Y="36.0000" />
                <Children>
                  <AbstractNodeData Name="value" ActionTag="827567345" Tag="59" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="40.8672" RightMargin="-100.8672" FontSize="28" LabelText="npc_1" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="96.0000" Y="36.0000" />
                    <AnchorPoint ScaleY="1.0000" />
                    <Position X="40.8672" Y="36.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.1352" Y="1.0000" />
                    <PreSize X="2.6667" Y="1.0000" />
                    <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                    <OutlineColor A="255" R="0" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="9.2453" Y="172.6144" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0462" Y="0.8631" />
                <PreSize X="0.1800" Y="0.1800" />
                <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="state" ActionTag="-1815839661" Tag="60" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="9.2454" RightMargin="154.7546" TopMargin="31.0013" BottomMargin="132.9987" FontSize="28" LabelText="s:  " OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="36.0000" Y="36.0000" />
                <Children>
                  <AbstractNodeData Name="value" ActionTag="2076929317" Tag="61" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="40.8672" RightMargin="-86.8672" TopMargin="-0.0001" BottomMargin="0.0001" FontSize="28" LabelText="work" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="82.0000" Y="36.0000" />
                    <AnchorPoint ScaleY="1.0000" />
                    <Position X="40.8672" Y="36.0001" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.1352" Y="1.0000" />
                    <PreSize X="2.2778" Y="1.0000" />
                    <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                    <OutlineColor A="255" R="0" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="9.2454" Y="150.9987" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0462" Y="0.7550" />
                <PreSize X="0.1800" Y="0.1800" />
                <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="money" ActionTag="-961478182" Tag="101" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="9.2454" RightMargin="151.7546" TopMargin="52.6172" BottomMargin="111.3828" FontSize="28" LabelText="$:  " OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="39.0000" Y="36.0000" />
                <Children>
                  <AbstractNodeData Name="value" ActionTag="-400930060" Tag="102" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="40.8681" RightMargin="-180.8681" FontSize="28" LabelText="999999999" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="179.0000" Y="36.0000" />
                    <AnchorPoint ScaleY="1.0000" />
                    <Position X="40.8681" Y="36.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.0479" Y="1.0000" />
                    <PreSize X="4.5897" Y="1.0000" />
                    <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                    <OutlineColor A="255" R="0" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="9.2454" Y="129.3828" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0462" Y="0.6469" />
                <PreSize X="0.1950" Y="0.1800" />
                <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="mentality" ActionTag="1574417469" Tag="104" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="9.2454" RightMargin="151.7546" TopMargin="79.5754" BottomMargin="84.4246" FontSize="28" LabelText="M:  " OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="39.0000" Y="36.0000" />
                <Children>
                  <AbstractNodeData Name="Bg" ActionTag="1267770720" Tag="62" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="44.2728" RightMargin="-200.2728" TopMargin="-5.4992" BottomMargin="-7.5008" LeftEage="15" RightEage="15" TopEage="16" BottomEage="16" Scale9OriginX="15" Scale9OriginY="16" Scale9Width="165" Scale9Height="17" ctype="ImageViewObjectData">
                    <Size X="195.0000" Y="49.0000" />
                    <Children>
                      <AbstractNodeData Name="Lbr_Value" ActionTag="1458293512" Tag="63" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="2.5000" RightMargin="2.5000" TopMargin="2.0000" BottomMargin="2.0000" ProgressInfo="100" ctype="LoadingBarObjectData">
                        <Size X="190.0000" Y="45.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="97.5000" Y="24.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.9744" Y="0.9184" />
                        <ImageFileData Type="Normal" Path="res/Game/CardAndTown/com/img/com_blue.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Txt_Percent" ActionTag="-387143203" Tag="64" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="56.5000" RightMargin="56.5000" TopMargin="3.5000" BottomMargin="3.5000" FontSize="34" LabelText="100%" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="82.0000" Y="42.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="97.5000" Y="24.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.4205" Y="0.8571" />
                        <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                        <OutlineColor A="255" R="0" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="44.2728" Y="16.9992" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.1352" Y="0.4722" />
                    <PreSize X="5.0000" Y="1.3611" />
                    <FileData Type="Normal" Path="res/Game/CardAndTown/com/img/com_gray.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="9.2454" Y="102.4246" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0462" Y="0.5121" />
                <PreSize X="0.1950" Y="0.1800" />
                <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="food" ActionTag="1937024695" Tag="106" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="9.2454" RightMargin="154.7546" TopMargin="116.6357" BottomMargin="47.3643" FontSize="28" LabelText="F:  " OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="36.0000" Y="36.0000" />
                <Children>
                  <AbstractNodeData Name="Bg" ActionTag="1410126052" Tag="65" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="40.8672" RightMargin="-199.8672" TopMargin="-5.4992" BottomMargin="-7.5008" LeftEage="15" RightEage="15" TopEage="16" BottomEage="16" Scale9OriginX="15" Scale9OriginY="16" Scale9Width="165" Scale9Height="17" ctype="ImageViewObjectData">
                    <Size X="195.0000" Y="49.0000" />
                    <Children>
                      <AbstractNodeData Name="Lbr_Value" ActionTag="-719153022" Tag="66" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="2.5000" RightMargin="2.5000" TopMargin="2.0000" BottomMargin="2.0000" ProgressInfo="100" ctype="LoadingBarObjectData">
                        <Size X="190.0000" Y="45.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="97.5000" Y="24.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.9744" Y="0.9184" />
                        <ImageFileData Type="Normal" Path="res/Game/CardAndTown/com/img/com_red.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Txt_Percent" ActionTag="-1397014617" Tag="67" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="56.5000" RightMargin="56.5000" TopMargin="3.5000" BottomMargin="3.5000" FontSize="34" LabelText="100%" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="82.0000" Y="42.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="97.5000" Y="24.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.4205" Y="0.8571" />
                        <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                        <OutlineColor A="255" R="0" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="40.8672" Y="16.9992" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.1352" Y="0.4722" />
                    <PreSize X="5.4167" Y="1.3611" />
                    <FileData Type="Normal" Path="res/Game/CardAndTown/com/img/com_gray.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="9.2454" Y="65.3643" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0462" Y="0.3268" />
                <PreSize X="0.1800" Y="0.1800" />
                <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="stamina" ActionTag="-970048334" Tag="108" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="9.2500" RightMargin="154.7500" TopMargin="153.6962" BottomMargin="10.3038" FontSize="28" LabelText="S:  " OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="36.0000" Y="36.0000" />
                <Children>
                  <AbstractNodeData Name="Bg" ActionTag="214907014" Tag="68" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="40.8672" RightMargin="-199.8672" TopMargin="-5.4992" BottomMargin="-7.5008" LeftEage="15" RightEage="15" TopEage="16" BottomEage="16" Scale9OriginX="15" Scale9OriginY="16" Scale9Width="165" Scale9Height="17" ctype="ImageViewObjectData">
                    <Size X="195.0000" Y="49.0000" />
                    <Children>
                      <AbstractNodeData Name="Lbr_Value" ActionTag="-903226973" Tag="69" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="2.5000" RightMargin="2.5000" TopMargin="2.0000" BottomMargin="2.0000" ProgressInfo="100" ctype="LoadingBarObjectData">
                        <Size X="190.0000" Y="45.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="97.5000" Y="24.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.9744" Y="0.9184" />
                        <ImageFileData Type="Normal" Path="res/Game/CardAndTown/com/img/com_yellow.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Txt_Percent" ActionTag="-2071054998" Tag="70" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="56.5000" RightMargin="56.5000" TopMargin="3.5000" BottomMargin="3.5000" FontSize="34" LabelText="100%" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="82.0000" Y="42.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="97.5000" Y="24.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.4205" Y="0.8571" />
                        <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                        <OutlineColor A="255" R="0" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="40.8672" Y="16.9992" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.1352" Y="0.4722" />
                    <PreSize X="5.4167" Y="1.3611" />
                    <FileData Type="Normal" Path="res/Game/CardAndTown/com/img/com_gray.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="9.2500" Y="28.3038" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0463" Y="0.1415" />
                <PreSize X="0.1800" Y="0.1800" />
                <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Btn_Info" ActionTag="1789876491" Tag="26" IconVisible="False" LeftMargin="122.0000" RightMargin="16.0000" TopMargin="20.0000" BottomMargin="142.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="27" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="62.0000" Y="38.0000" />
                <Children>
                  <AbstractNodeData Name="info" ActionTag="186014064" Tag="27" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="6.5000" RightMargin="6.5000" TopMargin="4.6000" BottomMargin="8.4000" FontSize="20" LabelText="Info" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="49.0000" Y="25.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="31.0000" Y="20.9000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5500" />
                    <PreSize X="0.7903" Y="0.6579" />
                    <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                    <OutlineColor A="255" R="0" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="184.0000" Y="161.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9200" Y="0.8050" />
                <PreSize X="0.3100" Y="0.1900" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_click.png" Plist="" />
                <PressedFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_click.png" Plist="" />
                <NormalFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>