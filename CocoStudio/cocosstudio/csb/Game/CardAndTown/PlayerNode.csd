<GameFile>
  <PropertyGroup Name="PlayerNode" Type="Node" ID="9c41e936-3090-4ef8-bf42-83e8043e20f6" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="0.5000" />
      <ObjectData Name="Node" Tag="122" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Grp_View" ActionTag="-1798955161" Tag="123" IconVisible="False" LeftMargin="-8.0000" RightMargin="-8.0000" TopMargin="-8.0000" BottomMargin="-8.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="16.0000" Y="16.0000" />
            <Children>
              <AbstractNodeData Name="Grp_ColliderBox" ActionTag="-120338458" Tag="166" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftEage="5" RightEage="5" TopEage="5" BottomEage="5" Scale9OriginX="5" Scale9OriginY="5" Scale9Width="6" Scale9Height="6" ctype="ImageViewObjectData">
                <Size X="16.0000" Y="16.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="8.0000" Y="8.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="res/Game/CardAndTown/com/img/ImgbuildHide.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="bodyAni" ActionTag="-235388801" Tag="21" IconVisible="False" TopMargin="-8.0000" BottomMargin="8.0000" ctype="SpriteObjectData">
                <Size X="16.0000" Y="16.0000" />
                <AnchorPoint ScaleX="0.5000" />
                <Position X="8.0000" Y="8.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="7.0000" Y="4.0000" />
                <FileData Type="MarkedSubImage" Path="res/Game/CardAndTown/player/1/move_2_2.png" Plist="plist/Game/CardAndTown/character/temp_1.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Img_Name" ActionTag="641216948" Tag="126" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-16.9998" RightMargin="-17.0002" TopMargin="-45.6585" BottomMargin="41.6585" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="50.0000" Y="20.0000" />
                <Children>
                  <AbstractNodeData Name="value" ActionTag="-668228730" Tag="125" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-11.0000" RightMargin="-11.0000" TopMargin="-11.5000" BottomMargin="-11.5000" FontSize="36" LabelText="NPC" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="72.0000" Y="43.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="25.0000" Y="10.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.4400" Y="2.1500" />
                    <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                    <OutlineColor A="255" R="146" G="146" B="146" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" />
                <Position X="8.0002" Y="41.6585" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="2.6037" />
                <PreSize X="3.1250" Y="1.2500" />
                <FileData Type="Normal" Path="res/com/img/grey_panel.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>