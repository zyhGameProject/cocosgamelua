<GameFile>
  <PropertyGroup Name="GameScene" Type="Scene" ID="dc5a03db-324c-4b46-8787-20509b02bb9d" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="16" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1822817564" Tag="24" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" TouchEnable="True" StretchWidthEnable="True" StretchHeightEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="-33" Scale9OriginY="-33" Scale9Width="66" Scale9Height="66" ctype="PanelObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Grp_View" ActionTag="-1243525915" Tag="17" IconVisible="False" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" TouchEnable="True" StretchWidthEnable="True" StretchHeightEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="960.0000" Y="640.0000" />
            <Children>
              <AbstractNodeData Name="KW_GRP_MAP" ActionTag="397155125" Tag="113" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="960.0000" Y="640.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="KW_GRP_Objects" ActionTag="723476038" Tag="127" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="960.0000" Y="640.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="KW_GRP_BuildNode" ActionTag="-163718659" Tag="38" IconVisible="False" LeftMargin="-564.5149" RightMargin="1476.5149" TopMargin="421.9589" BottomMargin="170.0411" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="48.0000" Y="48.0000" />
                <Children>
                  <AbstractNodeData Name="show" ActionTag="-1080563913" Tag="47" IconVisible="False" ClipAble="True" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="48.0000" Y="48.0000" />
                    <AnchorPoint ScaleX="0.5000" />
                    <Position X="24.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" />
                    <PreSize X="1.0000" Y="1.0000" />
                    <SingleColor A="255" R="0" G="255" B="0" />
                    <FirstColor A="255" R="0" G="255" B="18" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="img" ActionTag="905012723" Alpha="102" Tag="39" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" TopMargin="-48.0000" LeftEage="15" RightEage="15" TopEage="31" BottomEage="31" Scale9OriginX="15" Scale9OriginY="31" Scale9Width="18" Scale9Height="34" ctype="ImageViewObjectData">
                    <Size X="48.0000" Y="96.0000" />
                    <AnchorPoint ScaleX="0.5000" />
                    <Position X="24.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" />
                    <PreSize X="1.0000" Y="2.0000" />
                    <FileData Type="MarkedSubImage" Path="res/Game/CardAndTown/build/base/build_1.png" Plist="plist/Game/CardAndTown/build/build_base.plist" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-540.5149" Y="194.0411" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="-0.5630" Y="0.3032" />
                <PreSize X="0.0500" Y="0.0750" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="KW_BTN_BuildList" ActionTag="-855546850" CallBackType="Click" CallBackName="OnBtnUpdateBuildList" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" LeftMargin="25.0000" RightMargin="886.0000" TopMargin="566.0000" BottomMargin="25.0000" TouchEnable="True" FontSize="40" ButtonText="B" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="19" Scale9Height="27" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="49.0000" Y="49.0000" />
                <Children>
                  <AbstractNodeData Name="KW_GRP_BuildView" ActionTag="-1927989820" Tag="34" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="76.0000" RightMargin="-827.0000" TopMargin="-15.5000" BottomMargin="-15.5000" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="64" RightEage="64" TopEage="16" BottomEage="16" Scale9OriginX="-64" Scale9OriginY="-16" Scale9Width="128" Scale9Height="32" ctype="PanelObjectData">
                    <Size X="800.0000" Y="80.0000" />
                    <Children>
                      <AbstractNodeData Name="KW_GRP_BuildItem" ActionTag="1205296144" CallBackType="Click" CallBackName="OnBtnSelectBuild" Tag="36" IconVisible="False" LeftMargin="-376.7558" RightMargin="1111.7559" TopMargin="40.1196" BottomMargin="-25.1196" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="-33" Scale9OriginY="-33" Scale9Width="66" Scale9Height="66" ctype="PanelObjectData">
                        <Size X="65.0000" Y="65.0000" />
                        <Children>
                          <AbstractNodeData Name="bg" ActionTag="705322440" Tag="49" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="3.2500" RightMargin="3.2500" TopMargin="3.2500" BottomMargin="3.2500" LeftEage="16" RightEage="16" TopEage="16" BottomEage="16" Scale9OriginX="16" Scale9OriginY="16" Scale9Width="17" Scale9Height="17" ctype="ImageViewObjectData">
                            <Size X="58.5000" Y="58.5000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="32.5000" Y="32.5000" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.5000" Y="0.5000" />
                            <PreSize X="0.9000" Y="0.9000" />
                            <FileData Type="Normal" Path="res/Game/CardAndTown/com/img/red_button04.png" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="icon" ActionTag="216086954" Tag="37" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-15.5000" RightMargin="-15.5000" TopMargin="-18.7500" BottomMargin="-12.2500" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="66" Scale9Height="66" ctype="ImageViewObjectData">
                            <Size X="96.0000" Y="96.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="32.5000" Y="35.7500" />
                            <Scale ScaleX="0.4500" ScaleY="0.4500" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.5000" Y="0.5500" />
                            <PreSize X="1.4769" Y="1.4769" />
                            <FileData Type="MarkedSubImage" Path="res/Game/CardAndTown/build/base/build_3.png" Plist="plist/Game/CardAndTown/build/build_base.plist" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="-344.2558" Y="7.3804" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="-0.4303" Y="0.0923" />
                        <PreSize X="0.0812" Y="0.8125" />
                        <SingleColor A="255" R="150" G="200" B="255" />
                        <FirstColor A="255" R="150" G="200" B="255" />
                        <EndColor A="255" R="255" G="255" B="255" />
                        <ColorVector ScaleY="1.0000" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Lvw_BuildList" ActionTag="-637362791" Tag="35" IconVisible="False" PercentHeightEnable="True" PercentHeightEnabled="True" LeftMargin="-800.0000" RightMargin="800.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="62" RightEage="62" TopEage="14" BottomEage="14" Scale9OriginX="62" Scale9OriginY="14" Scale9Width="66" Scale9Height="17" ScrollDirectionType="0" VerticalType="Align_VerticalCenter" ctype="ListViewObjectData">
                        <Size X="800.0000" Y="80.0000" />
                        <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                        <Position Y="40.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition Y="0.5000" />
                        <PreSize X="1.0000" Y="1.0000" />
                        <FileData Type="Normal" Path="res/Game/CardAndTown/com/img/com_red.png" Plist="" />
                        <SingleColor A="255" R="150" G="150" B="255" />
                        <FirstColor A="255" R="150" G="150" B="255" />
                        <EndColor A="255" R="255" G="255" B="255" />
                        <ColorVector ScaleY="1.0000" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="76.0000" Y="24.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.5510" Y="0.5000" />
                    <PreSize X="16.3265" Y="1.6327" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="49.5000" Y="49.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0516" Y="0.0773" />
                <PreSize X="0.0510" Y="0.0766" />
                <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Normal" Path="res/Game/CardAndTown/com/img/red_button05.png" Plist="" />
                <PressedFileData Type="Normal" Path="res/Game/CardAndTown/com/img/red_button05.png" Plist="" />
                <NormalFileData Type="Normal" Path="res/Game/CardAndTown/com/img/red_button04.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Img_TimeBg" ActionTag="-1692106523" Tag="111" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="435.0000" RightMargin="435.0000" BottomMargin="600.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="90.0000" Y="40.0000" />
                <Children>
                  <AbstractNodeData Name="KW_TXT_Time" ActionTag="1586923644" Tag="112" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-0.5000" RightMargin="-0.5000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="30" LabelText="24:00" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="91.0000" Y="34.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="45.0000" Y="20.0000" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.0111" Y="0.8500" />
                    <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="255" B="255" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="480.0000" Y="640.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.0000" />
                <PreSize X="0.0938" Y="0.0625" />
                <FileData Type="Normal" Path="res/com/img/grey_panel.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="KW_GRP_ClickCamera" ActionTag="-1339954407" VisibleForFrame="False" Tag="88" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="240.0000" RightMargin="240.0000" TopMargin="160.0000" BottomMargin="160.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="480.0000" Y="320.0000" />
                <Children>
                  <AbstractNodeData Name="KW_BTN_CameraBack" ActionTag="-566532160" CallBackType="Click" CallBackName="OnBtnCameraBack" Tag="112" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" RightMargin="400.0000" BottomMargin="280.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="27" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="80.0000" Y="40.0000" />
                    <Children>
                      <AbstractNodeData Name="value" ActionTag="308626910" Tag="113" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="12.0000" RightMargin="12.0000" TopMargin="7.5000" BottomMargin="7.5000" FontSize="20" LabelText="back" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="56.0000" Y="25.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="40.0000" Y="20.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.7000" Y="0.6250" />
                        <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                        <OutlineColor A="255" R="0" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleY="1.0000" />
                    <Position Y="320.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="1.0000" />
                    <PreSize X="0.1667" Y="0.1250" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_normal.png" Plist="" />
                    <PressedFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_click.png" Plist="" />
                    <NormalFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_normal.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="480.0000" Y="320.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.5000" Y="0.5000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="KW_GRP_EVENT" ActionTag="-1854295983" Tag="143" IconVisible="False" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" HorizontalEdge="LeftEdge" BottomMargin="128.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="960.0000" Y="512.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="480.0000" Y="128.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.2000" />
            <PreSize X="1.0000" Y="0.8000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>