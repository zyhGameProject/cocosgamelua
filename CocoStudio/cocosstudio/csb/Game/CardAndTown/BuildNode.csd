<GameFile>
  <PropertyGroup Name="BuildNode" Type="Node" ID="68cc07f1-1458-40cd-b720-822c2c432f66" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="58" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Node_Building" ActionTag="-1175461040" Tag="35" IconVisible="True" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="Img_BuildingRect" ActionTag="-579764561" Tag="37" IconVisible="False" LeftMargin="-50.0000" RightMargin="-50.0000" TopMargin="-50.0000" BottomMargin="-50.0000" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="100.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="Img_percent" ActionTag="-999630106" Tag="40" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-1.0000" RightMargin="-1.0000" TopMargin="-29.0000" BottomMargin="112.0000" LeftEage="64" RightEage="64" TopEage="16" BottomEage="16" Scale9OriginX="64" Scale9OriginY="16" Scale9Width="67" Scale9Height="17" ctype="ImageViewObjectData">
                    <Size X="102.0000" Y="17.0000" />
                    <Children>
                      <AbstractNodeData Name="Lbr_percent" ActionTag="1750888988" Tag="38" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="1.0000" RightMargin="1.0000" TopMargin="1.0000" BottomMargin="1.0000" ProgressInfo="1" ctype="LoadingBarObjectData">
                        <Size X="100.0000" Y="15.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="51.0000" Y="8.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.9804" Y="0.8824" />
                        <ImageFileData Type="Normal" Path="res/Game/CardAndTown/com/img/com_green.png" Plist="" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" />
                    <Position X="50.0000" Y="112.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="1.1200" />
                    <PreSize X="1.0200" Y="0.1700" />
                    <FileData Type="Normal" Path="res/Game/CardAndTown/com/img/com_gray.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Txt_percent" ActionTag="-967435298" Tag="39" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="39.5000" RightMargin="39.5000" TopMargin="-40.0000" BottomMargin="115.0000" FontSize="20" LabelText="1%" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="21.0000" Y="25.0000" />
                    <AnchorPoint ScaleX="0.5000" />
                    <Position X="50.0000" Y="115.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="1.1500" />
                    <PreSize X="0.2100" Y="0.2500" />
                    <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                    <OutlineColor A="255" R="0" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="res/Game/CardAndTown/com/img/red_panel.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_Build" ActionTag="-931058958" VisibleForFrame="False" Tag="36" IconVisible="True" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="img" ActionTag="-135917230" Tag="41" IconVisible="False" LeftMargin="-24.0000" RightMargin="-24.0000" TopMargin="-72.0000" BottomMargin="-24.0000" TouchEnable="True" LeftEage="15" RightEage="15" TopEage="31" BottomEage="31" Scale9OriginX="15" Scale9OriginY="31" Scale9Width="18" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="48.0000" Y="96.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position Y="24.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="res/Game/CardAndTown/build/base/build_1.png" Plist="plist/Game/CardAndTown/build/build_base.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Grp_ColliderBox" ActionTag="-867626290" Tag="108" IconVisible="False" LeftMargin="-8.0000" RightMargin="-8.0000" TopMargin="-8.0000" BottomMargin="-8.0000" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="16.0000" Y="16.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="255" G="0" B="11" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Btn_Item" ActionTag="-1472192944" VisibleForFrame="False" Tag="189" IconVisible="False" LeftMargin="-18.0000" RightMargin="-18.0000" TopMargin="-16.5000" BottomMargin="-16.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="96" Scale9Height="88" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="36.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
                <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btns" ActionTag="-2081987699" VisibleForFrame="False" Tag="146" IconVisible="True" TopMargin="-24.0000" BottomMargin="24.0000" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position Y="24.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>