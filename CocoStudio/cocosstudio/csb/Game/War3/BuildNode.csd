<GameFile>
  <PropertyGroup Name="BuildNode" Type="Node" ID="7c442051-9bef-4c62-839b-e811442136f0" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="23" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Node_Build" ActionTag="352444470" Tag="35" IconVisible="True" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="Grp_ColliderBox" ActionTag="1565900924" Tag="780" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-32.0000" RightMargin="-32.0000" TopMargin="-11.0000" BottomMargin="-11.0000" LeftEage="5" RightEage="5" TopEage="5" BottomEage="5" Scale9OriginX="5" Scale9OriginY="5" Scale9Width="6" Scale9Height="6" ctype="ImageViewObjectData">
                <Size X="64.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="res/Game/CardAndTown/com/img/ImgbuildHide.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="model" ActionTag="1141355884" Tag="134" IconVisible="False" LeftMargin="-64.0000" RightMargin="-64.0000" TopMargin="-88.0000" BottomMargin="-8.0000" ctype="SpriteObjectData">
                <Size X="128.0000" Y="96.0000" />
                <AnchorPoint ScaleX="0.5000" />
                <Position Y="-8.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="res/Game/War3/build/ArchMageTower.png" Plist="plist/Game/War3/build/build.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Grp_ClickBox" ActionTag="1603484230" Tag="37" IconVisible="False" LeftMargin="-50.0000" RightMargin="-50.0000" TopMargin="-87.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="100.0000" Y="87.0000" />
                <AnchorPoint ScaleX="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="255" G="0" B="11" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>