<GameFile>
  <PropertyGroup Name="GameScene" Type="Scene" ID="0b765421-7671-4d3e-a1b2-748b46dcaadd" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="37" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-1211375518" Tag="56" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" StretchWidthEnable="True" StretchHeightEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="-33" Scale9OriginY="-33" Scale9Width="66" Scale9Height="66" ctype="PanelObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Grp_View" ActionTag="-1976679142" Tag="39" IconVisible="False" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" StretchWidthEnable="True" StretchHeightEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="960.0000" Y="640.0000" />
            <Children>
              <AbstractNodeData Name="KW_GRP_TouchEvent" ActionTag="-1084747131" Tag="2220" IconVisible="False" RightMargin="960.0000" TopMargin="640.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="KW_GRP_Terrain" ActionTag="-333817477" CallBackName="OnBtnClickTerrain" Tag="40" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" StretchWidthEnable="True" StretchHeightEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="960.0000" Y="640.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="KW_GRP_Objects" ActionTag="-1926083129" Tag="41" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="960.0000" Y="640.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="KW_GRP_OperateUI" ActionTag="-162979517" Tag="45" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="960.0000" Y="640.0000" />
                <Children>
                  <AbstractNodeData Name="Grp_Map" ActionTag="1170460491" Tag="46" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" RightMargin="720.0000" TopMargin="400.0000" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="240.0000" Y="240.0000" />
                    <AnchorPoint />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.2500" Y="0.3750" />
                    <SingleColor A="255" R="0" G="0" B="0" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Grp_Attr" ActionTag="840380140" Tag="50" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" LeftMargin="230.0000" RightMargin="400.0000" TopMargin="454.4000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="330.0000" Y="185.6000" />
                    <Children>
                      <AbstractNodeData Name="Grp_Attr_Base" ActionTag="1060916216" VisibleForFrame="False" Tag="315" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" ctype="PanelObjectData">
                        <Size X="330.0000" Y="185.6000" />
                        <Children>
                          <AbstractNodeData Name="Img_Defence" ActionTag="1271934018" Tag="316" IconVisible="False" LeftMargin="89.6484" RightMargin="144.3516" TopMargin="73.9572" BottomMargin="23.6428" LeftEage="31" RightEage="31" TopEage="29" BottomEage="29" Scale9OriginX="31" Scale9OriginY="29" Scale9Width="34" Scale9Height="30" ctype="ImageViewObjectData">
                            <Size X="96.0000" Y="88.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="137.6484" Y="67.6428" />
                            <Scale ScaleX="0.5000" ScaleY="0.5000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.4171" Y="0.3645" />
                            <PreSize X="0.2909" Y="0.4741" />
                            <FileData Type="Normal" Path="res/Game/War3/power/main_icons_0002.png" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Txt_Defence" ActionTag="-1773015718" Tag="319" IconVisible="False" LeftMargin="178.6109" RightMargin="74.3891" TopMargin="106.4572" BottomMargin="56.1428" FontSize="20" LabelText="dasdasd" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                            <Size X="77.0000" Y="23.0000" />
                            <AnchorPoint ScaleY="0.5000" />
                            <Position X="178.6109" Y="67.6428" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="0" G="0" B="0" />
                            <PrePosition X="0.5412" Y="0.3645" />
                            <PreSize X="0.2333" Y="0.1239" />
                            <FontResource Type="Normal" Path="res/Game/War3/com/font/arial.ttf" Plist="" />
                            <OutlineColor A="255" R="255" G="0" B="0" />
                            <ShadowColor A="255" R="110" G="110" B="110" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Img_Attack" ActionTag="-449405390" Tag="317" IconVisible="False" LeftMargin="89.6484" RightMargin="144.3516" TopMargin="22.8605" BottomMargin="74.7395" LeftEage="31" RightEage="31" TopEage="29" BottomEage="29" Scale9OriginX="31" Scale9OriginY="29" Scale9Width="34" Scale9Height="30" ctype="ImageViewObjectData">
                            <Size X="96.0000" Y="88.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="137.6484" Y="118.7395" />
                            <Scale ScaleX="0.5000" ScaleY="0.5000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.4171" Y="0.6398" />
                            <PreSize X="0.2909" Y="0.4741" />
                            <FileData Type="Normal" Path="res/Game/War3/power/main_icons_0023.png" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Txt_Attack" ActionTag="728106312" Tag="318" IconVisible="False" LeftMargin="178.6109" RightMargin="74.3891" TopMargin="55.3605" BottomMargin="107.2395" FontSize="20" LabelText="dasdasd" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                            <Size X="77.0000" Y="23.0000" />
                            <AnchorPoint ScaleY="0.5000" />
                            <Position X="178.6109" Y="118.7395" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="0" G="0" B="0" />
                            <PrePosition X="0.5412" Y="0.6398" />
                            <PreSize X="0.2333" Y="0.1239" />
                            <FontResource Type="Normal" Path="res/Game/War3/com/font/arial.ttf" Plist="" />
                            <OutlineColor A="255" R="255" G="0" B="0" />
                            <ShadowColor A="255" R="110" G="110" B="110" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint />
                        <Position />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition />
                        <PreSize X="1.0000" Y="1.0000" />
                        <SingleColor A="255" R="150" G="200" B="255" />
                        <FirstColor A="255" R="150" G="200" B="255" />
                        <EndColor A="255" R="255" G="255" B="255" />
                        <ColorVector ScaleY="1.0000" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Grp_Attr_Build" ActionTag="848030461" VisibleForFrame="False" Tag="144" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="18.5000" RightMargin="18.5000" TopMargin="45.6000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                        <Size X="293.0000" Y="140.0000" />
                        <Children>
                          <AbstractNodeData Name="Img_percent" ActionTag="-1398490303" Tag="3664" IconVisible="False" LeftMargin="89.0000" RightMargin="52.0000" TopMargin="30.2000" BottomMargin="92.8000" LeftEage="64" RightEage="64" TopEage="16" BottomEage="16" Scale9OriginX="64" Scale9OriginY="16" Scale9Width="67" Scale9Height="17" ctype="ImageViewObjectData">
                            <Size X="152.0000" Y="17.0000" />
                            <Children>
                              <AbstractNodeData Name="Lbr_percent" ActionTag="1974426409" Tag="3665" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="1.0000" RightMargin="1.0000" TopMargin="1.0000" BottomMargin="1.0000" ProgressInfo="0" ctype="LoadingBarObjectData">
                                <Size X="150.0000" Y="15.0000" />
                                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                                <Position X="76.0000" Y="8.5000" />
                                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                                <CColor A="255" R="255" G="255" B="255" />
                                <PrePosition X="0.5000" Y="0.5000" />
                                <PreSize X="0.9868" Y="0.8824" />
                                <ImageFileData Type="Normal" Path="res/Game/CardAndTown/com/img/com_green.png" Plist="" />
                              </AbstractNodeData>
                            </Children>
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="165.0000" Y="101.3000" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.5631" Y="0.7236" />
                            <PreSize X="0.5188" Y="0.1214" />
                            <FileData Type="Normal" Path="res/Game/CardAndTown/com/img/com_gray.png" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Txt_percent" ActionTag="1779656389" Tag="3666" IconVisible="False" LeftMargin="154.5000" RightMargin="117.5000" TopMargin="22.2000" BottomMargin="92.8000" FontSize="20" LabelText="1%" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                            <Size X="21.0000" Y="25.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="165.0000" Y="105.3000" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.5631" Y="0.7521" />
                            <PreSize X="0.0717" Y="0.1786" />
                            <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                            <OutlineColor A="255" R="0" G="0" B="0" />
                            <ShadowColor A="255" R="110" G="110" B="110" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Node_Unit_1" ActionTag="1694249470" Tag="145" IconVisible="True" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="43.9998" RightMargin="249.0002" TopMargin="37.5292" BottomMargin="102.4708" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                            <Size X="0.0000" Y="0.0000" />
                            <AnchorPoint />
                            <Position X="43.9998" Y="102.4708" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.1502" Y="0.7319" />
                            <PreSize X="0.0000" Y="0.0000" />
                            <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Node_Unit_2" ActionTag="-1930137432" Tag="163" IconVisible="True" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="43.2102" RightMargin="249.7898" TopMargin="101.7346" BottomMargin="38.2654" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                            <Size X="0.0000" Y="0.0000" />
                            <AnchorPoint />
                            <Position X="43.2102" Y="38.2654" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.1475" Y="0.2733" />
                            <PreSize X="0.0000" Y="0.0000" />
                            <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Node_Unit_3" ActionTag="100348845" Tag="169" IconVisible="True" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="112.0993" RightMargin="180.9007" TopMargin="101.7346" BottomMargin="38.2654" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                            <Size X="0.0000" Y="0.0000" />
                            <AnchorPoint />
                            <Position X="112.0993" Y="38.2654" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.3826" Y="0.2733" />
                            <PreSize X="0.0000" Y="0.0000" />
                            <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Node_Unit_4" ActionTag="1524623111" Tag="175" IconVisible="True" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="180.9884" RightMargin="112.0116" TopMargin="101.7346" BottomMargin="38.2654" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                            <Size X="0.0000" Y="0.0000" />
                            <AnchorPoint />
                            <Position X="180.9884" Y="38.2654" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.6177" Y="0.2733" />
                            <PreSize X="0.0000" Y="0.0000" />
                            <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Node_Unit_5" ActionTag="-2052906813" Tag="181" IconVisible="True" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="249.8775" RightMargin="43.1225" TopMargin="101.7346" BottomMargin="38.2654" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                            <Size X="0.0000" Y="0.0000" />
                            <AnchorPoint />
                            <Position X="249.8775" Y="38.2654" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.8528" Y="0.2733" />
                            <PreSize X="0.0000" Y="0.0000" />
                            <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint ScaleX="0.5000" />
                        <Position X="165.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" />
                        <PreSize X="0.8879" Y="0.7543" />
                        <SingleColor A="255" R="150" G="200" B="255" />
                        <FirstColor A="255" R="150" G="200" B="255" />
                        <EndColor A="255" R="255" G="255" B="255" />
                        <ColorVector ScaleY="1.0000" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position X="230.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2396" />
                    <PreSize X="0.3438" Y="0.2900" />
                    <SingleColor A="255" R="255" G="255" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Grp_Prop" ActionTag="138538013" Tag="49" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="BottomEdge" LeftMargin="558.0000" RightMargin="270.0000" TopMargin="448.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="132.0000" Y="192.0000" />
                    <Children>
                      <AbstractNodeData Name="Node_Item_1" ActionTag="-291555203" VisibleForFrame="False" Tag="81" IconVisible="True" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="34.0000" RightMargin="98.0000" TopMargin="34.0000" BottomMargin="158.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="34.0000" Y="158.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.2576" Y="0.8229" />
                        <PreSize X="0.0000" Y="0.0000" />
                        <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Node_Item_2" ActionTag="1098598683" VisibleForFrame="False" Tag="87" IconVisible="True" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="98.0000" RightMargin="34.0000" TopMargin="34.0000" BottomMargin="158.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="98.0000" Y="158.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.7424" Y="0.8229" />
                        <PreSize X="0.0000" Y="0.0000" />
                        <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Node_Item_3" ActionTag="58836521" VisibleForFrame="False" Tag="93" IconVisible="True" VerticalEdge="BothEdge" LeftMargin="34.0000" RightMargin="98.0000" TopMargin="96.0000" BottomMargin="96.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="34.0000" Y="96.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.2576" Y="0.5000" />
                        <PreSize X="0.0000" Y="0.0000" />
                        <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Node_Item_4" ActionTag="1644026660" VisibleForFrame="False" Tag="99" IconVisible="True" VerticalEdge="BothEdge" LeftMargin="98.0000" RightMargin="34.0000" TopMargin="96.0000" BottomMargin="96.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="98.0000" Y="96.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.7424" Y="0.5000" />
                        <PreSize X="0.0000" Y="0.0000" />
                        <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Node_Item_5" ActionTag="1006495475" VisibleForFrame="False" Tag="105" IconVisible="True" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" LeftMargin="34.0000" RightMargin="98.0000" TopMargin="158.0000" BottomMargin="34.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="34.0000" Y="34.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.2576" Y="0.1771" />
                        <PreSize X="0.0000" Y="0.0000" />
                        <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Node_Item_6" ActionTag="778325581" VisibleForFrame="False" Tag="111" IconVisible="True" HorizontalEdge="RightEdge" VerticalEdge="BottomEdge" LeftMargin="98.0000" RightMargin="34.0000" TopMargin="158.0000" BottomMargin="34.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="98.0000" Y="34.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.7424" Y="0.1771" />
                        <PreSize X="0.0000" Y="0.0000" />
                        <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="1.0000" />
                    <Position X="690.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7188" />
                    <PreSize X="0.1375" Y="0.3000" />
                    <SingleColor A="255" R="255" G="85" B="72" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Grp_Power" ActionTag="814706966" Tag="47" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="BottomEdge" LeftMargin="690.0000" TopMargin="450.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="270.0000" Y="190.0000" />
                    <Children>
                      <AbstractNodeData Name="Grp_Power_Nodes" ActionTag="-116998734" VisibleForFrame="False" Tag="320" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" ctype="PanelObjectData">
                        <Size X="270.0000" Y="190.0000" />
                        <Children>
                          <AbstractNodeData Name="Node_Power_1" ActionTag="122627630" Tag="129" IconVisible="True" LeftMargin="34.3875" RightMargin="235.6125" TopMargin="34.9782" BottomMargin="155.0218" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                            <Size X="0.0000" Y="0.0000" />
                            <AnchorPoint />
                            <Position X="34.3875" Y="155.0218" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.1274" Y="0.8159" />
                            <PreSize X="0.0000" Y="0.0000" />
                            <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Node_Power_2" ActionTag="1292846363" Tag="135" IconVisible="True" LeftMargin="101.7026" RightMargin="168.2974" TopMargin="34.9782" BottomMargin="155.0218" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                            <Size X="0.0000" Y="0.0000" />
                            <AnchorPoint />
                            <Position X="101.7026" Y="155.0218" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.3767" Y="0.8159" />
                            <PreSize X="0.0000" Y="0.0000" />
                            <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Node_Power_3" ActionTag="-1500452058" Tag="141" IconVisible="True" LeftMargin="169.0177" RightMargin="100.9823" TopMargin="34.9782" BottomMargin="155.0218" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                            <Size X="0.0000" Y="0.0000" />
                            <AnchorPoint />
                            <Position X="169.0177" Y="155.0218" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.6260" Y="0.8159" />
                            <PreSize X="0.0000" Y="0.0000" />
                            <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Node_Power_4" ActionTag="1791610990" Tag="147" IconVisible="True" LeftMargin="236.3327" RightMargin="33.6673" TopMargin="34.9782" BottomMargin="155.0218" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                            <Size X="0.0000" Y="0.0000" />
                            <AnchorPoint />
                            <Position X="236.3327" Y="155.0218" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.8753" Y="0.8159" />
                            <PreSize X="0.0000" Y="0.0000" />
                            <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Node_Power_5" ActionTag="-1418136681" Tag="153" IconVisible="True" LeftMargin="34.3875" RightMargin="235.6125" TopMargin="95.8208" BottomMargin="94.1792" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                            <Size X="0.0000" Y="0.0000" />
                            <AnchorPoint />
                            <Position X="34.3875" Y="94.1792" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.1274" Y="0.4957" />
                            <PreSize X="0.0000" Y="0.0000" />
                            <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Node_Power_6" ActionTag="-1061014409" Tag="159" IconVisible="True" LeftMargin="101.4815" RightMargin="168.5185" TopMargin="96.2751" BottomMargin="93.7249" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                            <Size X="0.0000" Y="0.0000" />
                            <AnchorPoint />
                            <Position X="101.4815" Y="93.7249" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.3759" Y="0.4933" />
                            <PreSize X="0.0000" Y="0.0000" />
                            <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Node_Power_7" ActionTag="1301945923" Tag="165" IconVisible="True" LeftMargin="168.9071" RightMargin="101.0929" TopMargin="96.2751" BottomMargin="93.7249" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                            <Size X="0.0000" Y="0.0000" />
                            <AnchorPoint />
                            <Position X="168.9071" Y="93.7249" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.6256" Y="0.4933" />
                            <PreSize X="0.0000" Y="0.0000" />
                            <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Node_Power_8" ActionTag="-1271905752" Tag="171" IconVisible="True" LeftMargin="236.3328" RightMargin="33.6672" TopMargin="96.2751" BottomMargin="93.7249" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                            <Size X="0.0000" Y="0.0000" />
                            <AnchorPoint />
                            <Position X="236.3328" Y="93.7249" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.8753" Y="0.4933" />
                            <PreSize X="0.0000" Y="0.0000" />
                            <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Node_Power_9" ActionTag="-936484106" Tag="177" IconVisible="True" LeftMargin="34.3875" RightMargin="235.6125" TopMargin="156.6634" BottomMargin="33.3366" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                            <Size X="0.0000" Y="0.0000" />
                            <AnchorPoint />
                            <Position X="34.3875" Y="33.3366" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.1274" Y="0.1755" />
                            <PreSize X="0.0000" Y="0.0000" />
                            <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Node_Power_10" ActionTag="-1551948216" Tag="183" IconVisible="True" LeftMargin="101.7025" RightMargin="168.2975" TopMargin="156.6634" BottomMargin="33.3366" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                            <Size X="0.0000" Y="0.0000" />
                            <AnchorPoint />
                            <Position X="101.7025" Y="33.3366" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.3767" Y="0.1755" />
                            <PreSize X="0.0000" Y="0.0000" />
                            <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Node_Power_11" ActionTag="920985870" Tag="189" IconVisible="True" LeftMargin="169.0175" RightMargin="100.9825" TopMargin="156.6634" BottomMargin="33.3366" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                            <Size X="0.0000" Y="0.0000" />
                            <AnchorPoint />
                            <Position X="169.0175" Y="33.3366" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.6260" Y="0.1755" />
                            <PreSize X="0.0000" Y="0.0000" />
                            <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                          </AbstractNodeData>
                          <AbstractNodeData Name="Node_Power_12" ActionTag="-1001392843" Tag="195" IconVisible="True" LeftMargin="236.3326" RightMargin="33.6674" TopMargin="156.6634" BottomMargin="33.3366" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                            <Size X="0.0000" Y="0.0000" />
                            <AnchorPoint />
                            <Position X="236.3326" Y="33.3366" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.8753" Y="0.1755" />
                            <PreSize X="0.0000" Y="0.0000" />
                            <FileData Type="Normal" Path="csb/Game/War3/PowerNode.csd" Plist="" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint />
                        <Position />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition />
                        <PreSize X="1.0000" Y="1.0000" />
                        <SingleColor A="255" R="150" G="200" B="255" />
                        <FirstColor A="255" R="150" G="200" B="255" />
                        <EndColor A="255" R="255" G="255" B="255" />
                        <ColorVector ScaleY="1.0000" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="1.0000" />
                    <Position X="960.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.0000" />
                    <PreSize X="0.2813" Y="0.2969" />
                    <SingleColor A="255" R="255" G="255" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>