<GameFile>
  <PropertyGroup Name="PowerNode" Type="Node" ID="a3b62aa8-a654-4ac2-9412-e264e81ea062" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="51" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Grp_View" ActionTag="1183605627" Tag="52" IconVisible="False" LeftMargin="-32.0000" RightMargin="-32.0000" TopMargin="-32.0000" BottomMargin="-32.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="64.0000" Y="64.0000" />
            <Children>
              <AbstractNodeData Name="frame" ActionTag="617941594" VisibleForFrame="False" Tag="54" IconVisible="False" LeftMargin="3.0000" RightMargin="3.0000" TopMargin="3.0000" BottomMargin="3.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="33" BottomEage="33" Scale9OriginX="33" Scale9OriginY="33" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="58.0000" Y="58.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="32.0000" Y="32.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.9063" Y="0.9063" />
                <FileData Type="Normal" Path="res/Game/War3/com/img/red_panel.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="icon" ActionTag="-1408482668" Tag="53" IconVisible="False" LeftMargin="-16.0000" RightMargin="-16.0000" TopMargin="-12.0000" BottomMargin="-12.0000" LeftEage="16" RightEage="16" TopEage="16" BottomEage="16" Scale9OriginX="16" Scale9OriginY="16" Scale9Width="64" Scale9Height="56" ctype="ImageViewObjectData">
                <Size X="96.0000" Y="88.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="32.0000" Y="32.0000" />
                <Scale ScaleX="0.7000" ScaleY="0.7000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.5000" Y="1.3750" />
                <FileData Type="Normal" Path="res/Game/War3/power/main_icons_0000.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="num" ActionTag="1483445688" VisibleForFrame="False" Tag="3571" IconVisible="False" LeftMargin="37.0407" RightMargin="7.9593" TopMargin="34.4226" BottomMargin="4.5774" FontSize="20" LabelText="11" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="19.0000" Y="25.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="56.0407" Y="17.0774" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.8756" Y="0.2668" />
                <PreSize X="0.2969" Y="0.3906" />
                <FontResource Type="Normal" Path="res/Game/War3/com/font/kenvector_future_thin.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="255" B="255" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>