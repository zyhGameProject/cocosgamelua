<GameFile>
  <PropertyGroup Name="PathEditorUI" Type="Layer" ID="4abb8755-8f71-479a-861b-fbb9cee7441e" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="25" ctype="GameLayerObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-942558008" Tag="46" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" TouchEnable="True" StretchWidthEnable="True" StretchHeightEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="view" ActionTag="251668771" Tag="26" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="66" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="960.0000" Y="640.0000" />
            <Children>
              <AbstractNodeData Name="KW_NOD_DrawList" ActionTag="-1640261473" Tag="47" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" RightMargin="960.0000" TopMargin="640.0000" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="KW_GRP_Touch" ActionTag="427379982" Tag="54" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="960.0000" Y="640.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Btn_Back" ActionTag="-1985592807" VisibleForFrame="False" CallBackType="Click" CallBackName="onBtnBack" Tag="28" IconVisible="False" LeftMargin="13.2875" RightMargin="826.7125" TopMargin="17.6115" BottomMargin="573.3885" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="27" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="120.0000" Y="49.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="-2055374573" Tag="29" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="32.0000" RightMargin="32.0000" TopMargin="12.0000" BottomMargin="12.0000" FontSize="20" LabelText="back" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="56.0000" Y="25.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="60.0000" Y="24.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.4667" Y="0.5102" />
                    <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="255" B="255" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="73.2875" Y="597.8885" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0763" Y="0.9342" />
                <PreSize X="0.1250" Y="0.0766" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_click.png" Plist="" />
                <PressedFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_click.png" Plist="" />
                <NormalFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Btn_Obstacle" ActionTag="-984124920" CallBackType="Click" CallBackName="onBtnSetObstacle" Tag="30" IconVisible="False" LeftMargin="825.5300" RightMargin="14.4700" TopMargin="17.6113" BottomMargin="573.3887" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="27" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="120.0000" Y="49.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="592470479" Tag="31" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="5.0000" RightMargin="5.0000" TopMargin="12.0000" BottomMargin="12.0000" FontSize="20" LabelText="obstacle" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="110.0000" Y="25.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="60.0000" Y="24.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="254" G="155" B="0" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9167" Y="0.5102" />
                    <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                    <OutlineColor A="255" R="111" G="111" B="111" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="885.5300" Y="597.8887" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9224" Y="0.9342" />
                <PreSize X="0.1250" Y="0.0766" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_click.png" Plist="" />
                <PressedFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_click.png" Plist="" />
                <NormalFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Btn_Start" ActionTag="1468195107" CallBackType="Click" CallBackName="onBtnSetStart" Tag="48" IconVisible="False" LeftMargin="825.5300" RightMargin="14.4700" TopMargin="77.6608" BottomMargin="513.3392" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="27" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="120.0000" Y="49.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="491149941" Tag="49" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="27.0000" RightMargin="27.0000" TopMargin="12.0000" BottomMargin="12.0000" FontSize="20" LabelText="start" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="66.0000" Y="25.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="60.0000" Y="24.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="153" G="75" B="244" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.5500" Y="0.5102" />
                    <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                    <OutlineColor A="255" R="111" G="111" B="111" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="885.5300" Y="537.8392" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9224" Y="0.8404" />
                <PreSize X="0.1250" Y="0.0766" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_click.png" Plist="" />
                <PressedFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_click.png" Plist="" />
                <NormalFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Btn_End" ActionTag="1405381238" CallBackType="Click" CallBackName="onBtnSetEnd" Tag="50" IconVisible="False" LeftMargin="825.5300" RightMargin="14.4700" TopMargin="137.7103" BottomMargin="453.2897" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="27" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="120.0000" Y="49.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="1634349143" Tag="51" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="39.0000" RightMargin="39.0000" TopMargin="12.0000" BottomMargin="12.0000" FontSize="20" LabelText="end" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="42.0000" Y="25.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="60.0000" Y="24.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="155" B="0" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.3500" Y="0.5102" />
                    <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                    <OutlineColor A="255" R="111" G="111" B="111" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="885.5300" Y="477.7897" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9224" Y="0.7465" />
                <PreSize X="0.1250" Y="0.0766" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_click.png" Plist="" />
                <PressedFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_click.png" Plist="" />
                <NormalFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Btn_Clear" ActionTag="-232792426" CallBackType="Click" CallBackName="onBtnClear" Tag="52" IconVisible="False" LeftMargin="825.5300" RightMargin="14.4700" TopMargin="197.7597" BottomMargin="393.2403" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="27" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="120.0000" Y="49.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="-748828708" Tag="53" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="25.0000" RightMargin="25.0000" TopMargin="12.0000" BottomMargin="12.0000" FontSize="20" LabelText="clear" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="70.0000" Y="25.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="60.0000" Y="24.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.5833" Y="0.5102" />
                    <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="255" B="255" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="885.5300" Y="417.7403" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9224" Y="0.6527" />
                <PreSize X="0.1250" Y="0.0766" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_click.png" Plist="" />
                <PressedFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_click.png" Plist="" />
                <NormalFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Btn_Path" ActionTag="-194989220" CallBackType="Click" CallBackName="onBtnFindPath" Tag="55" IconVisible="False" LeftMargin="825.5300" RightMargin="14.4700" TopMargin="257.8092" BottomMargin="333.1908" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="27" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="120.0000" Y="49.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="1827945459" Tag="56" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="6.0000" RightMargin="6.0000" TopMargin="12.0000" BottomMargin="12.0000" FontSize="20" LabelText="Find Path" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="108.0000" Y="25.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="60.0000" Y="24.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9000" Y="0.5102" />
                    <FontResource Type="Normal" Path="res/Game/CardAndTown/com/font/kenvector_future_thin.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="255" B="255" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="885.5300" Y="357.6908" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9224" Y="0.5589" />
                <PreSize X="0.1250" Y="0.0766" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_click.png" Plist="" />
                <PressedFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_click.png" Plist="" />
                <NormalFileData Type="Normal" Path="res/Game/CardAndTown/com/img/grey_button_normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="67" B="72" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>