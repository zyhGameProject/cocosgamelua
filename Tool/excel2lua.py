﻿#coding:utf-8
import sys
import os
import xlrd
import re
import types
import shutil
import codecs


# curxlspath = "%s/client" % (luaDir)
# curluapath = "%s../Script\\app\\data" % (luaDir)

# 配置转换为lua文件
def SaveCfgToLua(_dataTab, _tabName, _luaName,_luaGamePath,_isOver):
    # log
    # print("----SaveCfgToLua _luaName=%s" % (_luaName))

    # 当前lua路径
    luaDir = os.path.abspath(".")

    luaPath = "%s\\%s" % (luaDir, _luaName)
    # print("luaPath=%s" % luaPath)

    # 打开文件
    file = codecs.open(luaPath, "w+", "utf-8")


    # 表开始
    file.write(unicode("\n\n--%s表" % _tabName, "utf-8"))
    file.write(unicode("\n\n", "utf-8"))



    strValue = "local %s=\n{\n" % (_tabName)
    strValue = unicode(strValue, "utf-8")
    file.write(strValue)

    # count=len(_dataTab)
    # for i in range(count):
    #    key=sorted_key_list[i]
    #    sortDataList[key]= _dict[key]

    sorted_key_list = sorted(_dataTab.keys())

    # 表数据
    count = len(sorted_key_list)
    # for key,data in _dataTab.items():
    for i in range(count):
        # 键开始
        key = sorted_key_list[i]
        data = _dataTab[key]
        keyType = data["id"]["type"]
        if (keyType == "Int"):
            strValue = "\t[%d]=\n\t{\n" % (data["id"]["value"]) # lua 从1开始遍历
        else:
            strValue = ("\t%s=\n\t{\n" % (data["id"]["value"])).encode("utf-8")
        strValue = unicode(strValue, "utf-8")
        file.write(strValue)

        # 值
        for filedName, filedValue in data.items():
            valueType = filedValue[u"type"]
            if (filedName != "desc" and filedName != "id" and filedValue["value"] != "null"):
                if (valueType == "Int"):
                    strValue = "\t\t%s=%d,\n" % (filedName, filedValue["value"])
                elif (valueType == "Float"):
                    strValue = "\t\t%s=%6.2f,\n" % (filedName, filedValue["value"])
                elif (valueType == "String"):
                    strValue = "\t\t%s=\"%s\",\n" % (filedName, filedValue["value"])
                elif (valueType == "StringArray"):
                    tempArray = ("%s" % filedValue["value"]).split(";")
                    strValue = "\t\t%s={\n" % (filedName)
                    aIndex = 0
                    for v in tempArray:
                        aIndex = aIndex + 1
                        strValue = "%s\t\t\t[%d]=\"%s\",\n" % (strValue, aIndex, v)
                    strValue = "%s\t\t},\n" % (strValue)
                elif (valueType == "Array"):
                    tempArray = ("%s" % filedValue["value"]).split(";")
                    strValue = "\t\t%s={\n" % (filedName)
                    aIndex = 0
                    for v in tempArray:
                        aIndex = aIndex + 1
                        strValue = "%s\t\t\t[%d]=%d,\n" % (strValue,aIndex,int(float(v)))
                    strValue = "%s\t\t},\n" % (strValue)
                elif (valueType == "IntMap"):
                    tempArray = ("%s" % filedValue["value"]).split(";")
                    strValue = "\t\t%s={\n" % (filedName)
                    for v in tempArray:
                        valueArray = v.split("=")
                        strValue = "%s\t\t\t[%d]=%s,\n" % (strValue,int(float(valueArray[0])),valueArray[1])
                    strValue = "%s\t\t},\n" % (strValue)
                elif (valueType == "Map"):
                    tempArray = ("%s" % filedValue["value"]).split(";")
                    strValue = "\t\t%s={\n" % (filedName)
                    for v in tempArray:
                        valueArray = v.split("=")
                        strValue = "%s\t\t\t[\"%s\"]=%s,\n" % (strValue,valueArray[0],valueArray[1])
                    strValue = "%s\t\t},\n" % (strValue)

                tValue = type(strValue)
                if (tValue != types.UnicodeType):
                    strValue = unicode(strValue, "utf-8")
                file.write(strValue)
        # 键结束
        file.write(unicode("\t},\n", "utf-8"))

    # 表结束
    file.write(unicode("}\n\n", "utf-8"))
    strValue = unicode("return %s\n" % (_tabName), "utf-8")
    file.write(strValue)


    # 关闭
    file.close()



    # 保存文件到代码目录
    luaDir = os.path.abspath(".")
    strValue = "%s/../Script\\app\\game\\%s" % (luaDir,_luaGamePath)

    if (os.path.exists(strValue)):
        strValue =  "%s\\Data" % (strValue)

        codeLuaDir = os.path.abspath(strValue)
        codeLuaPath = "%s\\%s" % (codeLuaDir, _luaName)
        if (os.path.exists(codeLuaPath)):
            os.remove(codeLuaPath)
        shutil.move(luaPath, codeLuaPath)




# 导出一个配置文件
def LoadCfg(_tabName, _xlsPath, _sheetName, _luaGamePath, _isOver):
    # log
    # print("----LoadCfg tName=%s Path=%s sName=%s" % (_tabName,_xlsPath,_sheetName))

    # test
    # if(_tabName=="TabMinimap"):
    #    print("--load tab:%s" % _tabName)

    global G_ErrorData
    # G_ErrorData=[]

    # 读取xls
    data = xlrd.open_workbook(_xlsPath)

    # 获取工作表
    sheet = data.sheet_by_name(_sheetName)
    rowCount = sheet.nrows
    colCount = sheet.ncols
    # print("rowCount=%d" % rowCount)

    # 字段名
    keyList = sheet.row_values(0)
    # 类型
    typeList = sheet.row_values(1)
    # 字段名
    filterList = sheet.row_values(3)

    # 读取值
    dataTab = {}
    mainKeyValue = 0
    for i in range(rowCount):
        if (i > 2):
            dataRow = {}
            row = sheet.row_values(i)
            for c in range(colCount):
                keyValue = keyList[c].encode('utf-8')
                typeValue = typeList[c]
                # 生成字段值
                # print("---keyValue=%s" % keyValue)
                # try:
                #     # if(i==84 and c==2):
                #     # print("log")
                #     # totalValue = GenerateFileValue(typeValue, row[c])
                # except:
                #     # print("=================xxxxxxxxxxxxxxxxxx==================")
                #     estr = "----GenerateFileValue fail!  _tabName=%s  _sheetName=%s row=%d c=%d keyName=%s type=%s value=%s" % (
                #         _xlsPath, _sheetName, i + 1, c + 1, keyValue, typeValue, row[c])
                #     G_ErrorData.append(estr)
                #     # print(estr)
                # print("=================xxxxxxxxxxxxxxxxxx==================")
                # break;

                # 保存一个字段
                dataValue = {"type": typeValue, "value": row[c]}
                dataRow[keyValue] = dataValue
            # 保存一行数据
            dataTab[mainKeyValue] = dataRow
            mainKeyValue = mainKeyValue + 1

    # 排序
    # sorted_key_list=sorted(dataTab)
    # sortedDataTab=map(lambda x:{x:dataTab[x]},sorted_key_list)
    # sortedDataTab= SortDict(dataTab)

    # 配置转换为lua文件
    try:
        luaName = "%s.lua" % _sheetName
        SaveCfgToLua(dataTab, _tabName, luaName,_luaGamePath,_isOver)
    except:
        print("============")
        print("----SaveCfgToLuaError!  luaName=%s" % (luaName))
        G_ErrorData.append(estr)
        print("=================================================")



# 获取配置信息
def GetConfigInfo(_loadCfgIdList):
    # 读取xls
    data = xlrd.open_workbook("config.xlsx")

    # 获取工作表
    sheet = data.sheet_by_name("config")
    rowCount = sheet.nrows
    colCount = sheet.ncols

    # 获取游戏表
    sheetGame = data.sheet_by_name("gameConfig")


    # 字段名
    keyList = sheet.row_values(0)

    # 读取值
    dataTab = {}

    for i in range(rowCount):
        if(i>2):
            dataRow = {}
            row = sheet.row_values(i)
            for c in range(colCount):
                v = row[c]
                colName = keyList[c]
                typeValue = type(v)
                if (typeValue is float):
                    colValue = int(v)
                else:
                    colValue = v.encode('utf-8')
                if(colName == "luaGamePath"):
                    rowTemp = sheetGame.row_values(2+colValue)
                    colValue = rowTemp[1]
                    if v not in _loadCfgIdList:
                        dataRow = {}
                        break
                dataRow[colName] = colValue

            if len(dataRow) != 0:
                dataTab[dataRow["id"]] = dataRow

    return dataTab

#导出所有配置文件到lua
def ExportAllCfgToLua(_isLoadAll,_loadCfgIdList):
    cfgList=GetConfigInfo(_loadCfgIdList)
    curPath=os.path.abspath(".")
    fileList={}
    # strValue = "%s/%s/" % (curPath)
    # clientPath = os.path.abspath(strValue)
    # 加载
    loadPercent = 0
    dataCount = len(cfgList)
    curIndex = 0
    initPath = "%s\\%s" % (curPath, "init.lua")
    # initFile = codecs.open(initPath, "w+", "utf-8")
    # initFile.write("local GData={}\n")
    for cfgId, data in cfgList.items():
        # 是否需要加载
        needLoad = True
        if data["luaGamePath"] not in fileList:
            initPath = "%s\\%s.lua" % (curPath, data["luaGamePath"])
            fileList[data["luaGamePath"]] = codecs.open(initPath, "w+", "utf-8")
            fileList[data["luaGamePath"]].write("local GData={}\n")
        initFile = fileList[data["luaGamePath"]]
        if (needLoad):
            curIndex = curIndex + 1
            nameStr = "%s %s -->%s" % (data["tabName"], data["excelFile"], data["sheetName"])
            print("------begin load %s id=%d " % (nameStr, data["id"]))
            strValue = "%s/%s/" % (curPath,data["luaGamePath"])
            clientPath = os.path.abspath(strValue)
            filePath = "%s/%s" % (clientPath, data["excelFile"])
            initFile.write("GData.%s=require(\"app.game.%s.Data.%s\")\n" % (data["tabName"], data["luaGamePath"],data["sheetName"]))
            LoadCfg(data["tabName"], filePath, data["sheetName"], data["luaGamePath"],(curIndex == dataCount))
            # if(curIndex==dataCount):
            #     initFile.write("return GData")
            #     initFile.close()
            #     strValue = "%s/../Script\\app\\game\\%s\\Data" % (curPath,data["luaGamePath"])
            #     codeLuaDir = os.path.abspath(strValue)
            #     codeLuaPath = "%s\\%s" % (codeLuaDir, "GData.lua")
            #     shutil.move(initPath, codeLuaPath)

            # 进度
            loadPercent = curIndex * 100 / dataCount
            print("------load %s ok! loadPercent=%6.1f%%" % (nameStr, loadPercent))
            print('--------------------------------------------------------------')

    for gameName, file in fileList.items():
        file.write("return GData")
        file.close()
        strValue = "%s/%s/../../Script\\app\\game\\%s\\Data" % (curPath,gameName,gameName)
        codeLuaDir = os.path.abspath(strValue)
        codeLuaPath = "%s\\%s" % (codeLuaDir, "GData.lua")
        initPath = "%s\\%s.lua" % (curPath, gameName)
        shutil.move(initPath, codeLuaPath)


if __name__ == '__main__':

    args=sys.argv
    argCount=len(args)
    isLoadAll=True
    loadCfgIdList={}
    if(argCount > 1):
        if(args[1]=="all"):
            isLoadAll=True
        elif (args[1] == "game"):
            for i in range(2, argCount):
                num = int(args[i])
                loadCfgIdList[num] = True
        else:
            print(" argv is Error ")
            exit(1)

    # 导出所有配置文件到lua
    ExportAllCfgToLua(isLoadAll, loadCfgIdList)

    exit(0)
