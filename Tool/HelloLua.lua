function clone(object)
    local lookup_table = {}
    local function _copy(object)
        if type(object) ~= "table" then
            return object
        elseif lookup_table[object] then
            return lookup_table[object]
        end
        local newObject = {}
        lookup_table[object] = newObject
        for key, value in pairs(object) do
            newObject[_copy(key)] = _copy(value)
        end
        return setmetatable(newObject, getmetatable(object))
    end
    return _copy(object)
end

function split(input, delimiter)
    input = tostring(input)
    delimiter = tostring(delimiter)
    if (delimiter=='') then return false end
    local pos,arr = 0, {}
    -- for each divider found
    for st,sp in function() return string.find(input, delimiter, pos, true) end do
        table.insert(arr, string.sub(input, pos, st - 1))
        pos = sp + 1
    end
    table.insert(arr, string.sub(input, pos))
    return arr
end

local Transform = {
    Add = function(self,str)
        -- print("Transform Add,arg: "..str)
    end
}

local Command = {
    action = 1,
}

local Animation = {
    constValue = 99,
    print = function(self)
        print(self.constValue)
    end,
}

local Entity = {
    Add = function()
        print("Entity Add")
    end,
    print = function(self)
        print("Entity print ")
    end,
    Get = function(self,component)
        return component
    end,
    GetComponent = function(self,cmpName)
        return self.__components[cmpName]
    end,
    __components = {} --组件表
}

local mt = {
    __index = function(self,str)
        local strArray = split(str,"_")
        if next(strArray) == nil then return nil end 
        local cmpName = strArray[1]
        local funcName = strArray[2] 
        local component = self:GetComponent(cmpName)
        local func = nil

        if component == nil or funcName == nil then 
            print("[error] component is nil")
            return nil
        end  

        if component[funcName] then 
            func = function(self,...)
                return component[funcName](component,...)
            end 
        else
            func = function(self,...)
                return self[funcName](self,component,...)
            end
        end 
        return func
    end,
    __newindex = function(self,str,arg)
        print("mt [__newindex] "..str.." [arg] "..arg)
        local strArray = split(str,"_")
        local cmpName = strArray[1]
        local argName = strArray[2] 
        local component = self:GetComponent(cmpName)
        component[argName] = arg
    end,
    __call = function(self,arg)
        self.__components = arg
    end
}
setmetatable(Entity,mt)

function CreateEntity(id)
    local entity = {}
    entity.id = id
    entity["Transform"] = clone(Transform)
    entity["Command"] = clone(Command)
    entity["Animation"] = Animation
    return entity
end

local entity1 = CreateEntity(1)
local entity2 = CreateEntity(2)

function SetComponent_1(index)
    if index%2 == 0 then 
        Entity(entity1)
    else
        Entity(entity2)
    end
end
function SetComponent_2(index)
    local __components = {}
    if index%2 == 0 then 
        __components = entity1
    else
        __components = entity2
    end
    Entity.GetComponent = function(self,cmpName)
        return __components[cmpName]
    end
end

local sTime,eTime = 0,0
local time = 500000

sTime = os.clock()
for i=1,time,1 do 
    SetComponent_1(i)
    Entity:Transform_Add("hello Transform")
end
eTime = os.clock()
print("cost "..(eTime - sTime))

sTime = os.clock()
for i=1,time,1 do 
    SetComponent_2(i)
    Entity:Transform_Add("hello Transform")
end
eTime = os.clock()
print("cost "..(eTime - sTime))

local config = {"a","b","c","d","e","f"}
local tuples = "100101"

sTime = os.clock()
for id,comptName ipairs(config) do 
end
eTime = os.clock()
print("cost "..(eTime - sTime))

-- GetTempEntity(entity1)