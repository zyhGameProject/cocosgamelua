local UIBase = class("UIBase")
local G_ControlCenter = ControlCenter

UIBase.path = ""
function UIBase:ctor(...)
    self.bindNodes = {}
    self.rootNode = nil
    local registerClickNode,kwNodes = {}
    kwNodes,registerClickNode,self.rootNode = cc.CSLoader:createCustomNode(self.path)
    self:registerCSBEvent(registerClickNode)
    self:registerKWNodes(kwNodes)
    
    --@region 生命周期
    self:Awake(...)

    if self.InitDataNode then 
        self:InitDataNode()
    end 

    G_ControlCenter.dyData.Scene:addChild(self.rootNode)
    
    self:Start()
    --@endregion
end

function UIBase:Awake(...)
end

function UIBase:Start()
end

function UIBase:registerCSBEvent(tb)
    for node,eventName in pairs(tb) do 
        node:addClickEventListener(handler(self,self[eventName]))
    end
end

function UIBase:registerKWNodes(tb)
    for id,node in pairs(tb) do 
        self.bindNodes[node:getName()] = node
    end 
end

function UIBase:CloseUI()
    self.rootNode:removeFromParent()
    self.bindNodes = {}
    self.rootNode = nil
end

return UIBase

--[[ --ui模板
local G_UIUtils = UIUtils
local G_UIBase = UIBase
--访问kw节点 self.bindNodes[""]
--根节点 self.rootNode
local uiName = class("uiName",G_UIBase)
uiName.path = "csb/Plugin/PathEditorUI.csb"

function uiName:Awake(...)
end
--节点初始化数据
function uiName:InitDataNode()
end
--加入到场景后的调用事件
function uiName:Start()
end

--@region csb回调事件绑定

--@endregion

return uiName

]]
