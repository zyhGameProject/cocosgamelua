cc.exports.Plugin = require "plugin.init"

cc.exports.ControlCenter = require "app.ControlCenter"

cc.exports.UIBase = require "app.base.UIBase"

cc.exports.UIUtils = require "app.tool.UIUtils"
cc.exports.StringUtils = require "app.tool.StringUtils"
cc.exports.MessageUtils = require "app.tool.MessageUtils"
cc.exports.MathUtils = require "app.tool.MathUtils"

cc.exports.GameManager = require "app.game.GameManager"

