--[[
    luaide  模板位置位于 Template/FunTemplate/NewFileTemplate.lua 其中 Template 为配置路径 与luaide.luaTemplatesDir
    luaide.luaTemplatesDir 配置 https://www.showdoc.cc/web/#/luaide?page_id=713062580213505
    author:{author}
    time:2019-05-08 17:57:42
]]
local G_ControlCenter = ControlCenter
local G_UIUtils = UIUtils
local GameMenuUI = class("GameMenuUI",UIBase)
GameMenuUI.path = "csb/Lobby/GameMenuUI.csb"

function GameMenuUI:Awake()
    self:initGameMenu()
end

function GameMenuUI:initGameMenu()
    print("GameMenuUI:initGameMenu()")
    local itemNode = self.bindNodes["KW_GRP_GameItem"]
    local listNode = self.bindNodes["KW_LVW_GameMenu"]
    local node,btnNode,titleNode = {},{},{}
    listNode:removeAllChildren()
    for tag,info in pairs(G_ControlCenter.scData.GameInfoList) do
        node = itemNode:clone()
        btnNode = G_UIUtils.GetNode("Btn_JoinGame",node)
        titleNode =G_UIUtils.GetNode("title",node) 
        
        titleNode:setString(info.name)
        btnNode.GameInfo = info
        listNode:pushBackCustomItem(node)
    end 
end

function GameMenuUI:onBtnJoinGame(sender)
    G_ControlCenter.ChangeScene(G_ControlCenter.scData.SceneIDList.Game,sender.GameInfo)
end

function GameMenuUI:onBtnBack(sender)
    G_UIUtils.CloseUI(self)
end

return GameMenuUI