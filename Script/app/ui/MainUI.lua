local scheduler = cc.Director:getInstance():getScheduler()

local MainUI = class("MainUI",UIBase)
MainUI.path = "csb/Lobby/MainScene.csb"

function MainUI:Start()
   -- UIUtils.ShowUI(nil,ControlCenter.LoadPluginLuaFile("PathEditorUI","PathPlugin"))
--    local dispatcher = cc.Director:getInstance():getEventDispatcher()
--    local listener = cc.EventListenerCustom:create("event_renderer_recreated",handler(self,self.ShaderTest))
--    dispatcher:addEventListenerWithSceneGraphPriority(listener,self.rootNode)

    -- local _instance = self
    -- self.bindNodes["KW_SPR_Shader"]:runAction(cc.RepeatForever:create(cc.Sequence:create(cc.DelayTime:create(0.05),cc.CallFunc:create(function()
    --     _instance:ShaderTest( )
    -- end))))
end

function MainUI:onBtnStart(sender)
    UIUtils.ShowUI("GameMenuUI")
end

function MainUI:onBtnExit(sender)

end

function MainUI:ShaderTest( )
    -- 顶点shader
    local vertex = [[
        attribute vec4 a_position;
        attribute vec2 a_texCoord;
        attribute vec4 a_color;

        #ifdef GL_ES
        varying lowp vec4 v_fragmentColor;
        varying mediump vec2 v_texCoord;
        #else
        varying vec4 v_fragmentColor;
        varying vec2 v_texCoord;
        #endif

        void main()
        {
            gl_Position = CC_PMatrix * a_position;
            v_fragmentColor = a_color;
            v_texCoord = a_texCoord;
        }
    ]]

    -- 片段shader
    local fragment= [[
        /*
        #ifdef GL_ES 
        precision mediump float;  // shader默认精度为double，openGL为了提升渲染效率将精度设为float
        #endif 
        // varying变量为顶点shader经过光栅化阶段的线性插值后传给片段着色器
        varying vec4 v_fragmentColor;  // 颜色
        varying vec2 v_texCoord;       // 坐标
        void main(void) 
        { 
            // texture2D方法从采样器中进行纹理采样，得到当前片段的颜色值。CC_Texture0即为一个采样器
            vec4 c = texture2D(CC_Texture0, v_texCoord); 
            // c.rgb即是像素点的三种颜色，dot为点乘，vec3为经验值，可以随意修改
            float gray = dot(c.rgb, vec3(0.299, 0.587, 0.114)); 
            // shader的内建变量，表示当前片段的颜色
            gl_FragColor.xyz = vec3(gray); 
            // a为透明度
            gl_FragColor.a = c.a; 
        }
        */

        varying vec4 v_fragmentColor;  
        varying vec2 v_texCoord;  
        
        uniform sampler2D u_normalMap;  
        //uniform float timeFactor;  // uniform 定义的值即为传入的值，此值不可更改
        
        
        vec3 waveNormal(vec2 p) {  
            vec3 normal = texture2D(u_normalMap, p).xyz;  
            normal = -1.0 + normal * 2.0;  
            return normalize(normal);  
        }  
        
        void main() {  
            float timeFactor = 0.1;
            float offsetFactor = 0.5;//0.5
            float refractionFactor = 0.7;

            // simple UV animation
            vec3 normal = waveNormal(v_texCoord + vec2(CC_Time.y * timeFactor, CC_Time.x * timeFactor));

            // simple calculate refraction UV offset
            vec2 p = -1.0 + 2.0 * v_texCoord;//
            vec3 eyePos = vec3(0, 0, 10); //眼睛位置 位于中心点正上方
            vec3 inVec = normalize(vec3(p, 0) - eyePos);
            vec3 refractVec = refract(inVec, normal, refractionFactor);  //根据入射向量，法线，折射系数计算折射向量
            vec2 v_texCoordN = v_texCoord;
            v_texCoordN += refractVec.xy * offsetFactor;
            //v_texCoordN.x -= CC_Time.y*timeFactor *0.2; //移动水面贴图，可选 0.6

            //gl_FragColor = texture2D(u_normalMap, v_texCoord);
            gl_FragColor = texture2D(CC_Texture0, v_texCoordN);
        }
    ]]
    local sprite = self.bindNodes["KW_SPR_Shader"]
    sprite:setGLProgram(nil)
    local glProgram = cc.GLProgramCache:getInstance():getGLProgram("water_shader")
    if not glProgram then
        glProgram = cc.GLProgram:createWithByteArrays(vertex, fragment)
        cc.GLProgramCache:getInstance():addGLProgram(glProgram, "water_shader")
    end
    local glProgramState = cc.GLProgramState:getOrCreateWithGLProgram(glProgram)
    local normalTexture = cc.TextureCache:getInstance():addImage("res/MainScene/water_normal.jpg")
    glProgramState:setUniformTexture("u_normalMap", normalTexture)
    sprite:setGLProgram(glProgram)
    sprite:setGLProgramState(glProgramState)

    
    
    -- sprite:getGLProgramState():setUniformFloat("timeFactor", 0.1)
    
    
    -- scheduler:scheduleScriptFunc(function()
    --     glProgramState:setUniformFloat("timeFactor", -0.5)
    -- end,5,false)

end

return MainUI