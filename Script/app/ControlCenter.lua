local ControlCenter = {}

ControlCenter.scData = {
    --可存在的场景标识
    SceneIDList = {
        None = 0,
        Lobby = 1,
        Loading = 2,
        Game = 3,
    },
    GameInfoList = {
        CardAndTown = {
            id = 1,name = "CardAndTown",
            gameGlobalFile = {
                BuildManager = "Manager.BuildManager",
                World = "GameWorld",
            }
        },
        War3 = {
            id = 2,name = "War3",
            gameGlobalFile = {
                MapManager = "MapManager",
                World = "GameWorld",
            }
        },
    },
    deltaTimeRata = 0.05,
}

ControlCenter.dyData = {
    --当前场景
    Scene = nil,
    --场景标识
    SceneID = ControlCenter.scData.SceneIDList.None,

    GameFileHead = "",
    UiFileHead = "",
    PackageLoaded = {}, --游戏场景加载的lua文件
}

function ControlCenter.ChangeScene(tag,info)
    local scene = cc.Scene:create()
    if ControlCenter.dyData.Scene == nil then --加载第一个场景
        cc.Director:getInstance():runWithScene(scene)
    else
        ControlCenter.dyData.Scene = nil
        UIUtils.CloseAllUI()
        cc.Director:getInstance():replaceScene(scene)
    end
    ControlCenter.dyData.Scene = scene
    ControlCenter.dyData.SceneID = tag
    if tag == ControlCenter.scData.SceneIDList.Lobby then 
        ControlCenter.dyData.GameFileHead = "app."
        ControlCenter.dyData.UiFileHead = "app.ui."
        UIUtils.ShowUI("MainUI")
    elseif tag == ControlCenter.scData.SceneIDList.Game then 
        print("ControlCenter.ChangeScene EnterGame "..info.name)
        ControlCenter.dyData.GameFileHead = "app.game."..info.name.."."
        ControlCenter.dyData.UiFileHead = ControlCenter.dyData.GameFileHead.."ui."
        GameManager:Enter(info)
    end 
end

--导入当前场景lua文件
function ControlCenter.LoadGameLuaFile(filename,head)
    head = head or ControlCenter.dyData.GameFileHead
    return require(head..filename)
end
--导入当前场景UI
function ControlCenter.LoadUiLuaFile(filename,head)
    head = head or ControlCenter.dyData.UiFileHead
    return require(head..filename)
end
--导入插件
--@filename: lua文件名
--@head: 所属插件
function ControlCenter.LoadPluginLuaFile(filename,head)
    local str = "plugin."..head.."."
    return require(str..filename)
end


--@region  程序计时
ControlCenter.runTimeArray = {}
ControlCenter.runTimeArray.Count = 0
function ControlCenter.StartTime(str)
    str = str or "Time_"..ControlCenter.runTimeArray.Count
    ControlCenter.runTimeArray[str] = ControlCenter.runTimeArray[str] or {}
    ControlCenter.runTimeArray[str].tick = os.clock()
end
function ControlCenter.EndTime(str)
    local TimeData = ControlCenter.runTimeArray[str]
    if TimeData then 
        ControlCenter.runTimeArray[str].allTime = (TimeData.allTime or 0) + os.clock() - TimeData.tick
        ControlCenter.runTimeArray[str].count = (TimeData.count or 0) + 1 
    end 
end
--清空数据 打印所有时间
function ControlCenter.PrintTime()
    ControlCenter.runTimeArray.Count = nil
    for str,timeData in pairs(ControlCenter.runTimeArray) do 
        print(str.." cost:"..timeData.allTime.." count:"..timeData.count)
    end 
    ControlCenter.runTimeArray = {}
    ControlCenter.runTimeArray.Count = 0
end
--@endregion

return ControlCenter