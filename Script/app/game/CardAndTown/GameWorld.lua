local scheduler = cc.Director:getInstance():getScheduler()

local G_ControlCenter = ControlCenter

local G_UIUtils = UIUtils
local G_MessageUtils = MessageUtils


local G_GameManager = GameManager

local G_BuildManager = Game.BuildManager

local G_GameConfig = Game.Config

local GameWorld = {}

--@region 生命周期
function GameWorld:Awake()
    self.seed = tonumber(tostring(os.time()):reverse():sub(1,6)) --随机数种子
    --开始的时间
    self.time = {hour = 8,min = 0}
    --消耗的时间
    self.costTime = 0
    --1秒:现实3分
    self.timeRate = 3 
    --刷新频率
    self.deltaTime = 0.03
    --预存坐标点
    self.mapPoints = {}
    --游戏系统性状态
    self.worldState = G_GameConfig.ENUM_WORLD_STATE.DEBUG
    --csb缓存池
    self.csbPool = {}
    self.csbPoolTick = {}
    --计时器
    self.timeSchedulerId = 0
end

function GameWorld:Enter()
    local layOut = G_UIUtils.CallUIObjectFun("MainUI","GetEventLayOut")
    G_GameManager:RegisterKeyEvent(layOut)
    G_GameManager:RegisterMouseMoveEvent(layOut)
    G_GameManager:RegisterMouseEvent(layOut)
    ECSManager:CreateEntity(G_GameConfig.ENUM_ENTITY.PLAYER,{
        isPlayer = true
    })
    self.timeSchedulerId = scheduler:scheduleScriptFunc(self.Update,self.deltaTime,false)
end
function GameWorld:Exit()
    scheduler:unscheduleScriptEntry(self.timeSchedulerId) 
    local spriteFrame  = cc.SpriteFrameCache:getInstance()
    spriteFrame:removeSpriteFrames()
    G_GameManager:Exit()
end
--@endregion

--@region 
--@desc 刷新
function GameWorld.Update(tick)
    Game.World.UpdateTime(tick*Game.World.timeRate)
    -- Game.World.UpdateCsbPool()
    ECSManager:Update(tick)
    -- ControlCenter.StartTime("test")
    -- local a = 1
    -- for i=1,1000000000,1 do 
    --     a = a + 1
    -- end 
    -- ControlCenter.EndTime("test")
    -- ControlCenter.PrintTime()
end

function GameWorld.UpdateTime(addTime)
    local timeData = {
        [1] = { type = "min",value = 60 },
        [2] = { type = "hour",value = 24}
    }
    local lerpTime = 0
    local count = #timeData
    local nextType = ""
    Game.World.costTime = Game.World.costTime + addTime
    Game.World.time.min = Game.World.time.min + addTime
    for index,data in ipairs(timeData) do 
        lerpTime = Game.World.time[data.type] - data.value
        if lerpTime >= 0 then
            Game.World.time[data.type] = lerpTime
            if index + 1 <= count then 
                nextType = timeData[index+1].type
                Game.World.time[nextType] = Game.World.time[nextType] + 1
            end 
        end 
    end 
    G_UIUtils.CallUIObjectFun("MainUI","UpdateViewTime",Game.World.time)
end
--@endregion

--@region Tool
--@desc获取随机数 保持种子不变
--@desc 只能在初始化的时候调用,运行过程中不允许调用
function GameWorld.GetRandom(n,m)
    math.randomseed(Game.World.seed)
    Game.World.seed = Game.World.seed + 1
    return math.random(n,m)
end
--@desc arg : entity ,arg ,obj, func
function GameWorld.AddEntityListener(entity,arg,obj,func)
    entity.listener = entity.listener or {}
    entity.listener[arg] = entity.listener[arg] or {}
    table.insert(entity.listener[arg],handler(obj,func))
end
--@endregion

--@region mapData
function GameWorld.GetUnUsedBuildByActType(actType)
    local pointlist = {}
    for _actType,pointArray in pairs(Game.World.mapPoints) do 
        if actType == nil or actType == _actType then 
            for _,pointData in ipairs(pointArray) do 
                if pointData.bUsed == false then 
                    return pointData
                end 
            end 
        end 
    end 
    return nil
end
--@desc 获取坐标点的位置 arg: actType,actPointType
--@desc actPointType: 动作类型(1位)..序列号(2位)
function GameWorld.GetActPointByType(actType,actPointType)
    local pointlist = {}
    for _actType,pointArray in pairs(Game.World.mapPoints) do 
        if actType == nil or actType == _actType then 
            for _,pointData in ipairs(pointArray) do 
                if pointData.type == actPointType then 
                    return pointData
                end 
            end 
        end 
    end 
    return nil
end
--@endregion

--@region csbPool
-- function GameWorld.createNode(path)
--     local strArray = string.split(path,"/")
--     local nameIndex = strArray[#strArray]
--     local cacheNode = Game.World.csbPool[nameIndex]
--     if cacheNode == nil then 
--         cacheNode = cc.CSLoader:createNode(path)
--     end 
--     Game.World.csbPoolTick[nameIndex] = 1
--     return cacheNode
-- end
-- function GameWorld.UpdateCsbPool()
--     if next(Game.World.csbPool) == nil then 
--         return 
--     end
--     local dellist = {}
--     for csb_name,tick in pairs(Game.World.csbPoolTick) do 
--         tick = tick + 1
--         if tick > 10 then 
--             table.insert(dellist,csb_name)
--         else
--             Game.World.csbPoolTick[csb_name] = tick
--         end 
--     end 

--     for _,csb_name in ipairs(dellist) do 
--         Game.World.csbPoolTick[csb_name] = nil
--         Game.World.csbPool[csb_name] = nil
--     end 
-- end
--@endregion

--@region  AnimationType API
--@endregion


return GameWorld
