local GData={}
GData.TabRes=require("app.game.CardAndTown.Data.plist")
GData.TabAnimation=require("app.game.CardAndTown.Data.animation")
GData.TabConstValue=require("app.game.CardAndTown.Data.constValue")
GData.TabMapPointType=require("app.game.CardAndTown.Data.pointType")
GData.TabMap=require("app.game.CardAndTown.Data.map")
GData.TabBuild=require("app.game.CardAndTown.Data.build")
GData.TabCivilisationInfo=require("app.game.CardAndTown.Data.civilisationInfo")
GData.TabBuildPower=require("app.game.CardAndTown.Data.buildPower")
return GData