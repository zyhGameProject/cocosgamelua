

--TabMap表

local TabMap=
{
	[1]=
	{
		type=1,
		name="townHouse",
	},
	[2]=
	{
		type=2,
		name="townHall",
	},
	[3]=
	{
		type=3,
		name="townShop",
	},
	[4]=
	{
		type=3,
		name="townHospital",
	},
	[5]=
	{
		type=2,
		name="farmland",
	},
	[6]=
	{
		type=2,
		name="loggerHouse",
	},
}

return TabMap
