

--TabCivilisationInfo表

local TabCivilisationInfo=
{
	[1]=
	{
		energyLevel=1,
		builds={
			[1]=1,
			[2]=1000,
			[3]=2000,
			[4]=3000,
			[5]=4000,
			[6]=5000,
			[7]=6000,
		},
		name="stoneAge",
	},
	[2]=
	{
		energyLevel=2,
		builds={
			[1]=1,
			[2]=1000,
			[3]=2000,
			[4]=3000,
			[5]=4000,
			[6]=5000,
			[7]=6001,
		},
		name="chineseAge",
	},
	[3]=
	{
		energyLevel=2,
		builds={
			[1]=1,
			[2]=1000,
			[3]=2000,
			[4]=3000,
			[5]=4000,
			[6]=5000,
			[7]=6002,
		},
		name="MagicAge",
	},
	[4]=
	{
		energyLevel=2,
		builds={
			[1]=1,
			[2]=1000,
			[3]=2000,
			[4]=3000,
			[5]=4000,
			[6]=5000,
			[7]=6003,
		},
		name="modernAge",
	},
}

return TabCivilisationInfo
