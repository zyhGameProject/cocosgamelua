

--TabAnimation表

local TabAnimation=
{
	[1]=
	{
		idel=2,
		obj="bodyAni",
		isRepeat=1,
		step={
			[1]=1,
			[2]=2,
			[3]=3,
			[4]=2,
			[5]=1,
		},
		str="move_%s_",
		time=  0.16,
		name="move_common",
	},
}

return TabAnimation
