

--TabBuildPower表

local TabBuildPower=
{
	[1]=
	{
		res="res/Game/CardAndTown/build/power/main_icons_0005.png",
		name="levelUp",
	},
	[2]=
	{
		res="res/Game/CardAndTown/build/power/main_icons_0004.png",
		name="destory",
	},
	[3]=
	{
		res="res/Game/CardAndTown/build/power/main_icons_0002.png",
		name="move",
	},
	[4]=
	{
		res="res/Game/CardAndTown/build/power/ico_sell_0001.png",
		name="sell",
	},
	[5]=
	{
		res="res/Game/CardAndTown/build/power/main_icons_0021.png",
		name="create",
	},
}

return TabBuildPower
