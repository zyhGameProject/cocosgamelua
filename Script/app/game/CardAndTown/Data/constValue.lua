

--TabConstValue表

local TabConstValue=
{
	DEFAULT_FARMER_NUM=
	{
		value="4",
	},
	DEFAULT_SOLIDIER_NUM=
	{
		value="4",
	},
	DEFAULT_WOODCUTTER_NUM=
	{
		value="1",
	},
	DEFAULT_DOCTOER_NUM=
	{
		value="1",
	},
	DEFAULT_CRAFTSMAN_NUM=
	{
		value="1",
	},
	START_CLOCKTIME=
	{
		value="08:00",
	},
	TIME_RATE=
	{
		value="24",
	},
	TIME_DELTATIME=
	{
		value="0.01",
	},
	DEFAULT_AGE_TYPE=
	{
		value="1",
	},
}

return TabConstValue
