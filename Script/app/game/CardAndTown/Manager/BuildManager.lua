
local scheduler = cc.Director:getInstance():getScheduler()
local G_StringUtils = StringUtils
local G_UIUtils = UIUtils

local G_TabBuild = Game.GData.TabBuild

local BuildManager = {}

BuildManager.mapGrids = {}
BuildManager.mapSize = {}
BuildManager.tileSize = {}

--地图相关
function BuildManager:InitMapGrid(mapSize,tileSize)
	self.tileSize = tileSize
	self.mapSize = mapSize
	local pixelCount = cc.size(
		mapSize.width,
		mapSize.height
	)
	for x=1,pixelCount.width,1 do 
		self.mapGrids[x] = {}
		for y=1,pixelCount.height,1 do 
			self.mapGrids[x][y] = {}
			self.mapGrids[x][y].index = cc.p(x,y)
			self.mapGrids[x][y].pos = BuildManager:SwitchIndexToCenter(x,y)
			self.mapGrids[x][y].isCollisible = false
		end 
	end 
end
--更新地图碰撞点
function BuildManager:UpdateObjectCollsible(pos,isCollisible)
	local indexTab = self:SwitchPosToIndex(pos.x,pos.y)
	self.mapGrids[indexTab.x][indexTab.y].isCollisible = isCollisible
	if isCollisible then 
		G_UIUtils.CallUIObjectFun("MainUI","NodeFollowMouse")
		-- print(string.format("UpdateObjectCollsible cc.p(%d,%d)",pos.x,pos.y))
	end
end
--更新地图碰撞区域
function BuildManager:UpdateRectCollsible(pos,size,isCollisible)
	local pixelCount = self:SwitchSizeToCount(size.width,size.height)
	local indexTab = self:SwitchPosToIndex(pos.x,pos.y)
	indexTab.x = indexTab.x - math.ceil(pixelCount.width/2)
	indexTab.y = indexTab.y - math.ceil(pixelCount.height/2)
	for x=1,pixelCount.width,1 do 
		for y=1,pixelCount.height,1 do 
			local center = self:SwitchIndexToCenter(indexTab.x+x,indexTab.y+y)
			self:UpdateObjectCollsible(center,isCollisible)
		end
	end
end

--@return 发生碰撞的索引和是否可建造
function BuildManager:GetRectGridArray(pos,size)
	local indexTab = self:SwitchPosToIndex(pos.x-size.width/2,pos.y-size.height/2)
	local pixelCount = self:SwitchSizeToCount(size.width,size.height)

	local gridArray = {}
	local grid = {}
	local isCollisible = false
	for x=1,pixelCount.width,1 do 
		for y=1,pixelCount.height,1 do 
			local yList = self.mapGrids[indexTab.x+x] or {}
			local grid = yList[indexTab.y+y]
			if grid == nil then 
				isCollisible = true
			else
				isCollisible = grid.isCollisible
			end
			if isCollisible then 
				table.insert(gridArray,{ 
					pos = self:SwitchIndexToCenter(x,y),
					index = cc.p(x,y),
				})
			end 
		end 
	end
	return gridArray
end
--转换为格子个数
function BuildManager:SwitchSizeToCount(width,height)
	return cc.size(width/self.tileSize.width,height/self.tileSize.height)
end
--转换为数组坐标
function BuildManager:SwitchPosToIndex(x,y)
    return cc.p(math.ceil(x/self.tileSize.width),math.ceil(y/self.tileSize.height))
end
--真实坐标转为中心点坐标
function BuildManager:SwitchPosToCenter(x,y)
    local indexPos = self:SwitchPosToIndex(x,y)
    return self:SwitchIndexToCenter(indexPos.x,indexPos.y)
end
--从数组坐标转换为格子中心坐标
function BuildManager:SwitchIndexToCenter(x,y)
    return cc.p(self.tileSize.width*(x-0.5),self.tileSize.height*(y-0.5))
end

return BuildManager