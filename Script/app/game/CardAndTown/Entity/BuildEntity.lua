local G_StringUtils = StringUtils
local G_MessageUtils = MessageUtils
local G_MathUtils = MathUtils

local G_ECSManager = ECSManager

local G_BuildManager = Game.BuildManager

local G_TabBuildPower = Game.GData.TabBuildPower

local G_GameConfig = Game.Config
local G_GameWorld = Game.World

local G_UIUtils = UIUtils

local BuildEntity = {}
BuildEntity.tuples = {"Object","Card"}
BuildEntity.dataConfig = {
    clickShowBtns = {}, --点击后可显示的操作
    usePos = cc.p(0,0), --使用的位置
    res = "", --实体地址
    costTime = 0, --使用耗时
    colliderSize = cc.size(0,0) --建造区域
}
--初始化数据(默认数据)
function BuildEntity:InitData(entity)
    self.gameObject = cc.CSLoader:createNode("csb/Game/CardAndTown/BuildNode.csb")
    local _usePos = self.dataConfig.usePos
    self.usePos = G_BuildManager:SwitchPosToCenter(_usePos.x,_usePos.y)
end
--初始化组件
function BuildEntity:InitComponent(entity)
    entity.Object_colliderSize = self.dataConfig.colliderSize
    entity.Object_gameObject = self.gameObject
    entity.Card_costTime = self.dataConfig.costTime
    entity.Card_type = G_GameConfig.ENUM_CARD_TYPE.BUILD
    entity.Card_state = G_GameConfig.ENUM_CARD_STATE.START
    local enumCardState = G_GameConfig.ENUM_CARD_STATE
    entity:Card_RegisterCallBack(enumCardState.START,handler(entity,self.OnCardBegin))
    entity:Card_RegisterCallBack(enumCardState.USE,handler(entity,self.OnCardUsed))
    entity:Card_RegisterCallBack(enumCardState.END,handler(entity,self.OnCardEnd))

    local spriteFrame = cc.SpriteFrameCache:getInstance()
    spriteFrame:addSpriteFrames("plist/Game/CardAndTown/build/build_power.plist","plist/Game/CardAndTown/build/build_power.png")
end
--初始化额外界面
function BuildEntity:InitView(entity)
end
--初始化游戏物体
function BuildEntity:InitObject(entity)
    local gameObject = self.gameObject
    local imgNode = G_UIUtils.GetNode("Img_BuildingRect",gameObject)
    local ImgLbrNode = G_UIUtils.GetNode("Img_percent",imgNode)
    local txtNode = G_UIUtils.GetNode("Txt_percent",gameObject)
    local colliderSize = entity.Object_colliderSize
    local showNode = G_UIUtils.GetNode("Node_Build",gameObject)
    --初始化模型和碰撞盒节点
    local showImgNode = G_UIUtils.GetNode("img",showNode)
    local colliderNode = G_UIUtils.GetNode("Grp_ColliderBox",showNode)
    showImgNode:ignoreContentAdaptWithSize(true)
    showImgNode:loadTexture(self.dataConfig.res,ccui.TextureResType.plistType)
    colliderNode:setContentSize(colliderSize)
    --初始化建造基站节点
    imgNode:setContentSize(colliderSize)
    --设置进度条和节点坐标位置
    ImgLbrNode:setPosition(cc.p(colliderSize.width/2,colliderSize.height/2+12))
    txtNode:setPosition(cc.p(colliderSize.width/2,colliderSize.height/2+15))
    gameObject:setPosition(self.usePos)
    --设置地图的碰撞区域
    G_BuildManager:UpdateRectCollsible(self.usePos,colliderSize,true)
    --设置建筑权限按钮
    local btnListNode = G_UIUtils.GetNode("btns",showNode)
    local powerCount = #self.dataConfig.clickShowBtns
    local pointArray = G_MathUtils.GetDivideCirclePoint(powerCount,colliderSize.height/2,cc.p(0,colliderSize.height/2))
    local buildItemNode = G_UIUtils.GetNode("Btn_Item",showNode)
    btnListNode:setPositionY(colliderSize.height/2)
    for i=1,#self.dataConfig.clickShowBtns,1 do
        local btn = buildItemNode:clone()
        local powerIndex = self.dataConfig.clickShowBtns[i]
        local powerData = G_TabBuildPower[powerIndex]
        local res = powerData.res --res/Game/CardAndTown/build/power/ico_sell_0001.png
        btnListNode:addChild(btn)
        btn:loadTextures(res,res,res,ccui.TextureResType.plistType)
        btn:setName(powerData.name)
        btn.index = powerIndex
        btn:setPosition(pointArray[i])
        btn:setVisible(true)
    end 
    showImgNode:addClickEventListener(function()
        
    end)
    --加入到场景中
    G_UIUtils.CallUIObjectFun("MainUI","AddNodeToLayer",gameObject,"KW_GRP_Objects",-self.usePos.y)
end

function BuildEntity.OnCardBegin(entity)
    entity.Card_endTime = G_GameWorld.costTime + entity.Card_costTime
end

function BuildEntity.OnCardUsed(entity)
    --更新读条信息
    local gameObject = entity.Object_gameObject
    local lbrNode = G_UIUtils.GetNode("Lbr_percent",gameObject)
    local txtNode = G_UIUtils.GetNode("Txt_percent",gameObject)
    local percent = (1-(entity.Card_endTime - G_GameWorld.costTime)/entity.Card_costTime)*100
    lbrNode:setPercent(percent)
    txtNode:setString(string.format("%d%%",percent))
end

function BuildEntity.OnCardEnd(entity)
    local gameObject = entity.Object_gameObject
    local delNode = G_UIUtils.GetNode("Node_Building",gameObject)
    local showNode = G_UIUtils.GetNode("Node_Build",gameObject)
    local showImgNode = G_UIUtils.GetNode("img",showNode)
    local colliderSize = entity.Object_colliderSize
    delNode:removeFromParent()
    showNode:setVisible(true)
end

return BuildEntity