local G_StringUtils = StringUtils
local G_MessageUtils = MessageUtils

local G_ECSManager = ECSManager

local G_GameConfig = Game.Config
local G_GameWorld = Game.World

local G_UIUtils = UIUtils

local PlayerEntity = {}
PlayerEntity.tuples = {"Input","Attribute","Command","Animation","Transform","Object"}
PlayerEntity.dataConfig = {
    resId = 1, --资源id
    isPlayer = false,
}
--初始化数据(默认数据)
function PlayerEntity:InitData(entity)
    --人物节点
    self.gameObject = cc.CSLoader:createNode("csb/Game/CardAndTown/PlayerNode.csb")
end
--初始化组件
function PlayerEntity:InitComponent(entity)
    entity:Animation_AddPlist(self.dataConfig.resId)
    entity.Object_gameObject = self.gameObject
    entity.Object_resId = self.dataConfig.resId
    entity.Attribute_isPlayer = self.dataConfig.isPlayer
    entity.Transform_position = cc.p(480,320)
    if self.dataConfig.isPlayer then 
        G_ECSManager:AddComponent(entity.id,"Input")
        G_MessageUtils:RegisterListener(cc.Handler.EVENT_KEYBOARD_PRESSED,entity.Input,entity.Input_Pressed)
        G_MessageUtils:RegisterListener(cc.Handler.EVENT_KEYBOARD_RELEASED,entity.Input,entity.Input_Released)
    else
        G_ECSManager:AddComponent(entity.id,"AI")
    end 
end
--初始化界面
function PlayerEntity:InitView(entity)
end
--初始化游戏物体
function PlayerEntity:InitObject(entity)
    local gameObject = entity.Object_gameObject
    gameObject:setPosition(cc.p(480,320))
    G_UIUtils.CallUIObjectFun("MainUI","AddNodeToLayer",gameObject,"KW_GRP_Objects")
end

return PlayerEntity