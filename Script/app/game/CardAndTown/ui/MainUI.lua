local scheduler = cc.Director:getInstance():getScheduler()
local G_PathPlugin = Plugin.PathPlugin
local G_ControlCenter = ControlCenter

local G_StringUtils = StringUtils
local G_MessageUtils = MessageUtils
local G_UIUtils = UIUtils
local G_UIBase = UIBase

local G_ECSManager = ECSManager

local G_BuildManager = Game.BuildManager
local G_GameManager = GameManager

local G_TabCivilisationInfo = Game.GData.TabCivilisationInfo
local G_TabBuild = Game.GData.TabBuild
local G_TabConstValue = Game.GData.TabConstValue
local G_GameConfig = Game.Config
local G_GameWorld = Game.World

--访问kw节点 self.bindNodes[""]
--根节点 self.rootNode
local MainUI = class("MainUI",G_UIBase)
MainUI.path = "csb/Game/CardAndTown/GameScene.csb"
function MainUI:Awake()

    self.selBuild = {}
end
--节点初始化数据
function MainUI:InitDataNode()
    --按钮防吞
    local node = self.bindNodes["KW_GRP_EVENT"]
    node:setSwallowTouches(false)
    --插入裁剪节点
    node = self.bindNodes["KW_GRP_BuildNode"]
    local showNode = G_UIUtils.GetNode("show",node)
    local clippingRectangleNode = cc.ClippingRectangleNode:create(cc.rect(0,0,1,1))
    showNode:addChild(clippingRectangleNode)
    clippingRectangleNode:setAnchorPoint(cc.p(0,0))
    clippingRectangleNode:setPosition(cc.p(0,0))
    self.bindNodes["clippingNode"] = clippingRectangleNode
    node:setVisible(false)
    
    self:LoadMap()
    self:UpdateViewTime(G_GameWorld.time)
    self:UpdateBuildList(tonumber(G_TabConstValue.DEFAULT_AGE_TYPE.value))
end
--加入到场景后的调用事件
function MainUI:Start()
    G_MessageUtils:RegisterListener(cc.Handler.EVENT_MOUSE_MOVE,self,self.NodeFollowMouse)
    -- G_MessageUtils:RegisterListener(cc.Handler.EVENT_MOUSE_DOWN,self,self.ClickMouseBtn)
    G_MessageUtils:RegisterListener(cc.Handler.EVENT_MOUSE_UP,self,self.ClickMouseBtn)
    -- G_UIUtils.ShowUI(nil,G_ControlCenter.LoadPluginLuaFile("PathEditorUI","PathPlugin"))
end

--加载地图
function MainUI:LoadMap()
    local mapContainer = self.bindNodes["KW_GRP_MAP"]
    local mapRes = "map/CardAndTown/map.tmx"
    local map = ccexp.TMXTiledMap:create(mapRes)
    
    G_BuildManager:InitMapGrid(map:getMapSize(),map:getTileSize())
    --初始化寻路地图
    G_PathPlugin.LoadPathFindMap(G_BuildManager.mapGrids)

    self.bindNodes["KW_GRP_MAP"]:addChild(map)
end
--更新时间界面
function MainUI:UpdateViewTime(timeData)
    local timeStr = string.format( "%02d:%02d",timeData.hour,timeData.min)
    G_UIUtils.setString(self.bindNodes["KW_TXT_Time"],timeStr)
end
function MainUI:GetEventLayOut()
	
    return self.bindNodes["KW_GRP_EVENT"]
end

--初始化文明可建造的建筑列表
function MainUI:UpdateBuildList(ageIndex)
    local buildViewNode = self.bindNodes["KW_GRP_BuildView"]
    local buildListNode = G_UIUtils.GetNode("Lvw_BuildList",buildViewNode)
    local buildItemNode = self.bindNodes["KW_GRP_BuildItem"]
    buildListNode:removeAllChildren()
    local ageInfo = G_TabCivilisationInfo[ageIndex]
    if ageInfo then 
        for _,buildIndex in ipairs(ageInfo.builds or {}) do 
            local buildInfo = G_TabBuild[buildIndex]
            local cloneNode = buildItemNode:clone()
            local iconNode = G_UIUtils.GetNode("icon",cloneNode)
            iconNode:loadTexture(buildInfo.res,ccui.TextureResType.plistType)
            cloneNode.buildInfo = buildInfo
            buildListNode:pushBackCustomItem(cloneNode)
        end 
    end 
end

--@region csb回调事件绑定
function MainUI:OnBtnUpdateBuildList(sender)
    local isNeedShow = sender:getTag() == 0 -- true
    local listNode = self.bindNodes["KW_GRP_BuildView"]
    local listNodeSize = listNode:getContentSize()
    local movedis = isNeedShow and listNodeSize.width or -listNodeSize.width
    local action1 = cc.MoveBy:create(0.5,cc.p(movedis,0))
    listNode = G_UIUtils.GetNode("Lvw_BuildList",listNode)
    sender:setTag((isNeedShow and 1 or 0))
    sender:setEnabled(false)
    if isNeedShow then 
        listNode:setVisible(true)
    end 
    listNode:runAction(cc.Sequence:create(action1,cc.CallFunc:create(function()
        sender:setEnabled(true)
        listNode:setVisible(isNeedShow)
    end)))
end
function MainUI:OnBtnSelectBuild(sender)
    local buildInfo = sender.buildInfo
    local buildPrefabNode = self.bindNodes["KW_GRP_BuildNode"]
    local imgNode = G_UIUtils.GetNode("img",buildPrefabNode)
    local showNode = G_UIUtils.GetNode("show",buildPrefabNode)
    imgNode:loadTexture(buildInfo.res,ccui.TextureResType.plistType)
    imgNode:ignoreContentAdaptWithSize(true)
    showNode:setContentSize(buildInfo.collider.width,buildInfo.collider.height)
    self.bindNodes["clippingNode"]:removeAllChildren()
    self.bindNodes["clippingNode"]:setClippingRegion(cc.rect(0,0,buildInfo.collider.width,buildInfo.collider.height))
    -- -- layOut:setAnchorPoints(cc.p(0,0.5))
    -- layOut:setBackGroundColorType(ccui.LayoutBackGroundColorType.solid)
    -- layOut:setBackGroundColor(cc.c3b(255,0,0))

    

    self.selBuild = buildInfo
    self.selBuild.isBuild = false
    buildPrefabNode:setPosition(cc.p(-1000,0))
    buildPrefabNode:setVisible(true)
end
--@endregion

--添加节点到场景当中
--有些父节点特殊处理
function MainUI:AddNodeToLayer(node,parent_name,zOrder)
    local nodelist = self.bindNodes[parent_name]
    zOrder = zOrder or 10
    if nodelist then 
        if nodelist.pushBackCustomItem then 
            nodelist:pushBackCustomItem(node,zOrder)
            return
        end 
        nodelist:addChild(node,zOrder)
    end 
end
function MainUI:RemoveNode(node,parent_name)
    local nodelist = self.bindNodes[parent_name]
    if nodelist then 
        node:removeFromParent()
        node:release()
    end 
end
--界面点击建造
function MainUI:ClickMouseBtn(pos,mouseType)
    local buildPrefabNode = self.bindNodes["KW_GRP_BuildNode"]
    local selBuildInfo = self.selBuild
    if next(selBuildInfo) ~= nil and pos.y > 120 then 
        local isDelListener = false
        --点击左键
        if mouseType == 0 then
            if selBuildInfo.isBuild then 
                print("开始建造")
                G_ECSManager:CreateEntity(G_GameConfig.ENUM_ENTITY.BUILD,{
                    usePos = pos,
                    res = selBuildInfo.res,
                    costTime = selBuildInfo.buildTime,
                    colliderSize = selBuildInfo.collider,
                    clickShowBtns = selBuildInfo.power,
                })
            else
                print("无法建造")
            end 
        elseif mouseType == 1 then 
            isDelListener = true
            print("取消建造")
        end

        if isDelListener then 
            buildPrefabNode:setVisible(false)
            self.selBuild = {}
        end 
    end
end
--建筑跟踪鼠标位置
function MainUI:NodeFollowMouse(mousePos)
    local buildPrefabNode = self.bindNodes["KW_GRP_BuildNode"]
    if next(self.selBuild) ~= nil then 
        mousePos = mousePos or G_UIUtils.GetPosition(buildPrefabNode)
        local centerPos = G_BuildManager:SwitchPosToCenter(mousePos.x,mousePos.y)
        local curPos = G_UIUtils.GetPosition(buildPrefabNode)
        local gridArray = G_BuildManager:GetRectGridArray(centerPos,self.selBuild.collider)
        self.bindNodes["clippingNode"]:removeAllChildren()
        buildPrefabNode:setPosition(centerPos)
        self.selBuild.isBuild = true
        if #gridArray ~= 0 then
            self.selBuild.isBuild = false
            for _,data in ipairs(gridArray) do 
                local name = data.index.x.."_"..data.index.y
                if self.bindNodes["clippingNode"]:getChildByName(name) == nil then 
                    local sprite = cc.Sprite:create("res/Game/CardAndTown/com/img/ImgbuildHide.png")
                    sprite:setPosition(data.pos)
                    sprite:setName(name)
                    self.bindNodes["clippingNode"]:addChild(sprite)
                end
            end 
        end
    end
end

return MainUI