local G_SpriteFrame = cc.SpriteFrameCache:getInstance()
local G_StringUtils = StringUtils

local G_TabRes = Game.GData.TabRes
local G_TabAnimation = Game.GData.TabAnimation

local Animation = {}
--单个动画数据
--[[
    [aniIndex]={
        frames={}, --帧列表
        lerpTime=0, --间隔时间
        isRepeat=false, --是否循环
        aniNodeName="", --播放的精灵名字
        resId=0,
    }
--]]
Animation.aniClipList = {}
--已导入模型列表
Animation.modeList = {}
--单例组件 只存在一个
Animation.isGlobal = true

function Animation:AddPlist(_resId)
    if self.modeList[_resId] == nil then 
        local modeData = G_TabRes[_resId]
        G_SpriteFrame:addSpriteFrames(modeData.plist,modeData.png)
        modeData.ref = 1
        self.modeList[_resId] = modeData
        if next(modeData.aniList) ~= nil then
            for cmd,aniIndex in pairs(modeData.aniList) do 
                if self.aniClipList[aniIndex] == nil then 
                    local aniClip = {}
                    local aniData = G_TabAnimation[aniIndex]
                    aniClip.frames = {}
                    for index,frame in ipairs(aniData.step) do 
                        aniClip.frames[index] = aniData.str..frame..".png"
                    end
                    aniClip.lerpTime = aniData.time
                    aniClip.isRepeat = aniData.isRepeat
                    aniClip.aniNodeName = aniData.obj
                    aniClip.idel = aniData.idel
                    self.aniClipList[aniIndex] = aniClip
                end 
            end  
        end 
    else
        self.modeList[_resId].ref = self.modeList[_resId].ref + 1
    end 
end
--是否存在可播放的动画
function Animation:GetAniData(_resId,_cmd)
    local modeData = self.modeList[_resId]
    if modeData then 
        local aniIndex = modeData.aniList[tostring(_cmd)]
        if aniIndex then 
            local aniData = clone(self.aniClipList[aniIndex])
            for index,_ in ipairs(aniData.frames) do 
                aniData.frames[index] = string.format(aniData.frames[index],_cmd)
                aniData.frames[index] = modeData.res..aniData.frames[index]
            end
            return aniData
        end 
    end 
    return nil
end


return Animation