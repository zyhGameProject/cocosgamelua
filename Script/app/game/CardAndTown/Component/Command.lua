local G_GameConfig = Game.Config
local Command = {}

--[[
    --动作格式
    [ENUM_COMMAND] = action,
    --动画格式
    [ENUM_COMMAND] = resId --模型码
]]
Command.dataList = {}

Command.state = G_GameConfig.ENUM_PLAYER_STATE.IDEL
--节点播放的动画
Command.playAni = {}

function Command:Clean()
    self.dataList = {}
end

return Command