local Input = {}

Input.ClickKey = {}

function Input:Pressed(keyCode)
    self.ClickKey[keyCode] = true
end

function Input:Released(keyCode)
    self.ClickKey[keyCode] = nil
end

return Input