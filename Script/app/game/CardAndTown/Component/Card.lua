local G_GameConfig = Game.Config

local Card = {}
--卡牌类型
Card.type = 0
--使用结束时间
Card.endTime = 0
--使用时间
Card.costTime = 0
--开始使用卡牌时调用
Card.OnCardBegin = {}
--卡牌使用中调用
Card.OnCardUsed = {}
--卡牌使用时调用
Card.OnCardEnd = {}
--状态
Card.state = G_GameConfig.ENUM_CARD_STATE.NONE

function Card:RegisterCallBack(state,callBack)
    if state == G_GameConfig.ENUM_CARD_STATE.START then 
        self.OnCardBegin = callBack
    elseif state == G_GameConfig.ENUM_CARD_STATE.USE then 
        self.OnCardUsed = callBack
    elseif state == G_GameConfig.ENUM_CARD_STATE.END then 
        self.OnCardEnd = callBack
    end 
end

return Card