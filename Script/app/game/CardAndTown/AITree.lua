local AITree = {}
AITree = {
    name = "Selector_Root",
    {
        name = "Selector_Action",
        { 
            name = "Sequence_Sleep",
            { name = "Condition_IsToSleep"},
            {
                name = "Selector_Sleep",
                { name = "Action_MoveToTarget"},
                { name = "Action_Sleep"}
            },
        },
        {
            name = "Sequence_Work",
            { name = "Condition_IsToWork"},
            {
                name = "Selector_Work",
                { name = "Action_MoveToTarget"},
                { name = "Action_Work"}
            },
        },
        { name = "Action_Idel"},
    }
}

return AITree