local G_GameManager = GameManager

local G_BuildManager = Game.BuildManager

local G_GameConfig = Game.Config
local G_GameWorld = Game.World

local TransformSystem = {}

function TransformSystem:GetTuples()
    return {"Transform","Command"}
end

function TransformSystem:Init()
end

function TransformSystem:Update(entity,tick)
    local data = entity.Command_dataList[G_GameConfig.ENUM_PLAYER_STATE.MOVE]
    if data then
        local speed = entity.Transform_speed*tick
        local moveDis = cc.p(data.lerpPos.x*speed,data.lerpPos.y*speed)
        local lastPos = entity.Transform_position
        local nextPos = cc.p(lastPos.x+moveDis.x,lastPos.y+moveDis.y)
        local actionData = {}
        actionData.GetAction = function(gameObject)
            local toPos = nextPos
            return TransformSystem.SyncTransfomr(gameObject,toPos)
        end
        actionData.cmd = data.dir
        entity.Command_dataList[G_GameConfig.ENUM_PLAYER_STATE.MOVE] = actionData
        actionData.isReplay = false
        if data.dir ~= entity.Transform_dir then 
            actionData.isReplay = true
        end 
        local lastCenterPos = G_BuildManager:SwitchPosToCenter(lastPos.x,lastPos.y)
        if math.abs(nextPos.x - lastCenterPos.x) >= G_BuildManager.tileSize.width/2
            or math.abs(nextPos.y - lastCenterPos.y) >= G_BuildManager.tileSize.height/2 then 
            --更新地图数据
            G_BuildManager:UpdateObjectCollsible(lastCenterPos,false)
            G_BuildManager:UpdateObjectCollsible(nextPos,true)
        end
        entity.Transform_position = nextPos
        entity.Transform_dir = data.dir
    end 
end

function TransformSystem.SyncTransfomr(gameObject,pos)
    local centerPos = G_BuildManager:SwitchPosToCenter(pos.x,pos.y)
    gameObject:setPosition(pos)
    gameObject:setLocalZOrder(-centerPos.y)
end

return TransformSystem