
local G_GameManager = GameManager
local G_GameConfig = Game.Config
local InputSystem = {}

function InputSystem:GetTuples()
    return {"Input","Command","Attribute"}
end

function InputSystem:Init()
end

function InputSystem:Update(entity,tick)
    entity:Command_Clean()

    local keyList = entity.Input_ClickKey
    local enumPlayerState = G_GameConfig.ENUM_PLAYER_STATE
    
    entity.Command_state = enumPlayerState.IDEL

    if next(keyList) ~= nil then
        for keyCode,_ in pairs(keyList) do 
            --处理玩家输入
            self:DealPlayerMove(keyCode,entity,enumPlayerState.MOVE)
        end
    end
end

function InputSystem:DealPlayerMove(keyCode,entity,state)
    local lerpPos = cc.p(0,0)
    local dir = 5
    local enumCommand = G_GameConfig.ENUM_COMMAND
    if keyCode == cc.KeyCode.KEY_W then 
        dir = enumCommand.MOVE_UP
        lerpPos = cc.p(0,1)
    elseif keyCode == cc.KeyCode.KEY_A then
        dir = enumCommand.MOVE_LEFT
        lerpPos = cc.p(-1,0)
    elseif keyCode == cc.KeyCode.KEY_D then
        dir = enumCommand.MOVE_RIGHT
        lerpPos = cc.p(1,0)
    elseif keyCode == cc.KeyCode.KEY_S then
        dir = enumCommand.MOVE_DOWN
        lerpPos = cc.p(0,-1)
    end
    --需要一直更新
    if dir ~= 5 then 
        entity.Command_state = state
        entity.Command_dataList[entity.Command_state] = {
            lerpPos = lerpPos,
            dir = dir
        }
    end
end


return InputSystem