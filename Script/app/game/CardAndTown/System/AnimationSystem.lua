local G_SpriteFrame  = cc.SpriteFrameCache:getInstance()
local G_UIUtils = UIUtils
local G_GameManager = GameManager

local G_GameConfig = Game.Config
local G_GameWorld = Game.World

local AnimationSystem = {}

function AnimationSystem:GetTuples()
    -- return {"Transform","Animation","Command","Object"}
    return {"Animation","Command","Object"}
end

function AnimationSystem:Init()

end

function AnimationSystem:Update(entity,tick)
    local dataList = entity.Command_dataList
    if next(dataList) ~= nil then
        for _type,data in pairs(dataList) do 
            if data.GetAction then 
                self:PlayMoveAction(_type,data,entity)
            end
        end
    else
        local playAni = entity.Command_playAni
        if next(playAni) ~= nil then 
            local gameObject = entity.Object_gameObject
            for name,idelFrame in pairs(playAni) do 
                local aniNode = {}
                local frame = G_SpriteFrame:getSpriteFrame(idelFrame)
                aniNode = G_UIUtils.GetNode(name,gameObject)
                aniNode:setSpriteFrame(frame)
                aniNode:stopAllActions()
            end 
            entity.Command_playAni = {}
        end 
    end 
end

function AnimationSystem:PlayMoveAction(_type,data,entity)
    if _type == G_GameConfig.ENUM_PLAYER_STATE.MOVE then
        local action = data.GetAction
        local gameObject = entity.Object_gameObject
        local resId = entity.Object_resId
        local aniData = entity:Animation_GetAniData(resId,data.cmd)
        local playAni = entity.Command_playAni
        local aniNode = G_UIUtils.GetNode(aniData.aniNodeName,gameObject)
        local ani = self:GetAnimation(aniData.frames,aniData.lerpTime,aniData.isRepeat,aniNode)
        if data.isReplay then 
            aniNode:stopAllActions()
            playAni[aniData.aniNodeName] = nil
        end 

        if playAni[aniData.aniNodeName] == nil then 
            aniNode:runAction(ani)
            playAni[aniData.aniNodeName] = aniData.frames[aniData.idel]
        end 

        local pos = G_UIUtils.GetPosition(gameObject)
        -- print(string.format("gameObject cc.p(%d,%d)",pos.x,pos.y))
        action(gameObject)
    end
end


--@frameArray: 需要播放的精灵表
--@perUnit: 帧间隔
--@isRepeat: 是否循环 (1:是)
function AnimationSystem:GetAnimation(frames,lerpTime,isRepeat)
    local ani = {}
    local animation = cc.Animation:create()
    for _,frameName in ipairs(frames) do 
        local frame = G_SpriteFrame:getSpriteFrame(frameName)
        animation:addSpriteFrame(frame)
    end 
    animation:setDelayPerUnit(lerpTime)          --设置两个帧播放时间   
    animation:setRestoreOriginalFrame(true)    --动画执行后还原初始状态
    ani = cc.Animate:create(animation)
    if isRepeat == 1 then 
        ani = cc.RepeatForever:create(ani)
    end 
    return ani
end

return AnimationSystem