

local G_GameManager = GameManager

local G_GameWorld = Game.World
local G_GameConfig = Game.Config

local CardSystem = {}

function CardSystem:GetTuples()
    return {"Card"}
end

function CardSystem:Init()
end

function CardSystem:Update(entity,tick)
    local state = entity.Card_state
    local callBack = {}

    if state == G_GameConfig.ENUM_CARD_STATE.START then 
        entity.Card_endTime = G_GameWorld.costTime + entity.Card_costTime
        entity.Card_state = G_GameConfig.ENUM_CARD_STATE.USE
        callBack = entity.Card_OnCardBegin
    elseif state == G_GameConfig.ENUM_CARD_STATE.USE then 
        if G_GameWorld.costTime > entity.Card_endTime then 
            entity.Card_state = G_GameConfig.ENUM_CARD_STATE.END
            callBack = entity.Card_OnCardEnd
        else
            entity.Card_state = G_GameConfig.ENUM_CARD_STATE.USE
            callBack = entity.Card_OnCardUsed
        end 
        
    end

    if callBack and type(callBack) == "function" then 
        callBack()
    end 
end

return CardSystem