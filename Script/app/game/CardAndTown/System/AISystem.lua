local G_ATree = Plugin.AITreePlugin
local G_PathPlugin = Plugin.PathPlugin

local G_ControlCenter = ControlCenter

local G_GameManager = GameManager
local G_GameWorld = Game.World
local G_GameConfig = Game.Config

local AISystem = {}

function AISystem:GetTuples()
    return {"AI","Command","Attribute"}
end

function AISystem:Init()
    local aiTree = G_ControlCenter.LoadGameLuaFile("AITree")
    G_ATree.LoadTabTree(aiTree)
    G_ATree.LoadTabFunc(self)
end

function AISystem:Update(entity,tick)
    G_ATree.SetFuncSelf(entity)
    G_ATree.ExcuteNode(entity:AI_get().rootNode)
end

--@region 条件节点
function AISystem.Condition_IsToWork(entity)
    local time = G_GameWorld.time
    if time.hour > 8 or time.hour < 20 then 
        --工作时间
        entity.Command_state = G_GameConfig.ENUM_PLAYER_STATE.WORK
        return G_ATree.STATE.SUCCESS
    end 
    entity.Command_state = G_GameConfig.ENUM_PLAYER_STATE.IDEL
    return G_ATree.STATE.FAILUER 
end

function AISystem.Condition_IsToSleep(entity)
    local time = G_GameWorld.time
    if time.hour <= 8 or time.hour >= 20 then 
        --休息时间
        entity.Command_state = G_GameConfig.ENUM_PLAYER_STATE.SLEEP
        return G_ATree.STATE.SUCCESS
    end 
    entity.Command_state = G_GameConfig.ENUM_PLAYER_STATE.IDEL
    return G_ATree.STATE.FAILUER 
end
--@endregion

--@region 动作节点
function AISystem.Action_MoveToTarget(entity)
    local curPos = entity.Transform_position
    local targetPos = entity.AI_targetPos
    local dis = math.abs(targetPos.x-curPos.x) + math.abs(targetPos.y - curPos.y)

    --抵达工作地点
    if dis < 2 then 
        return G_ATree.STATE.FAILUER
    elseif next(entity.AI_path) == nil then 
        --路径不存在
        local path = G_PathPlugin.GetPath(targetPos,curPos)
        path = path.parent
        entity.AI_path = path
    end 
    return G_ATree.STATE.RUNNING
end
--@endregion



return AISystem