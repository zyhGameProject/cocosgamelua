local Config={}
--组件表
Config.ENUM_COMPONENT = {
    --Data组件
    "Card",
    "Input",
    "AI",
    "Transform",
    "Attribute",
    "Command",
    --View组件
    "Animation",
    "Object",
}
--实体表
Config.ENUM_ENTITY = {
    --实体 = 卡牌类型？
    PLAYER = "PlayerEntity", --人物卡
    BUILD = "BuildEntity", --建筑卡
} 
--系统表
Config.ENUM_SYSTEM = {
    --Data
    "CardSystem",
    "InputSystem", 
    -- "AISystem",
    "TransformSystem",
    --View
    "AnimationSystem",
} 
--监听事件枚举
Config.ENUM_MESSAGE_EVENT = {
    
}
--单位状态枚举
Config.ENUM_PLAYER_STATE = {
    IDEL = 1,
    MOVE = 2,
    WORK = 3,
    SLEEP = 4,
}
--卡牌类型
Config.ENUM_CARD_TYPE = {
    BUILD = 1,
}
--卡牌状态
Config.ENUM_CARD_STATE = {
    NONE = 1, --空
    START = 2,
    USE = 3,
    END = 4,
}
--单位状态枚举
-- Config.ENUM_UNIT_STATE = {
--     MENTALITY = 1,
--     FOOD = 2,
--     STAMINA = 3,
-- }
-- 世界模式
Config.ENUM_WORLD_STATE = {
    NONE = 1,
    RUNNING = 2,
    DEBUG = 3,
}
--操作命令
--命名规则 头命令_子命令 = 命令码
Config.ENUM_COMMAND = {
    NONE = 0,
    MOVE_DOWN_LEFT = 1,
    MOVE_LEFT = 4,
    MOVE_DOWN = 2,
    MOVE_RIGHT = 6,
    MOVE_UP = 8,
    MOVE_UP_RIGHT = 9,
    ANIM = 10,
}

return Config