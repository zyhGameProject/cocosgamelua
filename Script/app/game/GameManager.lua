local G_ControlCenter = ControlCenter

local G_MessageUtils = MessageUtils

local GameManager = {}
function GameManager:Enter(info)
    --[[ --必要的文件
        Game = Game or {}
        local Config = {}
        --组件表
        Config.ENUM_COMPONENT = {} 
        --实体表
        Config.ENUM_ENTITY = {} 
        --系统表
        Config.ENUM_SYSTEM = {} 
        --监听事件枚举
        Config.ENUM_MESSAGE_EVENT = {
            KEY_PRESS = 1,
            KEY_REALSE = 2,
        }
        Game.Config = Config
    ]]
    cc.exports.Game = {} -- 游戏文件下所有的全局函数
    cc.exports.ECSManager = {} --框架全局函数
    local File = {
        globalFile = {
            GData = "Data.GData", --excel数据
            Config = "Config", --框架枚举
        },
        frameworkFile = "base.ECSManager",
        enterFile = "init",
    }
    --事件组
    self.listenerArry = {}
    --需要初始化的全局数据
    for key,fileName in pairs(File.globalFile) do
        Game[key] = G_ControlCenter.LoadGameLuaFile(fileName)
    end
    --需要额外初始化的全局数据
    if info.gameGlobalFile then 
        for key,fileName in pairs(info.gameGlobalFile) do
            Game[key] = G_ControlCenter.LoadGameLuaFile(fileName)
        end
    end 
    --初始化框架
    ECSManager = require("app.game."..(File.frameworkFile))
    ECSManager:Init(Game.Config.ENUM_COMPONENT,Game.Config.ENUM_SYSTEM)
    --程序入口
    G_ControlCenter.LoadGameLuaFile(File.enterFile)
end

function GameManager:Exit()
    package.loaded[ControlCenter.dyData.GameFileHead.."init"]=nil
    self:ClearEvent()
    ECSManager:Clear()
    G_MessageUtils:ClearListener()
    G_ControlCenter.ChangeScene(G_ControlCenter.scData.SceneIDList.Lobby)
end

--@region 注册公共事件
--@layOut: 绑定容器
--@obj: 调用对象
--@OnKeyPressed: 按下事件
--@OnKeyReleased: 松开事件
function GameManager:RegisterKeyEvent(layOut)
    local OnKeyPressed = function(keyCode,event)
        -- print("GameWorld.OnKeyPressed event"..keyCode)
        G_MessageUtils:OnEvent(cc.Handler.EVENT_KEYBOARD_PRESSED,keyCode,event)
    end
    local OnKeyReleased = function(keyCode,event)
        -- print("GameWorld.OnKeyReleased event"..keyCode)
        G_MessageUtils:OnEvent(cc.Handler.EVENT_KEYBOARD_RELEASED,keyCode,event)
    end
    -- 注册键盘事件
    local dispatcher = cc.Director:getInstance():getEventDispatcher()
    local listener = cc.EventListenerKeyboard:create()
    listener:registerScriptHandler(OnKeyPressed, cc.Handler.EVENT_KEYBOARD_PRESSED)
    listener:registerScriptHandler(OnKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED)
    dispatcher:addEventListenerWithSceneGraphPriority(listener,layOut)
	table.insert(self.listenerArry,listener)
end
--注册鼠标移动事件
function GameManager:RegisterMouseMoveEvent(layOut)
    local OnMouseMoved = function(touch)
        local location = touch:getLocation()
        local mouseType = touch:getMouseButton()
        --鼠标起始点在左上角
        location.y = display.height - location.y
        G_MessageUtils:OnEvent(cc.Handler.EVENT_MOUSE_MOVE,location,mouseType)
    end

    local dispatcher = cc.Director:getInstance():getEventDispatcher()
    local listener = cc.EventListenerMouse:create()
    --local location = touch:getLocation()
    listener:registerScriptHandler(OnMouseMoved,cc.Handler.EVENT_MOUSE_MOVE)
    dispatcher:addEventListenerWithSceneGraphPriority(listener,layOut)
    table.insert(self.listenerArry,listener)
end
--鼠标点击松开事件
function GameManager:RegisterMouseEvent(layOut)
    local OnMouseUp = function(touch)
        local location = touch:getLocation()
        local mouseType = touch:getMouseButton()
        --鼠标起始点在左上角
        location.y = display.height - location.y
        G_MessageUtils:OnEvent(cc.Handler.EVENT_MOUSE_UP,location,mouseType)
    end
	local OnMouseMoved = function(touch)
        local location = touch:getLocation()
        local mouseType = touch:getMouseButton()
        --鼠标起始点在左上角
        location.y = display.height - location.y
        G_MessageUtils:OnEvent(cc.Handler.EVENT_MOUSE_MOVE,location,mouseType)
    end
    local OnMouseDown = function(touch)
        local location = touch:getLocation()
        local mouseType = touch:getMouseButton()
        --鼠标起始点在左上角
        location.y = display.height - location.y
        G_MessageUtils:OnEvent(cc.Handler.EVENT_MOUSE_DOWN,location,mouseType)
    end
    local dispatcher = cc.Director:getInstance():getEventDispatcher()
    local listener = cc.EventListenerMouse:create()
    listener:registerScriptHandler(OnMouseDown,cc.Handler.EVENT_MOUSE_DOWN)
	listener:registerScriptHandler(OnMouseMoved,cc.Handler.EVENT_MOUSE_MOVE)
    listener:registerScriptHandler(OnMouseUp,cc.Handler.EVENT_MOUSE_UP)
    dispatcher:addEventListenerWithSceneGraphPriority(listener,layOut)
    table.insert(self.listenerArry,listener)
end

--点击事件
--[[function GameManager:RegisterTouchEvent(layOut)
    -- local OnMouseUp = function(touch)
    --     local location = touch:getLocation()
    --     local mouseType = touch:getMouseButton()
    --     --鼠标起始点在左上角
    --     location.y = display.height - location.y
    --     G_MessageUtils:OnEvent(cc.Handler.EVENT_MOUSE_UP,location,mouseType)
    -- end
    -- local OnMouseDown = function(touch)
    --     local location = touch:getLocation()
    --     local mouseType = touch:getMouseButton()
    --     --鼠标起始点在左上角
    --     location.y = display.height - location.y
    --     G_MessageUtils:OnEvent(cc.Handler.EVENT_MOUSE_DOWN,location,mouseType)
    -- end
    print("RegisterTouchEvent")
    local touchMoved = function(touch, event)
        local location = touch:getLocation()
        print(location.x..location.y)
        return true
    end
    local touchEnded = function(touch, event)
        local location = touch:getLocation()
        print(location.x..location.y)
        return true
    end
    local listener = cc.EventListenerTouchOneByOne:create()
    local eventDispatcher = layOut:getEventDispatcher()
    listener:registerScriptHandler(touchMoved,cc.Handler.EVENT_TOUCH_BEGAN)
    listener:registerScriptHandler(touchEnded,cc.Handler.EVENT_TOUCH_ENDED )
    eventDispatcher:addEventListenerWithSceneGraphPriority(listener, layOut)
end
--]]

function GameManager:ClearEvent()
    local eventCount = #self.listenerArry
    if eventCount > 0 then 
        local dispatcher = cc.Director:getInstance():getEventDispatcher()
        for _,v in ipairs(self.listenerArry) do 
            dispatcher:removeEventListener(v)
        end
        self.listenerArry = {}
    end 
end
--@endregion

return GameManager