
local G_ControlCenter = ControlCenter

local G_UIUtils = UIUtils
local G_UIBase = UIBase

local G_GameConfig = Game.Config
local G_GameWorld = Game.World

--访问kw节点 self.bindNodes[""]
--根节点 self.rootNode
local GameInitUI = class("GameInitUI",G_UIBase)
GameInitUI.path = "csb/Game/War3/GameInitUI.csb"

function GameInitUI:Awake(...)
end
--节点初始化数据
function GameInitUI:InitDataNode()
end
--加入到场景后的调用事件
function GameInitUI:Start()
end

--@region csb回调事件绑定
function GameInitUI:onBtnBack(sender)
    G_UIUtils.CloseUI(self)
    G_GameWorld:Exit()
    
end
--@endregion

return GameInitUI