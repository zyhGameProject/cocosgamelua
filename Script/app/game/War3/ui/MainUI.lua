local scheduler = cc.Director:getInstance():getScheduler()
local G_PathPlugin = Plugin.PathPlugin
local G_ControlCenter = ControlCenter

local G_StringUtils = StringUtils
local G_MessageUtils = MessageUtils
local G_UIUtils = UIUtils
local G_UIBase = UIBase

local G_ECSManager = ECSManager

local G_MapManager = Game.MapManager
local G_GameManager = GameManager

local G_TabUnit = Game.GData.TabUnit

local G_TabPowerRes = Game.GData.TabPowerRes

local G_GameConfig = Game.Config
local G_GameWorld = Game.World

--访问kw节点 self.bindNodes[""]
--根节点 self.rootNode
local MainUI = class("MainUI",G_UIBase)
MainUI.path = "csb/Game/War3/GameScene.csb"
local LayerTag = {
    None = 0,
    Trerrain = 1,
}
local attrGroupType = {
	None = 0,
	Attr = 1, -- 属性界面
	Build = 2, -- 建造界面 ...(isUpdateLine,)
	HeroAttr = 3, --英雄属性界面
}
function MainUI:Awake()
    self.selUnit = {
		pageInfo = {},
		entityId = -1,
		curPage = 1,
	}
    self.clickTag = LayerTag.None
    self.selBoxStart = cc.p(0,0)
	self.selBoxEnd = cc.p(0,0)
end
--节点初始化数据
function MainUI:InitDataNode()
    local node = {}
	local _self = self
    --按钮防吞
    -- local node = self.bindNodes["KW_GRP_EVENT"]
    -- node:setSwallowTouches(false)
    --插入裁剪节点
    -- node = self.bindNodes["KW_GRP_BuildNode"]
    -- local showNode = G_UIUtils.GetNode("show",node)
    -- local clippingRectangleNode = cc.ClippingRectangleNode:create(cc.rect(0,0,1,1))
    -- showNode:addChild(clippingRectangleNode)
    -- clippingRectangleNode:setAnchorPoint(cc.p(0,0))
    -- clippingRectangleNode:setPosition(cc.p(0,0))
    -- self.bindNodes["clippingNode"] = clippingRectangleNode
    -- node:setVisible(false)

    node = cc.DrawNode:create()
    node:setName("KW_Select_DrawNode")
    self.rootNode:addChild(node)
    self.bindNodes["KW_Select_DrawNode"] = node
	
	--权限按钮绑定
	local grpPower = G_UIUtils.GetNode("Grp_Power",self.bindNodes["KW_GRP_OperateUI"])
	local grpPowerNodes = G_UIUtils.GetNode("Grp_Power_Nodes",grpPower)
	for _,powerNode in pairs(grpPowerNodes:getChildren()) do 
		powerNode.cmd = 0
		node = G_UIUtils.GetNode("Grp_View",powerNode)
		node:addClickEventListener(handler(self,self.OnBtnPowerBtn))
	end

    -- node = G_UIUtils.GetNode("Grp_Left",self.bindNodes["KW_GRP_OperateUI"])
    -- node:setSwallowTouches(true)

    --cc.c4b(1,254,1,1)
    --[[local layOut = self.bindNodes["KW_GRP_TouchEvent"]
    local listener = cc.EventListenerTouchOneByOne:create()
    local eventDispatcher = cc.Director:getInstance():getEventDispatcher()--layOut:getEventDispatcher()
    listener:registerScriptHandler(handler(self,self.OnTouchBegan),cc.Handler.EVENT_TOUCH_BEGAN)
    listener:registerScriptHandler(handler(self,self.OnTouchMoved),cc.Handler.EVENT_TOUCH_MOVED)
    listener:registerScriptHandler(handler(self,self.OnTouchesEnd),cc.Handler.EVENT_TOUCH_ENDED )
    eventDispatcher:addEventListenerWithSceneGraphPriority(listener, layOut)--]]

    
    self:LoadMap()
   -- self:UpdateViewTime(G_GameWorld.time)
end
--加入到场景后的调用事件
function MainUI:Start()
	G_MessageUtils:RegisterListener(cc.Handler.EVENT_MOUSE_DOWN,self,self.OnMouseDown)
    G_MessageUtils:RegisterListener(cc.Handler.EVENT_MOUSE_MOVE,self,self.OnMouseMove)
    G_MessageUtils:RegisterListener(cc.Handler.EVENT_MOUSE_UP,self,self.OnMouseUp)
end

function MainUI:GetEventLayOut(str)
    return self.bindNodes[str]
end
--加载地图
function MainUI:LoadMap()
    local mapContainer = self.bindNodes["KW_GRP_Terrain"]
    local mapRes = "map/War3/map.tmx"
    local map = ccexp.TMXTiledMap:create(mapRes)
    
    G_MapManager:InitMapGrid(map:getMapSize(),map:getTileSize())
    --初始化寻路地图
    G_PathPlugin.LoadPathFindMap(G_MapManager.mapGrids)

    self.bindNodes["KW_GRP_Terrain"]:addChild(map)

    local objects = map:getObjectGroups()
    local tPoints = {}
    for _,pointObj in pairs(objects) do 
        tPoints = pointObj:getObjects()
        for _,pointData in pairs(tPoints) do 
            G_GameWorld.mapPoints[tonumber(pointData.type)] = cc.p(pointData.x,pointData.y)
        end 
    end
end
--@region csb回调事件绑定
-- function MainUI:OnBtnClickTerrain(sender)
--     self.clickTag = LayerTag.Trerrain
-- end
--@endregion

--添加节点到场景当中
--有些父节点特殊处理
function MainUI:AddNodeToLayer(node,parent_name,zOrder)
    local nodelist = self.bindNodes[parent_name]
    zOrder = zOrder or 10
    if nodelist then 
        if nodelist.pushBackCustomItem then 
            nodelist:pushBackCustomItem(node,zOrder)
            return
        end 
        nodelist:addChild(node,zOrder)
    end 
end
function MainUI:RemoveNode(node,parent_name)
    local nodelist = self.bindNodes[parent_name]
    if nodelist then 
        node:removeFromParent()
        node:release()
    end 
end

--@region 框选事件注册
function MainUI:OnMouseDown(location, mouseType)
	--点击左键
	if mouseType == 0 then
		print("OnMouseDown")
		self.selBoxStart = location
		if self.selBoxStart.y > 240 then
			--取消所有选择单位
			print("ClearSelList")
			G_ECSManager:UpdateGComponentData("Mouse","ClearSelList")
		end
	--点击右键	
	elseif mouseType == 1 then 
		G_ECSManager:UpdateGComponentData("Mouse","SetRightClickData",location,G_GameConfig.RIGHTMOUSE_CLICKTYPE.POSITION)
	end
end

function MainUI:OnMouseMove(location, mouseType)
	if self.selBoxStart.x ~= 0 or self.selBoxStart.y ~= 0 then 
		self.selBoxEnd = location
		self:DrawSelectBox(self.selBoxStart,self.selBoxEnd)
	end
end

function MainUI:OnMouseUp(location, mouseType)
	if mouseType == 0 then
		print("OnMouseUp")
		self.selBoxStart = cc.p(0,0)
		self.selBoxEnd = cc.p(0,0)
		local drawNode = self.bindNodes["KW_Select_DrawNode"]
		if drawNode then 
			drawNode:clear()
		end
	end
end
--绘制选择框
function MainUI:DrawSelectBox(startPos, endPos)
	--self.bindNodes["KW_Select_DrawNode"]
	local drawNode = self.bindNodes["KW_Select_DrawNode"]
	if drawNode then 
		local startXendY = cc.p(startPos.x,endPos.y)
		local endXstartY = cc.p(endPos.x,startPos.y)
		--drawLine(cc.p(grid.index.x*16,0),cc.p(grid.index.x*16,display.height),cc.c4b(0,1,72,0.1))
		drawNode:clear()
		drawNode:drawLine(startPos,startXendY,cc.c4b(254,1,254,1))
		drawNode:drawLine(startPos,endXstartY,cc.c4b(254,1,254,1))
		drawNode:drawLine(endPos,startXendY,cc.c4b(254,1,254,1))
		drawNode:drawLine(endPos,endXstartY,cc.c4b(254,1,254,1))
	end
end
function MainUI:IsNeedUpdateMouseSel()
	return ( self.selBoxEnd.x ~= 0 or self.selBoxEnd.y ~= 0 ) and self.selBoxStart.y > 240
end
--是否在选择框中
function MainUI:IsInSelBox(pos)
	local startPos = self.selBoxStart
	local endPos = self.selBoxEnd
	if endPos.x == 0 and endPos.y == 0 then 
		return false
	end 
	local minX = math.min(startPos.x,endPos.x)
	local maxX = math.max(startPos.x,endPos.x)
	local minY = math.min(startPos.y,endPos.y)
	local maxY = math.max(startPos.y,endPos.y)
	return ((pos.x > minX and pos.x < maxX) and (pos.y > minY and pos.y < maxY))
end
--是否需要更新界面
function MainUI:IsNeedUpdateUI(entityId)
	if self.selUnit.entityId ~= entityId then 
		return true
	end
	return false
end
--更新展示界面的信息
function MainUI:UpdateEntityPageData(unitPower,entityId,curPage)
	local pageInfo = {}
	local data = {}
	local itemCount = 12
	curPage = curPage or 1
	if unitPower then 
		for id,infoStr in pairs(unitPower.powerList) do
			local page = math.ceil(id/itemCount)
			local index = math.fmod(id,itemCount)
			local strArray = string.split(infoStr,"_")
			pageInfo[page] = pageInfo[page] or {}
			data = {}
			data.cmd = tonumber(strArray[1])
			data.icon = G_TabPowerRes[tonumber(strArray[2])].res
			data.args = {}
			for i=3,#strArray,1 do 
				data.args[i-2] = strArray[i]
			end
			if index == 0 then index = itemCount end
			pageInfo[page][index] = data
		end
	end
	
	
	self.selUnit.entityId = entityId
	self.selUnit.pageInfo = pageInfo
	self.selUnit.curPage = curPage
end
--隐藏展示界面
function MainUI:HideShowGroup()
	--显示原界面
	if self.selUnit.entityId ~= -1 then 
		local GrpParent = self.bindNodes["KW_GRP_OperateUI"]
		local GrpPower = G_UIUtils.GetNode("Grp_Power",GrpParent)
		local GrpNodePowers = G_UIUtils.GetNode("Grp_Power_Nodes",GrpPower)
		local GrpProp = G_UIUtils.GetNode("Grp_Prop",GrpParent)
		
		--隐藏权限界面
		GrpNodePowers:setVisible(false)
		GrpProp:setVisible(false)
		self:UpdateGroupAttr(nil,attrGroupType.None)
		self.selUnit.pageInfo = {}
		self.selUnit.entityId = -1
		self.selUnit.curPage = 0
	end
end
--更新属性界面
function MainUI:UpdateGroupAttr(entityId,_type,args)
	local GrpAttr = G_UIUtils.GetNode("Grp_Attr",self.bindNodes["KW_GRP_OperateUI"])
	for _,node in pairs(GrpAttr:getChildren()) do 
		node:setVisible(false)
	end
	
	if _type == attrGroupType.None then 
		return 
	end
	
	if _type == attrGroupType.Attr then 
		local GrpAttrBase = G_UIUtils.GetNode("Grp_Attr_Base",GrpAttr)
		--更新攻击力
		if args.atkV then
			local txtAtk = G_UIUtils.GetNode("Txt_Attack",GrpAttrBase)
			txtAtk:setString(args.atkV)
		end
		
		--更新防御力
		if args.defV then
			local txtDef= G_UIUtils.GetNode("Txt_Defence",GrpAttrBase)
			txtDef:setString(args.defV)
		end
		GrpAttrBase:setVisible(true)
	elseif _type == attrGroupType.Build then 
		local isUpdateProcess = args.isUpdateProcess -- 是否更新单位表
		local isUpdateTaskLine = (args.taskLine and #args.taskLine > 0)
		local GrpAttrBuild = G_UIUtils.GetNode("Grp_Attr_Build",GrpAttr)
		local lbrNode = G_UIUtils.GetNode("Lbr_percent",GrpAttrBuild)
        local txtNode = G_UIUtils.GetNode("Txt_percent",GrpAttrBuild)
		--更新建筑单位
		if isUpdateTaskLine then 
			for i=1,5,1 do 
				local node = G_UIUtils.GetNode("Node_Unit_"..i,GrpAttrBuild)
				local data = args.taskLine[i]
				local iconNode = G_UIUtils.GetNode("icon",node)
				if data ~= nil then 
					iconNode:loadTexture(data.icon,ccui.TextureResType.localType)
				else
					iconNode:loadTexture("res/Game/War3/power/main_icons_0000.png",ccui.TextureResType.localType)
				end
			end
			args.isUpdateProcess = true
		end
		--更新进度条
		if isUpdateTaskLine and args.isUpdateProcess then 
			local data = args.taskLine[1]
			local percent = (1-(data.endTime - G_GameWorld.costTime)/data.buildTime)*100
			lbrNode:setPercent(percent)
			txtNode:setString(string.format("%d%%",percent))
		end
		GrpAttrBuild:setVisible(true)
	end
	
--	GrpAttr:setVisible(true)
end
--@endregion

--@region Grp_Power 操作相关
--更新单位权限界面
function MainUI:UpdateGroupPower()
	local GrpParent = self.bindNodes["KW_GRP_OperateUI"]
	local GrpRight = G_UIUtils.GetNode("Grp_Power",GrpParent)
	local GrpNodePowers = G_UIUtils.GetNode("Grp_Power_Nodes",GrpRight)
	if #self.selUnit.pageInfo > 0 then 
		local curPageInfo = self.selUnit.pageInfo[self.selUnit.curPage]
		for i=1,12,1 do 
			local node = G_UIUtils.GetNode("Node_Power_"..i,GrpNodePowers)
			if curPageInfo[i] and node then 
				local iconNode = G_UIUtils.GetNode("icon",node)
				iconNode:loadTexture(curPageInfo[i].icon,ccui.TextureResType.localType)
				node:setVisible(true)
				node.cmd = curPageInfo[i].cmd
				node.args = curPageInfo[i].args
				node.icon = curPageInfo[i].icon
			else
				node:setVisible(false)
			end
		end
	end
	GrpNodePowers:setVisible(true)
end
--点击权限按钮
function MainUI:OnBtnPowerBtn(sender)
	local _powerNode = sender:getParent()
	local cmd = _powerNode.cmd
	local args = _powerNode.args
	local icon = _powerNode.icon
	if cmd ~= 1 then 
		if cmd == 2 then 
			self:ExcutePower_JumpToPage(args[1])
		elseif cmd == 3 then 
			self:ExcutePower_BuildUnit(tonumber(args[1]),icon,self.selUnit.entityId)
		end
	end
end
--执行跳转页面命令
function MainUI:ExcutePower_JumpToPage(toPage)
	self.selUnit.curPage = tonumber(toPage)
	self:UpdateGroupPower()
end
--执行建筑生产单位单位命令
function MainUI:ExcutePower_BuildUnit(unitId,icon,entityId)
	local entity = G_ECSManager:GetEntityById(entityId)
	local unitData = G_TabUnit[unitId]
	entity:Build_AddBuildTask(icon,unitId,unitData.unitType,G_GameWorld.costTime+unitData.buildTime,unitData.buildTime)
	self:UpdateGroupAttr(entityId,attrGroupType.Build,{
		taskLine = entity.Build_taskLine
	})
end
--@endregion

return MainUI