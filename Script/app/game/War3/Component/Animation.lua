local G_SpriteFrame = cc.SpriteFrameCache:getInstance()
local G_StringUtils = StringUtils

local G_TabSpriteCache = Game.GData.TabSpriteCache
local G_TabAnimation = Game.GData.TabAnimation

local Animation = {}
--单个动画数据
--[[
    [aniIndex]={
        frames={}, --帧列表
        lerpTime=0, --间隔时间
        isRepeat=false, --是否循环
        aniNodeName="", --播放的精灵名字
        resId=0,
    }
--]]
Animation.aniClipList = {}
--已导入模型列表
Animation.frameList = {}
--单例组件 只存在一个
Animation.isGlobal = true

function Animation:AddSpriteFrames(_resId)
    local data = self.frameList[_resId]
    if data == nil then 
        data = G_TabSpriteCache[_resId]
        data.ref = 1
        G_SpriteFrame:addSpriteFrames(data.plist,data.png)
    else
        data.ref = data.ref + 1
    end
end
function Animation:AddAniClip(_aniId,_cmd)
    local aniData = G_TabAnimation[_aniId]
    local cacheData = G_TabSpriteCache[aniData.cacheId]
    local aniClipIndex = aniData.cacheId.."_".._cmd
    local data = self.aniClipList[aniClipIndex]
    if data == nil then 
        data = {}
        data.frames = {}
        for index,frame in ipairs(aniData.step) do 
            data.frames[index] = cacheData.sprite..string.format(aniData.head,frame)
        end
        data.lerpTime = aniData.time
        data.isRepeat = aniData.isRepeat
        data.aniNodeName = aniData.obj
        self.aniClipList[aniClipIndex] = data
    end 
end
function Animation:RemoveSpriteFrames(_resId)
    local data = self.frameList[_resId]
    data.ref = data.ref - 1
    if data.ref == 0 then 
        self.frameList[_resId] = nil 
        G_SpriteFrame:removeUnusedSpriteFrames()
    end 
end
--是否存在可播放的动画
function Animation:GetAniData(_resId,_cmd)
    local aniClipIndex = _resId.."_".._cmd
    return self.aniClipList[aniClipIndex]
end


return Animation