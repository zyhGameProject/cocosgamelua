local G_GameConfig = Game.Config

local Unit = {}

Unit.id = 0
Unit.hp = 0
Unit.mp = 0
--攻击范围
Unit.range = 0
--单位类型(英雄,建筑,士兵)
Unit.unitType = 1 
--种族类型
Unit.raceType = 1 
--攻击力
Unit.atkV = 1 
--攻击类型
Unit.atkT = 1 
--防御力
Unit.defV = 1 
--防御类型
Unit.defT = 1 
--移速
Unit.speed = 0
--建造耗时
Unit.buildTime = 0
--建造结束时间
Unit.buildEndTime = 0
--单位开始建造调用
Unit.OnEventCallBack = {}
--队伍标识
Unit.teamId = 0
--状态
Unit.state = G_GameConfig.ENUM_UNIT_STATE.NONE

function Unit:RegisterCallBack(state,callBack)
    self.OnEventCallBack[state] = callBack
end

function Unit:ExcuteEventCallBack(...)
	local callBack = self.OnEventCallBack[self.state]
	if callBack then 
		callBack(...)
	end
end


return Unit