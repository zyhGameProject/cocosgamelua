local Build = {}

--任务队列
Build.taskLine = {}
--添加数据到任务队列
function Build:AddBuildTask(icon,unitId,unitType,endTime,buildTime)
	local taskCount = #self.taskLine
	if taskCount < 5 then
		local data = {
			icon = icon, --图标
			unitId = unitId, -- 索引
			unitType = unitType, -- 类型
			endTime = endTime, --建造完成时间
			buildTime = buildTime, --建造需要的时间
		}
		if taskCount > 0 then
			data.endTime = self.taskLine[taskCount].endTime + data.buildTime
		end
		table.insert(self.taskLine,data) 
	else
		print("建造队列已满")
	end
end

function Build:RemoveBuildTask(index)
	table.remove(self.taskLine,index)
end


return Build