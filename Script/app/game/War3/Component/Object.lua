local Object = {}

--@region 玩家本身的数据
--当前坐标
Object.position = cc.p(0,0)
--@endregion

--目标坐标
Object.targetPos = cc.p(0,0)

--模型id
Object.modelId = 0
--游戏物体
Object.gameObject = {}
--碰撞区域
Object.colliderSize = cc.size(0,0)



return Object