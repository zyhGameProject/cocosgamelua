local G_GameConfig = Game.Config

local Mouse = {}
--左键选中
Mouse.selectEntityList = {}
Mouse.selectEntityCount = 0
--右键
Mouse.rightClickData = {} --坐标点或者单位Id
--单例组件 只存在一个
Mouse.isGlobal = true

local ENTITY_CLICK_TYPE = {
	IN_RANGE = 0, --在范围内
	IS_CLICK = 1, -- 被点击
}

function Mouse:ClearSelList()
	self.selectEntityList = {}
	self.selectEntityCount = 0
end
function Mouse:UpdateEntityMouseState(entityId,isSelect,state)
	local data = self.selectEntityList[entityId]
	local isUpdate = true
	if data then 
		if data.state > state then
			isUpdate = false
		end
	end
	
	if isUpdate then 
		if isSelect == true then 
			if data == nil then 
				self.selectEntityList[entityId] = {
					id = entityId,
					state = state
				}
				self.selectEntityCount = self.selectEntityCount + 1
			end
		elseif isSelect == false then 
			if data then 
				self.selectEntityCount = self.selectEntityCount - 1
				self.selectEntityList[entityId] = nil
			end
		end
	end
end
function Mouse:GetMouseState(id)
	return self.selectEntityList[id] ~= nil 
end

function Mouse:SetRightClickData(value,type)
	self.rightClickData = {
		value = value,
		type = type,
	}
end

function Mouse:GetRightClickData()
	local clickType = G_GameConfig.RIGHTMOUSE_CLICKTYPE
	local _type = self.rightClickData.type
	if _type == nil then return nil end
	if _type == clickType.TARGET then 
	
	elseif _type == clickType.POSITION then
		return self.rightClickData.value
	end
end
function Mouse:ClearRightClickData()
	self.rightClickData = {}
end

return Mouse