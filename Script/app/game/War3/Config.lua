local Config={}
--组件表
Config.ENUM_COMPONENT = {
	"AI",
	"Mouse",
    "Unit",
	"Build",
    "Animation",
    "Object",
}
--实体表
Config.ENUM_ENTITY = {
    SOLIDER = "SoliderEntity",
    BUILD = "BuildEntity",
} 
--系统表
Config.ENUM_SYSTEM = {
    --Data
	"MouseSystem",
	"AISystem",
    "UnitSystem",
    --View
    
} 

Config.ENUM_UNIT_STATE = {
    NONE = 0,
    BUILD_START = 1,
    BUILD_RUN = 2,
    BUILD_END = 3,
	UNIT_SELECTED = 5, --单位被选中
}

Config.ENUM_UNIT_ACTION = {
    NONE = 0,
    MOVE = 1,
    TEAM_MOVE = 2,	
}

Config.RIGHTMOUSE_CLICKTYPE = {
	TARGET = 1,
	POSITION = 2,
}

Config.ENUM_EVENT = {
    --开始建造的事件
    BUILD_START = "BUILD_START",
    --建造中的事件
    BUILD_USE = "BUILD_USE",
    --建造完成后的事件
    BUILD_END = "BUILD_END",
}

Config.ENUM_ANI_TYPE = {
    IDEL = 0,
    MOVE = 1,
    ATTACK = 2,
    DIED = 3,
}


return Config