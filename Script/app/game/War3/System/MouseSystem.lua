local G_UIUtils = UIUtils

local G_ECSManager = ECSManager

local G_GameManager = GameManager
local G_GameConfig = Game.Config
local G_GameWorld = Game.World

local MouseSystem = {}

function MouseSystem:GetTuples()
	return {"Mouse","Unit","Object"}
end

function MouseSystem:Init()
end

function MouseSystem:Update(comptArray,globalEntity,tick)
	local isSelected = nil
	local enumUnitState = G_GameConfig.ENUM_UNIT_STATE
	local count = 0
	local isUpdateMouseSel = G_UIUtils.CallUIObjectFun("MainUI","IsNeedUpdateMouseSel")
	
	for index,components in ipairs(comptArray) do 
		local entity = self:GetEntity(components)
		if isUpdateMouseSel then 
			isSelected = G_UIUtils.CallUIObjectFun("MainUI","IsInSelBox",entity.Object_position)
			globalEntity:Mouse_UpdateEntityMouseState(entity.id,isSelected,0)
		end 
		isSelected = globalEntity:Mouse_GetMouseState(entity.id)
		if isSelected then 
			entity.Unit_state = enumUnitState.UNIT_SELECTED
			entity.Object_targetPos = globalEntity:Mouse_GetRightClickData()
			count = count + 1
		elseif entity.Unit_state == enumUnitState.UNIT_SELECTED then 
			--entity.Unit_state = enumUnitState.None
		end
	end
	
	if count == 0 then
		G_UIUtils.CallUIObjectFun("MainUI","HideShowGroup")
    elseif count == 1 then 
	elseif count > 1 then 
		
	end
end

return MouseSystem
