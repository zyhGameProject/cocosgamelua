local G_ATree = Plugin.AITreePlugin
local G_PathPlugin = Plugin.PathPlugin

local G_ControlCenter = ControlCenter


local G_UIUtils = UIUtils

local G_ECSManager = ECSManager

local G_GameManager = GameManager
local G_GameConfig = Game.Config
local G_GameWorld = Game.World

local AISystem = {}

function AISystem:GetTuples()
	return {"AI","Mouse","Object"}
end

function AISystem:Init()
    G_ATree.LoadTabFunc(self)
end

function AISystem:Update(comptArray,globalEntity,tick)
	local rightClickData = globalEntity:Mouse_GetRightClickData()
	local selectEntityList = globalEntity.Mouse_selectEntityList
	local selectEntityCount = globalEntity.Mouse_selectEntityCount
	local isToRightTarget = (rightClickData ~= nil and selectEntityCount > 0)
	local endEntityIdList = {}
	local endPosList = {}
	for index,components in ipairs(comptArray) do 
		local entity = self:GetEntity(components)
		if isToRightTarget then 
			if selectEntityList[entity.id] then
				endEntityIdList[#endEntityIdList] = entity.id
				endPosList[#endPosList] = entity.Object_position
			end
		else
			
		end
	end
	
	if isToRightTarget then 
		local path = G_PathPlugin.GetPath(rightClickData.value,endPosList)
		print("asda")
	end
	
	--[[G_ATree.SetFuncSelf(entity)
    G_ATree.ExcuteNode(entity:AI_get().rootNode)--]]
	
	globalEntity:Mouse_ClearRightClickData()
end	

--@region 条件节点
function AISystem.Condition_IsToMouseTarget(entity)
    local time = G_GameWorld.time
    if time.hour > 8 or time.hour < 20 then 
        --工作时间
        return G_ATree.STATE.SUCCESS
    end 
    return G_ATree.STATE.FAILUER 
end
--@endregion

--@region 动作节点
function AISystem.Action_MoveToTarget(entity)
    local curPos = entity.Transform_position
    local targetPos = entity.AI_targetPos
    local dis = math.abs(targetPos.x-curPos.x) + math.abs(targetPos.y - curPos.y)

    --抵达工作地点
    if dis < 2 then 
        return G_ATree.STATE.FAILUER
    elseif next(entity.AI_path) == nil then 
        --路径不存在
        local path = G_PathPlugin.GetPath(targetPos,curPos)
        path = path.parent
        entity.AI_path = path
    end 
    return G_ATree.STATE.RUNNING
end
--@endregion

return AISystem
