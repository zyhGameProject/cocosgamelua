local G_UIUtils = UIUtils

local G_GameManager = GameManager

local G_GameWorld = Game.World
local G_GameConfig = Game.Config

local UnitSystem = {}

function UnitSystem:GetTuples()
    return {"Unit","Object"}
end

function UnitSystem:Init()
end

function UnitSystem:Update(comptArray,globalEntity,tick)	
	for _,components in ipairs(comptArray) do 
		local entity = self:GetEntity(components)
		local state = entity.Unit_state

		if state == G_GameConfig.ENUM_UNIT_STATE.BUILD_START then 
			entity.Unit_buildEndTime = G_GameWorld.costTime + entity.Unit_buildTime
			entity.Unit_state = G_GameConfig.ENUM_UNIT_STATE.BUILD_RUN
		elseif state == G_GameConfig.ENUM_UNIT_STATE.BUILD_RUN then 
			if G_GameWorld.costTime > entity.Unit_buildEndTime then 
				entity.Unit_state = G_GameConfig.ENUM_UNIT_STATE.BUILD_END
			else
				entity.Unit_state = G_GameConfig.ENUM_UNIT_STATE.BUILD_RUN
			end 
		end
		entity:Unit_ExcuteEventCallBack()
	end 
    
end

return UnitSystem