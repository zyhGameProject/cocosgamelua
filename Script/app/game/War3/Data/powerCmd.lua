

--TabPowerCmd表

local TabPowerCmd=
{
	[1]=
	{
		name="None",
	},
	[2]=
	{
		name="JumpToPage",
	},
	[3]=
	{
		name="BuildUnit",
	},
	[4]=
	{
		name="MoveToAndAttack",
	},
	[5]=
	{
		name="Upgrade",
	},
	[6]=
	{
		name="Cancel",
	},
	[7]=
	{
		name="UseSkill",
	},
	[9]=
	{
		name="CopyPower",
	},
}

return TabPowerCmd
