

--TabUnitPower表

local TabUnitPower=
{
	[50]=
	{
		powerList={
			[9]="2_3_2",
			[13]="3_1_1",
			[24]="2_2_1",
		},
	},
	[150]=
	{
		powerList={
			[9]="2_3_2",
			[13]="3_1_101",
			[24]="2_2_1",
		},
	},
}

return TabUnitPower
