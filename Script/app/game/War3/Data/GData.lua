local GData={}
GData.TabRaceType=require("app.game.War3.Data.raceType")
GData.TabOdType=require("app.game.War3.Data.odType")
GData.TabUnit=require("app.game.War3.Data.unit")
GData.TabAnimation=require("app.game.War3.Data.animation")
GData.TabSpriteCache=require("app.game.War3.Data.spriteCache")
GData.TabUnitType=require("app.game.War3.Data.unitType")
GData.TabUnitAni=require("app.game.War3.Data.unitAni")
GData.TabUnitPower=require("app.game.War3.Data.unitPower")
GData.TabPowerCmd=require("app.game.War3.Data.powerCmd")
GData.TabPowerRes=require("app.game.War3.Data.powerRes")
return GData