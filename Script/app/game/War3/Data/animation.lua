

--TabAnimation表

local TabAnimation=
{
	[1]=
	{
		head="soldier_lvl1_%04d.png",
		obj="model",
		isRepeat=1,
		cacheId=1,
		step={
			[1]=2,
			[2]=3,
			[3]=4,
			[4]=5,
			[5]=6,
		},
		time=  0.16,
		name="move_1",
	},
	[2]=
	{
		head="soldier_lvl1_%04d.png",
		obj="model",
		isRepeat=0,
		cacheId=1,
		step={
			[1]=7,
			[2]=11,
			[3]=14,
			[4]=17,
		},
		time=  0.16,
		name="attack_1",
	},
	[3]=
	{
		head="soldier_lvl1_%04d.png",
		obj="model",
		isRepeat=0,
		cacheId=1,
		step={
			[1]=18,
			[2]=19,
			[3]=20,
			[4]=21,
			[5]=22,
		},
		time=  0.16,
		name="died_1",
	},
	[4]=
	{
		head="soldier_lvl1_%04d.png",
		obj="model",
		isRepeat=1,
		cacheId=1,
		step={
			[1]=1,
			[2]=1,
		},
		time=  0.16,
		name="Idel_1",
	},
}

return TabAnimation
