

--TabSpriteCache表

local TabSpriteCache=
{
	[1]=
	{
		sprite="res/Game/War3/character/soldier/",
		name="solider_1",
		plist="plist/Game/War3/character/soldier_1.plist",
		png="plist/Game/War3/character/soldier_1.png",
	},
	[2]=
	{
		sprite="res/Game/War3/build/",
		name="build",
		plist="plist/Game/War3/build/build.plist",
		png="plist/Game/War3/build/build.png",
	},
}

return TabSpriteCache
