

--TabUnit表

local TabUnit=
{
	[1]=
	{
		buildTime= 10.00,
		collider={
			["width"]=16,
			["height"]=11,
		},
		name="A_近战兵",
		raceType=1,
		atkStr="1=20",
		hp=200.00,
		range= 16.00,
		cacheId=1,
		unitType=1,
		clickBox={
			["width"]=16,
			["height"]=28,
		},
		mp=100.00,
		model="res/Game/War3/character/soldier/soldier_lvl1_0001.png",
		defStr="1=2",
		speed= 32.00,
	},
	[2]=
	{
		buildTime= 10.00,
		collider={
			["width"]=8,
			["height"]=8,
		},
		name="A_远程兵",
		raceType=1,
		atkStr="1=15",
		hp=100.00,
		range= 48.00,
		unitType=1,
		clickBox={
			["width"]=0,
			["height"]=0,
		},
		mp=100.00,
		defStr="1=1",
		speed= 28.00,
	},
	[3]=
	{
		buildTime= 15.00,
		collider={
			["width"]=0,
			["height"]=0,
		},
		name="A_攻城车",
		raceType=1,
		atkStr="2=40",
		hp=500.00,
		range= 52.00,
		unitType=1,
		clickBox={
			["width"]=0,
			["height"]=0,
		},
		mp=  0.00,
		defStr="2=10",
		speed=  0.00,
	},
	[4]=
	{
		buildTime= 12.00,
		collider={
			["width"]=0,
			["height"]=0,
		},
		name="A_英雄_1",
		raceType=1,
		atkStr="3=50",
		hp=600.00,
		range= 16.00,
		unitType=2,
		clickBox={
			["width"]=0,
			["height"]=0,
		},
		mp=300.00,
		defStr="3=5",
		speed= 40.00,
	},
	[50]=
	{
		buildTime= 10.00,
		collider={
			["width"]=64,
			["height"]=22,
		},
		name="A_主城",
		raceType=1,
		atkStr="1=0",
		hp=200.00,
		range=  0.00,
		cacheId=2,
		unitType=3,
		clickBox={
			["width"]=100,
			["height"]=87,
		},
		mp=  0.00,
		model="res/Game/War3/build/AmazonTower_0006.png",
		defStr="2=20",
		speed=  0.00,
	},
	[51]=
	{
		buildTime=  9.00,
		collider={
			["width"]=0,
			["height"]=0,
		},
		name="A_兵营",
		raceType=1,
		atkStr="1=0",
		hp=400.00,
		range=  0.00,
		unitType=3,
		clickBox={
			["width"]=0,
			["height"]=0,
		},
		mp=  0.00,
		defStr="2=10",
		speed=  0.00,
	},
	[52]=
	{
		buildTime=  8.00,
		collider={
			["width"]=0,
			["height"]=0,
		},
		name="A_城墙",
		raceType=1,
		atkStr="1=0",
		hp=800.00,
		range=  0.00,
		unitType=3,
		clickBox={
			["width"]=0,
			["height"]=0,
		},
		mp=  0.00,
		defStr="2=20",
		speed=  0.00,
	},
	[53]=
	{
		buildTime=  7.00,
		collider={
			["width"]=0,
			["height"]=0,
		},
		name="A_防御塔",
		raceType=1,
		atkStr="2=30",
		hp=500.00,
		range= 48.00,
		unitType=3,
		clickBox={
			["width"]=0,
			["height"]=0,
		},
		mp=  0.00,
		defStr="2=10",
		speed=  0.00,
	},
	[100]=
	{
		buildTime=  6.00,
		collider={
			["width"]=0,
			["height"]=0,
		},
		name="A_英雄祭坛",
		raceType=1,
		atkStr="1=0",
		hp=400.00,
		range=  0.00,
		unitType=3,
		clickBox={
			["width"]=0,
			["height"]=0,
		},
		mp=  0.00,
		defStr="2=10",
		speed=  0.00,
	},
	[101]=
	{
		buildTime= 10.00,
		collider={
			["width"]=16,
			["height"]=11,
		},
		name="B_近战兵",
		raceType=2,
		atkStr="1=20",
		hp=200.00,
		range= 16.00,
		cacheId=1,
		unitType=1,
		clickBox={
			["width"]=16,
			["height"]=28,
		},
		mp=100.00,
		model="res/Game/War3/character/soldier/soldier_lvl1_0001.png",
		defStr="1=2",
		speed= 32.00,
	},
	[102]=
	{
		buildTime= 10.00,
		collider={
			["width"]=8,
			["height"]=8,
		},
		name="B_远程兵",
		raceType=2,
		atkStr="1=15",
		hp=100.00,
		range= 48.00,
		unitType=1,
		clickBox={
			["width"]=0,
			["height"]=0,
		},
		mp=100.00,
		defStr="1=1",
		speed= 28.00,
	},
	[103]=
	{
		buildTime= 15.00,
		collider={
			["width"]=0,
			["height"]=0,
		},
		name="B_攻城车",
		raceType=2,
		atkStr="2=40",
		hp=500.00,
		range= 52.00,
		unitType=1,
		clickBox={
			["width"]=0,
			["height"]=0,
		},
		mp=  0.00,
		defStr="2=10",
		speed=  0.00,
	},
	[104]=
	{
		buildTime= 12.00,
		collider={
			["width"]=0,
			["height"]=0,
		},
		name="B_英雄_End",
		raceType=2,
		atkStr="3=40",
		hp=500.00,
		range= 48.00,
		unitType=2,
		clickBox={
			["width"]=0,
			["height"]=0,
		},
		mp=300.00,
		defStr="3=5",
		speed= 40.00,
	},
	[150]=
	{
		buildTime= 10.00,
		collider={
			["width"]=64,
			["height"]=22,
		},
		name="B_主城",
		raceType=2,
		atkStr="1=0",
		hp=200.00,
		range=  0.00,
		cacheId=2,
		unitType=3,
		clickBox={
			["width"]=100,
			["height"]=87,
		},
		mp=  0.00,
		model="res/Game/War3/build/ArchMageTower.png",
		defStr="2=20",
		speed=  0.00,
	},
	[151]=
	{
		buildTime=  9.00,
		collider={
			["width"]=0,
			["height"]=0,
		},
		name="B_兵营",
		raceType=2,
		atkStr="1=0",
		hp=400.00,
		range=  0.00,
		unitType=3,
		clickBox={
			["width"]=0,
			["height"]=0,
		},
		mp=  0.00,
		defStr="2=10",
		speed=  0.00,
	},
	[152]=
	{
		buildTime=  8.00,
		collider={
			["width"]=0,
			["height"]=0,
		},
		name="B_城墙",
		raceType=2,
		atkStr="1=0",
		hp=800.00,
		range=  0.00,
		unitType=3,
		clickBox={
			["width"]=0,
			["height"]=0,
		},
		mp=  0.00,
		defStr="2=20",
		speed=  0.00,
	},
	[153]=
	{
		buildTime=  7.00,
		collider={
			["width"]=0,
			["height"]=0,
		},
		name="B_防御塔",
		raceType=2,
		atkStr="2=30",
		hp=500.00,
		range= 48.00,
		unitType=3,
		clickBox={
			["width"]=0,
			["height"]=0,
		},
		mp=  0.00,
		defStr="2=10",
		speed=  0.00,
	},
	[200]=
	{
		buildTime=  6.00,
		collider={
			["width"]=0,
			["height"]=0,
		},
		name="B_英雄祭坛",
		raceType=2,
		atkStr="1=0",
		hp=400.00,
		range=  0.00,
		unitType=3,
		clickBox={
			["width"]=0,
			["height"]=0,
		},
		mp=  0.00,
		defStr="2=10",
		speed=  0.00,
	},
}

return TabUnit
