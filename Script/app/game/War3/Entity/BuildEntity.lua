local G_UIUtils = UIUtils

local G_TabUnit = Game.GData.TabUnit
local G_TabUnitAni = Game.GData.TabUnitAni
local G_TabUnitPower = Game.GData.TabUnitPower

local G_ECSManager = ECSManager

local G_MapManager = Game.MapManager
local G_GameConfig = Game.Config
local G_GameWorld = Game.World

local BuildEntity = {}
BuildEntity.tuples = {"Animation","Object","Unit","Mouse","Build"}
BuildEntity.dataConfig = {
    unitId = 1,
    team = 1,
    point = cc.p(0,0),
    buildTime = -1, --有些不走配置表
}
--初始化数据(默认数据)
function BuildEntity:InitData(entity)
    self.gameObject = cc.CSLoader:createNode("csb/Game/War3/BuildNode.csb")
    self.unitInfo = G_TabUnit[self.dataConfig.unitId]
    self.unitAni = G_TabUnitAni[self.dataConfig.unitId]
    self.dataConfig.point = G_MapManager:SwitchPosToCenter(self.dataConfig.point.x,self.dataConfig.point.y)
    if self.dataConfig.buildTime < 0 then 
        self.dataConfig.buildTime = self.unitInfo.buildTime
    end 
	
end
--初始化组件
function BuildEntity:InitComponent(entity)
    local strArray = {}
    entity:Animation_AddSpriteFrames(self.unitInfo.cacheId)
    entity.Object_gameObject = self.gameObject
	entity.Object_position = self.dataConfig.point
    entity.Object_modelId = self.unitInfo.cacheId
    entity.Object_colliderSize = cc.size(self.unitInfo.collider.width,self.unitInfo.collider.height)
	entity.Unit_id = self.dataConfig.unitId
	entity.Unit_teamId = self.dataConfig.team
    entity.Unit_hp = self.unitInfo.hp
    entity.Unit_mp = self.unitInfo.mp
    entity.Unit_range = self.unitInfo.range
    entity.Unit_unitType = self.unitInfo.unitType
    entity.Unit_raceType = self.unitInfo.raceType
    entity.Unit_buildTime = self.dataConfig.buildTime
    strArray = self.unitInfo.atkStr.split("=")
    entity.Unit_atkT = tonumber(strArray[1])
    entity.Unit_atkV = tonumber(strArray[2])
    strArray = self.unitInfo.defStr.split("=")
    entity.Unit_defT = tonumber(strArray[1])
    entity.Unit_defT = tonumber(strArray[2])
    if self.unitAni.aniList then 
        for cmd,aniId in pairs(self.unitAni.aniList) do 
            entity:Animation_AddAniClip(aniId,cmd)
        end
    end 
	entity.Unit_state = G_GameConfig.ENUM_UNIT_STATE.BUILD_START
    local enumUnitState = G_GameConfig.ENUM_UNIT_STATE
    entity:Unit_RegisterCallBack(enumUnitState.BUILD_START,handler(entity,self.OnUnitBuildBegin))
    entity:Unit_RegisterCallBack(enumUnitState.BUILD_RUN,handler(entity,self.OnUnitBuild))
    entity:Unit_RegisterCallBack(enumUnitState.BUILD_END,handler(entity,self.OnUnitBuildEnd))
	entity:Unit_RegisterCallBack(enumUnitState.UNIT_SELECTED,handler(entity,self.OnUnitSelected))
end
--初始化额外界面
function BuildEntity:InitView(entity)
	local _entity = entity
	local gameObject = entity.Object_gameObject
	local showNode = G_UIUtils.GetNode("Node_Build",gameObject)
	local clickNode = G_UIUtils.GetNode("Grp_ClickBox",showNode)
	
	--[[clickNode:addClickEventListener(function(sender)
		_entity:Mouse_ClearSelList()
		_entity:Mouse_UpdateEntityMouseState(_entity.id,true,1)
	end)--]]
end
--初始化游戏物体
function BuildEntity:InitObject(entity)
    local gameObject = entity.Object_gameObject
    --local imgNode = G_UIUtils.GetNode("Img_BuildingRect",gameObject)
--    local ImgLbrNode = G_UIUtils.GetNode("Img_percent",imgNode)
--    local txtNode = G_UIUtils.GetNode("Txt_percent",gameObject)
    local colliderSize = entity.Object_colliderSize
    local showNode = G_UIUtils.GetNode("Node_Build",gameObject)
    --初始化模型和碰撞盒节点
    local showImgNode = G_UIUtils.GetNode("model",showNode)
    local colliderNode = G_UIUtils.GetNode("Grp_ColliderBox",showNode)
	local clickNode = G_UIUtils.GetNode("Grp_ClickBox",showNode)
	
    --showImgNode:ignoreContentAdaptWithSize(true)
    local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(self.unitInfo.model)
    showImgNode:setSpriteFrame(frame)
    colliderNode:setContentSize(colliderSize)
	if self.unitInfo.clickBox then 
		clickNode:setContentSize(cc.size(self.unitInfo.clickBox.width,self.unitInfo.clickBox.height))
	end
	
	
    --初始化建造基站节点
--    imgNode:setContentSize(colliderSize)
    --设置进度条和节点坐标位置
--    ImgLbrNode:setPosition(cc.p(colliderSize.width/2,colliderSize.height/2+12))
--    txtNode:setPosition(cc.p(colliderSize.width/2,colliderSize.height/2+15))
    
    --设置地图的碰撞区域
    -- G_MapManager:UpdateRectCollsible(self.usePos,colliderSize,true)
    self.gameObject:setPosition(self.dataConfig.point)
	
    --加入到场景中
    G_UIUtils.CallUIObjectFun("MainUI","AddNodeToLayer",gameObject,"KW_GRP_Objects",-self.dataConfig.point.y)
end

function BuildEntity.OnUnitBuildBegin(entity)
    entity.Unit_buildEndTime = G_GameWorld.costTime + entity.Unit_buildTime
end

function BuildEntity.OnUnitBuild(entity)
    --更新读条信息
    local gameObject = entity.Object_gameObject
--    local lbrNode = G_UIUtils.GetNode("Lbr_percent",gameObject)
--    local txtNode = G_UIUtils.GetNode("Txt_percent",gameObject)
    local percent = (1-(entity.Unit_buildEndTime - G_GameWorld.costTime)/entity.Unit_buildTime)*100
--    lbrNode:setPercent(percent)
--    txtNode:setString(string.format("%d%%",percent))
end

function BuildEntity.OnUnitBuildEnd(entity)
    local gameObject = entity.Object_gameObject
--    local delNode = G_UIUtils.GetNode("Node_Building",gameObject)
    local showNode = G_UIUtils.GetNode("Node_Build",gameObject)
    local showImgNode = G_UIUtils.GetNode("model",showNode)
    local colliderSize = entity.Object_colliderSize
--    delNode:removeFromParent()
    showNode:setVisible(true)
end

function BuildEntity.OnUnitSelected(entity)
	if entity.Mouse_selectEntityCount == 1 then 
		local showPage = 1
		local isNeedUpdateUI = G_UIUtils.CallUIObjectFun("MainUI","IsNeedUpdateUI",entity.id)
		local unitPower = G_TabUnitPower[entity.Unit_id]
		
		local taskLine = entity.Build_taskLine
		local taskData = taskLine[1]
		
		if isNeedUpdateUI then 
			G_UIUtils.CallUIObjectFun("MainUI","HideShowGroup")
			G_UIUtils.CallUIObjectFun("MainUI","UpdateEntityPageData",unitPower,entity.id)
			if unitPower then 
				--entity.Unit_state = G_GameConfig.ENUM_UNIT_STATE.NONE
				G_UIUtils.CallUIObjectFun("MainUI","UpdateGroupPower")
			end
			if #taskLine > 0 then 
					
			else
				G_UIUtils.CallUIObjectFun("MainUI","UpdateGroupAttr",entity.id,1,{
					defV = entity.Unit_defV,atkV = entity.Unit_atkV,
				})
			end
		end
		
		if taskData then 
			--建造完成
			if G_GameWorld.costTime > taskData.endTime then 
				if taskData.unitType == 1 then  --士兵
					print("建造士兵成功")	
					local unitData = G_TabUnit[taskData.unitId]
					G_ECSManager:CreateEntity(G_GameConfig.ENUM_ENTITY.SOLIDER,{
						unitId = taskData.unitId,
						team = entity.Unit_teamId,
						point = cc.p(400,300)
					})
				end
				entity:Build_RemoveBuildTask(1)
				if #taskLine == 0 then 
					G_UIUtils.CallUIObjectFun("MainUI","UpdateGroupAttr",entity.id,1,{
						defV = entity.Unit_defV,atkV = entity.Unit_atkV,
					})
					entity.Unit_state = G_GameConfig.ENUM_UNIT_STATE.NONE
				else
					G_UIUtils.CallUIObjectFun("MainUI","UpdateGroupAttr",entity.id,2,{
						taskLine = taskLine
					})
				end
			else
				G_UIUtils.CallUIObjectFun("MainUI","UpdateGroupAttr",entity.id,2,{
					taskLine = taskLine,
					isUpdateProcess = true,
				})
			end
		end
	end
end



return BuildEntity