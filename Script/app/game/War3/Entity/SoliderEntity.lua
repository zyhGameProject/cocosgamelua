local G_ATree = Plugin.AITreePlugin

local G_UIUtils = UIUtils

local G_ControlCenter = ControlCenter

local G_TabUnit = Game.GData.TabUnit
local G_TabUnitAni = Game.GData.TabUnitAni
local G_TabUnitPower = Game.GData.TabUnitPower

local G_ECSManager = ECSManager

local G_MapManager = Game.MapManager
local G_GameConfig = Game.Config
local G_GameWorld = Game.World

local SoliderEntity = {}
SoliderEntity.tuples = {"Animation","Mouse","Object","Unit","AI"}
SoliderEntity.dataConfig = {
    unitId = 1,
    team = 1,
	point = cc.p(0,0),
	buildTime = -1, --有些不走配置表
}
--初始化数据(默认数据)
function SoliderEntity:InitData(entity)
     --人物节点
     self.gameObject = cc.CSLoader:createNode("csb/Game/War3/SoliderNode.csb")
     self.unitInfo = G_TabUnit[self.dataConfig.unitId]
     self.unitAni = G_TabUnitAni[self.dataConfig.unitId]
	if self.dataConfig.buildTime < 0 then 
        self.dataConfig.buildTime = self.unitInfo.buildTime
    end 
end
--初始化组件
function SoliderEntity:InitComponent(entity)
    local strArray = {}
	--Animation
    entity:Animation_AddSpriteFrames(self.unitInfo.cacheId)
	--Object
    entity.Object_gameObject = self.gameObject
    entity.Object_modelId = self.unitInfo.cacheId
	entity.Object_position = self.dataConfig.point
	--Unit
	entity.Unit_id = self.dataConfig.unitId
	entity.Unit_teamId = self.dataConfig.team
    entity.Unit_hp = self.unitInfo.hp
    entity.Unit_mp = self.unitInfo.mp
    entity.Unit_range = self.unitInfo.range
    entity.Unit_unitType = self.unitInfo.unitType
    entity.Unit_raceType = self.unitInfo.raceType
    strArray = self.unitInfo.atkStr.split("=")
    entity.Unit_atkT = tonumber(strArray[1])
    entity.Unit_atkV = tonumber(strArray[2])
    strArray = self.unitInfo.defStr.split("=")
    entity.Unit_defT = tonumber(strArray[1])
    entity.Unit_defT = tonumber(strArray[2])
	entity.Unit.speed = self.unitInfo.speed
    if self.unitAni.aniList then 
        for cmd,aniId in pairs(self.unitAni.aniList) do 
            entity:Animation_AddAniClip(aniId,cmd)
        end
    end 
	local enumUnitState = G_GameConfig.ENUM_UNIT_STATE
	entity:Unit_RegisterCallBack(enumUnitState.UNIT_SELECTED,handler(entity,self.OnUnitSelected))
	--AI
	local aiTree = G_ControlCenter.LoadGameLuaFile("AITree")
    entity.AI_rootNode = G_ATree.ParseTabTree(aiTree)
end
--初始化额外界面
function SoliderEntity:InitView(entity)
	local _entity = entity
	local gameObject = entity.Object_gameObject
	local showNode = G_UIUtils.GetNode("Node_Solider",gameObject)
	local clickNode = G_UIUtils.GetNode("Grp_ClickBox",showNode)
	
	--[[clickNode:addClickEventListener(function(sender)
		_entity:Mouse_ClearSelList()
		_entity:Mouse_UpdateEntityMouseState(_entity.id,true,1)
	end)--]]
end
--初始化游戏物体
function SoliderEntity:InitObject(entity)
    local gameObject = entity.Object_gameObject
	local showNode = G_UIUtils.GetNode("Node_Solider",gameObject)
	
	local showImgNode = G_UIUtils.GetNode("model",showNode)
    local colliderNode = G_UIUtils.GetNode("Grp_ColliderBox",showNode)
	local clickNode = G_UIUtils.GetNode("Grp_ClickBox",showNode)
    local nameNode = G_UIUtils.GetNode("Img_Name",showNode)
	
	
	local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(self.unitInfo.model)
    showImgNode:setSpriteFrame(frame)
    colliderNode:setContentSize(colliderSize)
	if self.unitInfo.clickBox then 
		clickNode:setContentSize(cc.size(self.unitInfo.clickBox.width,self.unitInfo.clickBox.height))
	end
	
	
    G_UIUtils.GetNode("value",nameNode):setString(self.unitInfo.name)
	
    gameObject:setPosition(self.dataConfig.point)
    G_UIUtils.CallUIObjectFun("MainUI","AddNodeToLayer",gameObject,"KW_GRP_Objects")
end

function SoliderEntity.OnUnitSelected(entity)
	if entity.Mouse_selectEntityCount == 1 then 
		local showPage = 1
		local isNeedUpdateUI = G_UIUtils.CallUIObjectFun("MainUI","IsNeedUpdateUI",entity.id)
		local unitPower = G_TabUnitPower[entity.Unit_id]
		
		if isNeedUpdateUI then
			G_UIUtils.CallUIObjectFun("MainUI","HideShowGroup")
			G_UIUtils.CallUIObjectFun("MainUI","UpdateEntityPageData",unitPower,entity.id)
			if unitPower then 
				G_UIUtils.CallUIObjectFun("MainUI","UpdateGroupPower")
			end
			G_UIUtils.CallUIObjectFun("MainUI","UpdateGroupAttr",entity.id,1,{
				defV = entity.Unit_defV,atkV = entity.Unit_atkV,
			})
		end
	end
end


return SoliderEntity
