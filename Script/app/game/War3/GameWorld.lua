local G_Scheduler = cc.Director:getInstance():getScheduler()
local G_SpriteFrame = cc.SpriteFrameCache:getInstance()

local G_UIUtils = UIUtils

local G_GameManager = GameManager

local G_GameConfig = Game.Config

local GameWorld = {}

--@region 生命周期
function GameWorld:Awake()
    self.randSeed = tonumber(tostring(os.time()):reverse():sub(1,6)) --随机数种子
    --开始的时间
    self.time = {hour = 8,min = 0}
    --消耗的时间
    self.costTime = 0
    --1秒:现实3分
    self.timeRate = 3 
    --刷新频率
    self.deltaTime = 0.03
    --预存坐标点
    self.mapPoints = {}
    --计时器
    self.timeSchedulerId = 0
	--玩家标识
	self.playerTeamId = 1
end

function GameWorld:Enter()
     
    local tTeam = {
        [1] = {
            team = 1,
            color = cc.c3b(254,0,0),
            isPlayer = true,
            raceType = 1,
        },
        [2] = {
            team = 2,
            color = cc.c3b(0,254,0),
            isPlayer = false,
            raceType = 2,
        },
    }
    for i=1,#tTeam,1 do 
        local teamData = tTeam[i]
        local pos = self.mapPoints[i]
        local buildId = 1
        if teamData.raceType == 1 then 
            buildId = 50
        elseif teamData.raceType == 2 then 
            buildId = 150
        end 
        ECSManager:CreateEntity(G_GameConfig.ENUM_ENTITY.BUILD,{
            unitId = buildId,
            team = teamData.team,
            point = pos,
            buildTime = 0,
        })
    end 
    local layOut = G_UIUtils.CallUIObjectFun("MainUI","GetEventLayOut","KW_GRP_Terrain")
    -- G_GameManager:RegisterKeyEvent(layOut)
    -- G_GameManager:RegisterMouseMoveEvent(layOut)
    G_GameManager:RegisterMouseEvent(layOut)
    -- G_GameManager:RegisterTouchEvent(layOut)
    self.timeSchedulerId = G_Scheduler:scheduleScriptFunc(self.Update,self.deltaTime,false)
end
function GameWorld:Exit()
    G_Scheduler:unscheduleScriptEntry(self.timeSchedulerId) 
    G_SpriteFrame:removeSpriteFrames()
    G_GameManager:Exit()
end
--@endregion

--@region 
--@desc 刷新
function GameWorld.Update(tick)
    Game.World.UpdateTime(tick*Game.World.timeRate)
    -- Game.World.UpdateCsbPool()
    ECSManager:Update(tick)
    -- ControlCenter.StartTime("test")
    -- local a = 1
    -- for i=1,1000000000,1 do 
    --     a = a + 1
    -- end 
    -- ControlCenter.EndTime("test")
    -- ControlCenter.PrintTime()
end

function GameWorld.UpdateTime(addTime)
    local timeData = {
        [1] = { type = "min",value = 60 },
        [2] = { type = "hour",value = 24}
    }
    local lerpTime = 0
    local count = #timeData
    local nextType = ""
    Game.World.costTime = Game.World.costTime + addTime
    Game.World.time.min = Game.World.time.min + addTime
    for index,data in ipairs(timeData) do 
        lerpTime = Game.World.time[data.type] - data.value
        if lerpTime >= 0 then
            Game.World.time[data.type] = lerpTime
            if index + 1 <= count then 
                nextType = timeData[index+1].type
                Game.World.time[nextType] = Game.World.time[nextType] + 1
            end 
        end 
    end 
end
--@endregion

--@region Tool
--@desc获取随机数 保持种子不变
--@desc 只能在初始化的时候调用,运行过程中不允许调用
function GameWorld.GetRandom(n,m)
    math.randomseed(Game.World.randSeed)
    Game.World.randSeed = Game.World.randSeed + 1
    return math.random(n,m)
end
--@endregion


--@region commonFunc 公共方法
--根据命令执行权限
function GameWorld.ExcutePowerFunc(powerNode,powerId,args)
	
end

function GameWorld.IsPlayer(teamId)
	return teamId == Game.World.playerTeamId
end

--@endregion


return GameWorld
