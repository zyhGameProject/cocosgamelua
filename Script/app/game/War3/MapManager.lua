
local scheduler = cc.Director:getInstance():getScheduler()
local G_StringUtils = StringUtils
local G_UIUtils = UIUtils

local G_TabBuild = Game.GData.TabBuild

local MapManager = {}

MapManager.mapGrids = {}
MapManager.mapSize = {}
MapManager.tileSize = {}

--地图相关
function MapManager:InitMapGrid(mapSize,tileSize)
	self.tileSize = tileSize
	self.mapSize = mapSize
	local pixelCount = cc.size(
		mapSize.width,
		mapSize.height
	)
	for x=1,pixelCount.width,1 do 
		self.mapGrids[x] = {}
		for y=1,pixelCount.height,1 do 
			self.mapGrids[x][y] = {}
			self.mapGrids[x][y].index = cc.p(x,y)
			self.mapGrids[x][y].pos = MapManager:SwitchIndexToCenter(x,y)
			self.mapGrids[x][y].isCollisible = false
		end 
	end 
end
--转换为格子个数
function MapManager:SwitchSizeToCount(width,height)
	return cc.size(width/self.tileSize.width,height/self.tileSize.height)
end
--转换为数组坐标
function MapManager:SwitchPosToIndex(x,y)
    return cc.p(math.ceil(x/self.tileSize.width),math.ceil(y/self.tileSize.height))
end
--真实坐标转为中心点坐标
function MapManager:SwitchPosToCenter(x,y)
    local indexPos = self:SwitchPosToIndex(x,y)
    return self:SwitchIndexToCenter(indexPos.x,indexPos.y)
end
--真实坐标转为左下角坐标
function MapManager:SwitchPosToStart(x,y)
	local centerPos = self:SwitchPosToCenter(x,y)
    return cc.p(centerPos.x-self.tileSize.width/2,centerPos.y-self.tileSize.height/2)
end
--从数组坐标转换为格子中心坐标
function MapManager:SwitchIndexToCenter(x,y)
    return cc.p(self.tileSize.width*(x-0.5),self.tileSize.height*(y-0.5))
end

return MapManager