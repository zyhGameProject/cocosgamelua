local G_ControlCenter = ControlCenter
local G_MessageUtils = MessageUtils
local Component = require("app.game.base.Component")

local base = {
    id = 0,
    components = {},
    Add = function(self,component,tData)
        for argName,data in pairs(tData) do 
            if component[argName] then 
                local type = type(component[argName])
                if type == "table" then 
                    for k,v in pairs(data) do 
                        if component[argName][k] then 
                            component[argName][k] = component[argName][k] + v
                        end
                    end
                else
                    component[argName] = component[argName] + data
                end 
            end 
        end 
    end
}
local mt = {
    __index = function(self,str)
        local strArray = string.split(str,"_")
        local cmpName = strArray[1]
        local funcName = strArray[2] 
        local component = self.components[cmpName]
        local func = nil

        if component == nil then 
            print("[error] component " ..cmpName.." is nil")
            return nil
        end  

        if funcName == nil then 
            return component
        end 

        if component[funcName] then 
            if type(component[funcName]) == "function" then 
                func = function(self,...)
                    return component[funcName](component,...)
                end
            else
                func = component[funcName]
            end
        else
            func = function(self,...)
                return self[funcName](self,component,...)
            end
        end 
        return func
    end,
    __newindex = function(self,str,arg)
        local strArray = string.split(str,"_")
        local cmpName = strArray[1]
        local argName = strArray[2] 
        local component = self.components[cmpName]
        component[argName] = arg
    end,
}
setmetatable(base,mt)

local ExcuteFunc = function(entity,func)
    if func ~= nil then 
        func(entity)
    end 
end
local InitEntityCompoent = function(tuples)
    local components = {}
    local id = #ECSManager.entityList + 1
    if id ~= 1 then 
        id = ECSManager.entityList[id-1].id + 1
    end 
    components.id = id
    for _,cmpName in ipairs(tuples) do 
        components[cmpName] = Component:New(cmpName,id)
    end 
    ECSManager.entityList[id] = components
    return components
end


local Entity = {}

function Entity:Get(components)
    base.components = components
    base.id = components.id
    return base
end

function Entity:GetTemple(components)
	local temp = clone(base)
	temp.components = components
    temp.id = components.id or -1
    return temp
end

function Entity:Create(fileName,dataConfigEx)
    local center = G_ControlCenter.LoadGameLuaFile("Entity."..fileName)
    local components = InitEntityCompoent(center.tuples)
    local entity = Entity:GetTemple(components)
    if dataConfigEx ~= nil then 
        for key,value in pairs(dataConfigEx) do 
            center.dataConfig[key] = value
        end 
    end
    --初始化数据(默认数据)
    ExcuteFunc(entity,handler(center,center.InitData))
    --初始化组件
    ExcuteFunc(entity,handler(center,center.InitComponent))
    --初始化界面
    ExcuteFunc(entity,handler(center,center.InitView))
    --初始化游戏物体
    ExcuteFunc(entity,handler(center,center.InitObject))

    return entity
end

return Entity

--[[
local Entity_Name = {}
Entity_Name.tuples = {}
Entity_Name.dataConfig = {
}
--初始化数据(默认数据)
function Entity_Name:InitData(entity)
end
--初始化组件
function Entity_Name:InitComponent(entity)
end
--初始化额外界面
function Entity_Name:InitView(entity)
end
--初始化游戏物体
function Entity_Name:InitObject(entity)
end

return Entity_Name

--]]
