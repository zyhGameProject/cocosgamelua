local G_ControlCenter = ControlCenter
local Component = {}
--ECS_组件
Component.publicArg = {
    isGlobal = false,
    isNeedUpdate = false,
    isNeedUpdateForever = false,
}
function Component:Init(name)
    local cmp = G_ControlCenter.LoadGameLuaFile("Component."..name)
    for k,v in pairs(self.publicArg) do 
        if cmp[k] == nil then 
            cmp[k] = v
        end 
    end 
    ECSManager.copmonentList[name] = cmp
end
--获取组件通过文件名
function Component:New(name,entityId)
    local cmp = ECSManager.copmonentList[name]
    if cmp.isGlobal then 
        return cmp
    end
    cmp = self:SpliteFunc(clone(cmp),cmp)
    cmp.id = entityId
    return cmp
end

function Component:SpliteFunc(cmp,func)
    local delList = {}
    for name,arg in pairs(cmp) do 
        if type(arg) == "function" then 
            table.insert(delList,name)
        end
    end 

    if #delList > 0 then 
        for _,name in ipairs(delList) do 
            cmp[name] = nil
        end 
        setmetatable(cmp,{__index = func})
    end 
    return cmp
end

--[[ 组件模板
    local xxx = {}
    --是否为唯一组件
    xxx.isGlobal = false
    xxx.isNeedUpdate = false
    xxx.isNeedUpdateForever = false
    return xxx
]]

return Component