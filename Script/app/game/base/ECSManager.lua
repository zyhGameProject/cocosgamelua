local G_ControlCenter = ControlCenter
local G_MessageUtils = MessageUtils
local Component = require("app.game.base.Component")
local Entity = require("app.game.base.Entity")
local System = require("app.game.base.System")
local ECSManager = {}
--系统表,组件表初始化
function ECSManager:Init(componentConfig,systemConfig)
    self.copmonentList = {}
    self.systemList = {}
    self.entityList = {}

    if next(componentConfig) ~= nil then 
        for _,name in ipairs(componentConfig) do 
            Component:Init(name)
        end 
    end 
    if next(systemConfig) ~= nil then
        local system = {} 
        for _,name in ipairs(systemConfig) do 
            system = System:Init(name)
        end 
    end 
end

function ECSManager:Update(tick)
    if #self.systemList > 0 then 
        -- G_ControlCenter.StartTime("ECSManager framework")
        local tuples = {}
		local comptArray = {}
		local globalArray = {}
		local tempCompt = {}
		local comptCount = 0
        for _,system in ipairs(self.systemList) do 
            tuples = system:GetTuples()
            for id,compts in pairs(self.entityList) do 
                local tuplesCompt = {}
                tuplesCompt.id = id
				comptCount = #tuples
                for _,comptName in ipairs(tuples) do 
					tempCompt = compts[comptName]
                    if tempCompt == nil then
                        tuplesCompt = {}
                        break 
					else
						comptCount = comptCount - 1
						if tempCompt.isGlobal == false then
							tuplesCompt[comptName] = tempCompt
						else
							globalArray[comptName] = tempCompt
						end
                    end
                end 
                if comptCount == 0 then 
					table.insert(comptArray,tuplesCompt)
                end 
            end 
			--Entity:Get(compt)
			if #comptArray > 0 then 
				system:Update(comptArray,Entity:GetTemple(globalArray),tick)
				comptArray = {}
				globalArray = {}
			end 
        end 
        -- G_ControlCenter.EndTime("ECSManager framework")
        -- G_ControlCenter.PrintTime(tick)
    end 
end
--创建实体
function ECSManager:CreateEntity(fileName,dataConfigEx)
    return Entity:Create(fileName,dataConfigEx)
end

--获取实体通过实体编号
function ECSManager:GetEntityById(id)
	if id == nil then return nil end 
	local components = self.entityList[id]
	return Entity:GetTemple(components)
end
--添加实体组件
function ECSManager:AddComponent(id,name)
    local component = Component:New(name,id)
    self.entityList[id][name] = component
    return component
end
--移除实体组件
function ECSManager:RemoveComponent(entityid,name)
	local entity = self.entityList[id]
	if entity and entity[name] then 
		entity[name] = nil
	end
end
--更新公共组件数据,组件isGlobal=false and 不存在参数时无法更新
function ECSManager:UpdateGComponentData(name,key,value)
	local component = self.copmonentList[name]
	if component and component.isGlobal and component[key] then 
		local _type = type(component[key])
		if _type == "function" then 
			component[key](component)
		else
			component[key] = value
		end
	end
end
--清除数据
function ECSManager:Clear()
    self.copmonentList = {}
    self.systemList = {}
    self.entityList = {}
end

return ECSManager