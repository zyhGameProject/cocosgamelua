local G_ControlCenter = ControlCenter
local G_MessageUtils = MessageUtils

local Entity = require("app.game.base.Entity")

local base = {}

base.__index = base

--@desc 获取元组
function base:GetTuples()
end
--@desc 更新
function base:Update(entity,tick)
end

function base:GetEntity(args)
	return Entity.Get(nil,args)
end

local System = {}

--ECS_系统
function System:Init(name)
    
    local system = G_ControlCenter.LoadGameLuaFile("System."..name)

    setmetatable(system,base)

    if system.Init then 
        system:Init()
    end 
    table.insert(ECSManager.systemList,system)
    return system
end

--[[ 系统模板
    local G_GameManager = GameManager
    local system = {}
    function system:GetTuples()
        return {}
    end
    function system:Init()
    end
    function UnitSystem:Update(componentArray,tick)
		for _,components in ipairs(componentArray) do 
			local entity = self:GetEntity(components)
		end
	end
    return system
]]

return System