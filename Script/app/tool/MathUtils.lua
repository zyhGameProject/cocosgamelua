local MathUtils = {}

--获取圆等分的坐标点
--@count: 等分个数
function MathUtils.GetDivideCirclePoint(count,radius,center)
    local array = {}
    local radians = (math.pi / 180) * math.round(360.0 / count) --弧度
    local r = radius or 10
    local center = center or cc.p(0,0)

    for i=1,count,1 do 
        local pos = {}
        pos.x = center.x + r * math.sin(radians * i)
        pos.y = center.y + r * math.cos(radians * i)
        table.insert(array,pos)
    end 

    return array
end


return MathUtils