local UIUtils = {}
local G_ControlCenter = ControlCenter

UIUtils._uiInstance = {}
function UIUtils.ShowUI(filename,_ui)
	print("UIUtils.ShowUI "..filename)
	local ui = _ui == nil and G_ControlCenter.LoadUiLuaFile(filename) or _ui
	local ui_instance = ui:new()
	local ui_name = ui_instance.__cname
	if UIUtils._uiInstance[ui_name] then 
		error("instance "..ui_name.." 已存在")
	end 
	UIUtils._uiInstance[ui_name] = ui_instance
	return ui_instance
end

function UIUtils.CloseUI(_class)
	local ui_name = _class.__cname
	if UIUtils._uiInstance[ui_name] == nil then 
		return 
	end 
	UIUtils._uiInstance[ui_name]:CloseUI()
	UIUtils._uiInstance[ui_name] = nil 
end

function UIUtils.CloseAllUI()
	for _,ui_instance in pairs(UIUtils._uiInstance) do 
		ui_instance:CloseUI()
	end 
	UIUtils._uiInstance = {}
end

function UIUtils.GetNode(name,node)
	if(not node)then
		cclog("GetNode: error! self.node==nil name="..name)
		return nil
	end
	
	local retChild=node:getChildByName(name)
	
	if(retChild)then
		return retChild
	else
		local children=node:getChildren()
		local count=#children
		for i=1,count do
			local findNode=UIUtils.GetNode(name,children[i])
			if(findNode)then
				return findNode
			end
		end
	end
	
	return nil	
end

function UIUtils.GetPosition(node)
	return cc.p(node:getPositionX(),node:getPositionY())
end

function UIUtils.setString(node,text)
	if node == nil then return end 
	node:setString(text)
end

--调用对象函数
function UIUtils.CallUIObjectFun(_uiname,_funcName,...)	
	local objIns=UIUtils._uiInstance[_uiname]
	if(objIns)then
		local fun=objIns[_funcName]
		if(fun)then
			return fun(objIns,...)
		end	
	end
	error(_uiname..".".._funcName.." is nil")
end

return UIUtils
