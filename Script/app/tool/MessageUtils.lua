local MessageUtils = {}

MessageUtils.listnereArry = {}
function MessageUtils:RegisterListener(event,obj,func)
    self.listnereArry[event] = self.listnereArry[event] or {}
    local funcTab = {}
    local funArray = self.listnereArry[event]
    if funArray[func] ~= nil then 
        return funArray[func]
    end
    obj = obj or "static"
    self.listnereArry[event][obj] = func
end
--@desc 调用方法时返回true会删除监听
function MessageUtils:OnEvent(event,...)
    local funcArray = self.listnereArry[event]
    local delList = {}
    local isDel = false
    if funcArray and next(funcArray) ~= nil then 
        for obj,func in pairs(funcArray) do
            if obj == "static" then 
                isDel = func(...)
            else
                isDel = func(obj,...)
            end 
            if isDel then table.insert(delList,obj) end 
        end 
    end 

    if #delList > 0 then 
        for _,func in ipairs(delList) do 
            self.listnereArry[event][func] = nil
        end 
    end 
end
--@_func: 注册方法
--@_obj: 注册对象
function MessageUtils:RemoveListener(_func,_obj)
    _obj = _obj or "static"
    local delList = {}
    if next(self.listnereArry) ~= nil then 
        for event,array in pairs(self.listnereArry) do 
            if next(array) ~= nil then 
                for obj,func in pairs(array) do 
                    if obj == _obj then 
                        table.insert(delList,obj)
                    end 
                end 
            end 
        end 
    end 

    if #delList > 0 then 
        for _,obj in ipairs(delList) do 
            self.listnereArry[event][obj] = nil
        end 
    end 
end

function MessageUtils:ClearListener()
    self.listnereArry = {}
end

return MessageUtils