local StringUtils = {}
--解析lua表字符串
--a=1;b=
function StringUtils.ParseLuaTabStr(str)
    local tab = {}
    local strArray = string.split(str,";")
    local tabStrArray = {}

    for _,tabStr in ipairs(strArray) do 
        tabStrArray = string.split(tabStr,"=")
        if tabStrArray[2] ~= "" then 
            if string.find(tabStrArray[2],"%d") ~= nil then 
                tab[tabStrArray[1]] = tonumber(tabStrArray[2])
            else
                tab[tabStrArray[1]] = tabStrArray[2]
            end 
        end 
    end 

    return tab
end
-- 解析数字成数组
function StringUtils.ParseArrayNum(num)
    local array = {}
    if num ~= nil then 
        num = tonumber(num)
        local lastNum = 0
        while num~=0
        do 
            lastNum = num%10
            num = math.floor(num/10)
            table.insert(array,lastNum)
        end
    end 
    return array
end

return StringUtils