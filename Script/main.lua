
cc.FileUtils:getInstance():setPopupNotify(false)
--cc.FileUtils:getInstance():addSearchPath("Resources")

require "config"
require "cocos.init"
require "app.init"



local function main()
    ControlCenter.ChangeScene(ControlCenter.scData.SceneIDList.Lobby)
end

local status, msg = xpcall(main, __G__TRACKBACK__)
if not status then
    print(msg)
end
