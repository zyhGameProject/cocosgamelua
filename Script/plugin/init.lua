local Plugin = {}

Plugin.AITreePlugin = require("plugin.AITreePlugin.AITreePlugin")
Plugin.PathPlugin = require("plugin.PathPlugin.PathPlugin")

return Plugin