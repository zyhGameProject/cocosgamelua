local PathPluginHelp = {}

PathPluginHelp.isShowView = false
function PathPluginHelp.GetGridNode()
    local grid = {
        pos = cc.p(0,0),
        index = cc.p(0,0),
        isCollisible=false,--是否能够碰撞
        --多人寻路时调用
        --预测碰撞点
        timeVisibleStr = "",
    }

    return grid
end

function PathPluginHelp.SwitchToPathNode(grid)
    if grid == nil then return nil end 
    local gridExtra = {
        parent={}, --上一个节点
        g=0,--距离起点的权重
        h=0,--距离终点的权重
    }

    --总的权重
    gridExtra.GetF = function(self)
        return self.g + self.h
    end

    gridExtra.pos = grid.pos
    gridExtra.index = grid.index
    gridExtra.isCollisible = grid.isCollisible --是否可碰撞
	gridExtra.isTarget = grid.isTarget
    -- if Plugin.PathPlugin.isCleanResetMapGrid then 
    --     grid.timeVisibleStr = ""
    -- end
    -- gridExtra.timeVisibleStr = grid.timeVisibleStr

    return gridExtra
end

function PathPluginHelp.SaveColliderTime(pathNode)
    local tempNode = {}
    local node = {}
    local colliderTime = 1
    tempNode = pathNode
    while next(pathNode)
    do
        node = Plugin.PathPlugin.mapGrids[pathNode.index.x][pathNode.index.y]
        node.timeVisibleStr = node.timeVisibleStr..pathNode.g
        pathNode = pathNode.parent
    end
    pathNode = tempNode
end

function PathPluginHelp.GetGroundNode(pathNode)
    local grounds = {}
    local node = {}
    -- local timeVisibleStr = pathNode.timeVisibleStr
    -- local step = Plugin.PathPlugin.CacluteG(nil,pathNode)
    ControlCenter.StartTime("GetGroundNode")
    for x=-1,1,1 do 
        for y=-1,1,1 do 
            if math.abs(x+y) == 1 and Plugin.PathPlugin.mapGrids[pathNode.index.x+x] then
                node = PathPluginHelp.SwitchToPathNode(Plugin.PathPlugin.mapGrids[pathNode.index.x+x][pathNode.index.y+y])
                if node and node.isCollisible == false then 
                    -- and string.find(timeVisibleStr,step) == nil then
                    table.insert(grounds,node)
                end 
            end 
        end 
    end  
    ControlCenter.EndTime("GetGroundNode")
    return grounds
end

function PathPluginHelp.CreateBinartHeap()
    local bHeap = {}
    bHeap.array = {}

    bHeap.switchGrid = function(self,index_1,index_2)
        if index_1 == index_2 then return end 
        local data = self.array[index_2]
        self.array[index_2] = self.array[index_1]
        self.array[index_1] = data
    end

    bHeap.sortUp = function(self,pos)
        local last = pos
        local half = 0
        while last > 1 do
            half = math.floor(last/2)
            if self.array[last]:GetF() > self.array[half]:GetF()
                or (self.array[last]:GetF() == self.array[half]:GetF() 
                and self.array[last].h > self.array[half].h) then 
                break
            end 
            self.switchGrid(self,last,half)
            if last == 1 then 
                break
            end 
            last = half
        end
    end

    bHeap.push = function(self,grid)
        table.insert(self.array,grid)
        self:sortUp(#self.array)
    end

    bHeap.sortDown = function(self,pos)
        local lef,rig,next,minValue = 0,0,0
        while(pos*2 <= #self.array) 
        do
            lef = pos*2
            rig = lef + 1
            if self.array[lef] then 
                next = lef
                minValue = self.array[lef]:GetF()
            end 

            if self.array[rig] and self.array[rig]:GetF() < minValue then 
                next = rig
                minValue = self.array[rig]:GetF()
            end 
            if self.array[pos]:GetF() < minValue
                or (self.array[pos]:GetF() == minValue 
                and self.array[pos].h < self.array[next].h) then  
                break
            end 
            self.switchGrid(self,pos,next)
            pos = next
        end
    end

    bHeap.pop = function(self,index)
        local grid = self.array[1]
        self.switchGrid(self,1,#self.array)  
        table.remove( self.array, #self.array)
        self:sortDown(1)
        return grid
    end

    bHeap.Exists = function(self,node)
        for index,_node in pairs(self.array) do 
            if node.index.x == _node.index.x and node.index.y == _node.index.y then 
                return index
            end
        end 
        return nil
    end

    bHeap.getLength = function(self)
        return #bHeap.array
    end

    return bHeap
end

function PathPluginHelp.insertOpenNode(x,y)
    if PathPluginHelp.isShowView then 
        UIUtils.CallUIObjectFun("PathEditorUI","insertOpenNode",cc.p(x,y))
    end 
end

return PathPluginHelp 