local G_PathPluginHelp = require("plugin.PathPlugin.PathPluginHelp")
local PathPlugin = {}
--方块单位长宽
PathPlugin.gridSize = 16
PathPlugin.isCleanResetMapGrid = false --是否清理预测碰撞点

--@desc 起点(8,8) 终点(960-8,640-8)
-- 向上取整 math.ceil()
function PathPlugin.LoadPathFindMap(mapGrids)
    if mapGrids == nil then 
        mapGrids = {}
        local pixel = cc.size(Plugin.PathPlugin.gridSize,Plugin.PathPlugin.gridSize)
        local mapSize = cc.size(
            display.width/pixel.width,
            display.height/pixel.height
        )
        for x=1,mapSize.width,1 do 
            mapGrids[x] = {}
            for y=1,mapSize.height,1 do 
                mapGrids[x][y] = G_PathPluginHelp.GetGridNode()
                mapGrids[x][y].pos = Plugin.PathPlugin.SwitchIndexToCenter(x,y)
                mapGrids[x][y].index = cc.p(x,y)
            end 
        end 
    end 

    Plugin.PathPlugin.mapGrids = mapGrids
end
--离起点的距离
function PathPlugin.CacluteG(_self,parent)
    return parent.g + 1
end
--离终点的距离
function PathPlugin.CacluteH(_self,target)
    return (math.abs(_self.x-target.x) + math.abs(_self.y-target.y) ) * 1.2
end
--转换为数组坐标
function PathPlugin.SwitchPosToIndex(x,y)
    return cc.p(math.ceil(x/PathPlugin.gridSize),math.ceil(y/PathPlugin.gridSize))
end
--真实坐标转为中心点坐标
function PathPlugin.SwitchPosToCenter(x,y)
    local indexPos = PathPlugin.SwitchPosToIndex(x,y)
    return PathPlugin.SwitchIndexToCenter(indexPos.x,indexPos.y)
end
--从数组坐标转换为格子中心坐标
function PathPlugin.SwitchIndexToCenter(x,y)
    return cc.p(8+PathPlugin.gridSize*(x-1),8+PathPlugin.gridSize*(y-1))
end
--重置地图
function PathPlugin.ResetMapGrid()
    Plugin.PathPlugin.isCleanResetMapGrid = true
end
--A*寻路
function PathPlugin.GetPath(pos_s,tabPosE)
    local startTime = os.clock()
    local isFind = false
    local grid_s = Plugin.PathPlugin.SwitchPosToIndex(pos_s.x,pos_s.y)
	for _,pos_e in ipairs(tabPosE) do 
		local grid_e = Plugin.PathPlugin.SwitchPosToIndex(pos_e.x,pos_e.y)
		Plugin.PathPlugin.SetTargetNode(grid_e,true)
	end
    local pathNode = G_PathPluginHelp.SwitchToPathNode(Plugin.PathPlugin.mapGrids[grid_s.x][grid_s.y])
    local openlist = G_PathPluginHelp.CreateBinartHeap()
    local closelist = {}
    local closeG,openG,pathG = 0,0,0
    local maxCount = 0
    openlist:push(pathNode)
    while(openlist:getLength() > 0 )
    do 
        ControlCenter.StartTime("openlistPopTime")
        pathNode = openlist:pop()
        ControlCenter.EndTime("openlistPopTime")
        G_PathPluginHelp.insertOpenNode(pathNode.pos.x,pathNode.pos.y)
        if Plugin.PathPlugin.IsArriveToTarget(pathNode) or maxCount > 8800 then
            ControlCenter.PrintTime() 
            -- G_PathPluginHelp.SaveColliderTime(pathNode.parent)
            -- Plugin.PathPlugin.isCleanResetMapGrid = false
            break
        end 
        closelist[pathNode.index.x.."_"..pathNode.index.y] = pathNode.g
        
        for _,node in pairs(G_PathPluginHelp.GetGroundNode(pathNode,closelist)) do 
            --如果不存在在关闭列表
            ControlCenter.StartTime("otherCost")
            local openIndex = openlist:Exists(node) 
            openG = openIndex and openlist.array[openIndex].g or 0
            closeG = closelist[node.index.x.."_"..node.index.y]
            pathG = Plugin.PathPlugin.CacluteG(node,pathNode)
            ControlCenter.EndTime("otherCost")
            if closeG == nil then 
                if openG ~= 0 and pathG < openG then 
                    ControlCenter.StartTime("resortOpenlist")
                    openlist.array[openIndex].g = pathG
                    openlist.array[openIndex].parent = pathNode
                    openlist:sortUp(openIndex)
                    ControlCenter.EndTime("resortOpenlist")
                -- elseif closeG and closeG ~= 0 and pathG < closeG then 
                --     closelist[node.index.x.."_"..node.index.y] = nil
                --     node.parent = pathNode
                --     node.h = Plugin.PathPlugin.CacluteH(node.index,grid_e)
                --     node.g = pathG
                --     openlist:push(node)
                else
                    ControlCenter.StartTime("openlistPushTime")
                    node.parent = pathNode
                    node.h = Plugin.PathPlugin.CacluteH(node.index,grid_e)
                    node.g = pathG
					if Plugin.PathPlugin.IsArriveToTarget(node) then 
						pathNode = node
						break
					end
                    openlist:push(node)
                    ControlCenter.EndTime("openlistPushTime")
                end 
            end 
        end 
        maxCount = maxCount + 1
    end
	
	for _,pos_e in ipairs(tabPosE) do 
		local grid_e = Plugin.PathPlugin.SwitchPosToIndex(pos_e.x,pos_e.y)
		Plugin.PathPlugin.SetTargetNode(grid_e,false)
	end
    ControlCenter.PrintTime()
    -- Plugin.PathPlugin.isCleanResetMapGrid = false
    return pathNode
end

function PathPlugin.IsArriveToTarget(node)
	local isTarget = Plugin.PathPlugin.mapGrids[node.x][node.y].isTarget
	return isTarget == nil and false or isTarget
end

function PathPlugin.SetTargetNode(node,isOpen)
	isOpen = isOpen or true
	if isOpen then 
		Plugin.PathPlugin.mapGrids[node.x][node.y].isTarget = true
	else
		Plugin.PathPlugin.mapGrids[node.x][node.y].isTarget = nil
	end
	
end

return PathPlugin