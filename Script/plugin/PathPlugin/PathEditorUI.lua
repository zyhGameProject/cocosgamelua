local G_Math = math
local scheduler = cc.Director:getInstance():getScheduler()

local G_PathPlugin = Plugin.PathPlugin
local G_PathPluginHelp = require("plugin.PathPlugin.PathPluginHelp")

local G_UIUtils = UIUtils
local G_UIBase = UIBase

local PathEditorUI = class("PathEditorUI",G_UIBase)
PathEditorUI.path = "csb/Plugin/PathEditorUI.csb"

PathEditorUI.ENUM_DRAW_TYPE = {
    Obstacle = 1,
    Start = 2,
    End = 3,
    Path = 4,
    BackGround = 5,
}

PathEditorUI.ENUM_DRAW_C4B= {
    cc.c4b(1,1,1,1),
    cc.c4b(254,1,254,1),
    cc.c4b(1,254,1,1),
    cc.c4b(1,125,254,1),
    cc.c4b(30,30,30,0.1)
}

function PathEditorUI:Awake()
    if G_PathPlugin.mapGrids == nil then 
        G_PathPlugin.LoadPathFindMap()
    end 
    --当前绘制类型
    self.drawType = PathEditorUI.ENUM_DRAW_TYPE.Obstacle
    --类型点击的节点
    self.pointList = {}
    --是否需要清空节点
    self.isClickFindPath = false
    --openlist
    self.openList = {}
end

function PathEditorUI:BindNodeToLocal()
    self.drawBtnList = { 1,1,1,1,1 }
    for name,index in pairs(PathEditorUI.ENUM_DRAW_TYPE) do 
        self.drawBtnList[index] = G_UIUtils.GetNode("Btn_"..name,self.rootNode)
    end 
end

function PathEditorUI:InitDataNode()

    local _instance = self
    local enumDrawType = PathEditorUI.ENUM_DRAW_TYPE
    local enumDrawColor = PathEditorUI.ENUM_DRAW_C4B
    local color = enumDrawColor[self.drawType]
    
    local function onTouchBegan(touch, event)
        return true
    end
    local function onTouchEnded(touch, event)
        -- 获取点击位置
        
        local drawNode = _instance:GetDrawNode(self.drawType)
        local pos = touch:getLocation()
        local index = G_PathPlugin.SwitchPosToIndex(pos.x,pos.y)
        local center = G_PathPlugin.SwitchIndexToCenter(index.x,index.y)
        local isDraw = true

        color = enumDrawColor[self.drawType]
        self.pointList[self.drawType] = self.pointList[self.drawType] or {}
        if self.drawType == enumDrawType.Obstacle then 
            self.pointList[self.drawType][index.x.."_"..index.y] = true
        elseif self.drawType == enumDrawType.End then 
            if self.pointList[enumDrawType.Start] and #self.pointList[enumDrawType.Start] > 0 then 
                if #self.pointList[self.drawType] == #self.pointList[enumDrawType.Start] then 
                    return
                end 
                table.insert( self.pointList[self.drawType],center )
            else
                return
            end 
        else
            table.insert( self.pointList[self.drawType],center )
        end 
        
        drawNode:drawPoint(center,12,color)
    end
    local listener = cc.EventListenerTouchOneByOne:create()
    -- 注册两个回调监听方法
    listener:registerScriptHandler(onTouchBegan,cc.Handler.EVENT_TOUCH_BEGAN )
    listener:registerScriptHandler(onTouchEnded,cc.Handler.EVENT_TOUCH_ENDED )
    local layer = cc.Layer:create()
    local eventDispatcher = layer:getEventDispatcher()-- 事件派发器
    -- 绑定触摸事件到层当中
    eventDispatcher:addEventListenerWithSceneGraphPriority(listener, layer)
    self.bindNodes["KW_GRP_Touch"]:addChild(layer)

    local drawNode = self:GetDrawNode(enumDrawType.BackGround)
    for x,data in ipairs(G_PathPlugin.mapGrids) do 
        for y,grid in ipairs(data) do 
            drawNode:drawLine(cc.p(grid.index.x*16,0),cc.p(grid.index.x*16,display.height),cc.c4b(0,1,72,0.1))
            drawNode:drawLine(cc.p(0,grid.index.x*16),cc.p(display.width,grid.index.x*16),cc.c4b(0,1,72,0.1))
            if grid.isCollisible then 
                drawNode:drawPoint(grid.pos,12,color)
            end 
        end 
    end 

    self:selectBtn(self.drawType)
end

function PathEditorUI:Start()
    G_PathPluginHelp.isShowView = true
end

--@region 回调事件绑定
--设置障碍
function PathEditorUI:onBtnSetObstacle(sender)
    self:selectBtn(PathEditorUI.ENUM_DRAW_TYPE.Obstacle)
end

function PathEditorUI:onBtnSetStart(sender)
    self:selectBtn(PathEditorUI.ENUM_DRAW_TYPE.Start)
end

function PathEditorUI:onBtnSetEnd(sender)
    self:selectBtn(PathEditorUI.ENUM_DRAW_TYPE.End)
end

function PathEditorUI:onBtnClear(sender)
    local enumDrawType = PathEditorUI.ENUM_DRAW_TYPE
    local drawNode = {}
    for name,index in pairs(enumDrawType) do 
        drawNode = self:GetDrawNode(index)
        if (index == enumDrawType.Obstacle 
            and self.drawType == enumDrawType.Obstacle) 
            or index ~= enumDrawType.Obstacle 
            and index ~= enumDrawType.BackGround then 

            drawNode:clear()
            self.pointList[index] = {}
        end 
    end 
end

function PathEditorUI:onBtnFindPath(sender)
    local enumDrawType = PathEditorUI.ENUM_DRAW_TYPE
    local startList = self.pointList[enumDrawType.Start] or {}
    local endList = self.pointList[enumDrawType.End] or {}
    local pathDrawNode = self:GetDrawNode(enumDrawType.Path)
    if next(startList) == nil or next(endList) == nil then 
        print("[LOG] 要寻路的节点其中一方不能为空")
        return 
    end 
    local pathNode = {}
    local endPos = {}
    local drawType = self.drawType
    pathDrawNode:clear()
    G_PathPlugin.ResetMapGrid() --重置地图碰撞点
    self.bindNodes["KW_GRP_Touch"]:setTouchEnabled(false)
    self.drawType = PathEditorUI.ENUM_DRAW_TYPE.Path
    self:updateCloseNode(true)
    for _,startPos in ipairs(startList) do 
        for _,endPos in ipairs(endList) do
            pathNode = G_PathPlugin.GetPath(startPos,endPos)
            if pathNode == nil then 
                print("[ERROR] 路径不存在。。。。。。")
            else
                self:DrawPath(pathNode)
            end 
            break
        end
        if #endList > 1 then table.remove(endList,1) end 
    end
    self:updateCloseNode(false)
    self.drawType = drawType
end

function PathEditorUI:onBtnBack(sender)
    G_UIUtils.CloseUI(self)
end
--@endregion

function PathEditorUI:updateCloseNode(isCollisible)
    local enumDrawType = PathEditorUI.ENUM_DRAW_TYPE
    local indexGrp = {}
    for index,indexArray in pairs(self.pointList) do 
        if index == enumDrawType.Obstacle and next(indexArray) then 
            for indexStr,_ in pairs(indexArray) do  
                indexGrp = string.split(indexStr,"_")
                local x = tonumber(indexGrp[1])
                local y = tonumber(indexGrp[2])
                G_PathPlugin.mapGrids[x][y].isCollisible = isCollisible
            end
        end 
    end 
end

function PathEditorUI:insertOpenNode(pos)
    local data = {}
    local enumDrawType = PathEditorUI.ENUM_DRAW_TYPE
    local drawNode = self:GetDrawNode(enumDrawType.Path)
    data.pos = pos
    data.color = cc.c4b(255-#self.openList*4,255-#self.openList*4,#self.openList*4,1)
    drawNode:drawPoint(data.pos,12,data.color)
    table.insert(self.openList,data)
end

function PathEditorUI:selectBtn(type)
    local isSelect = false
    for index,btn in pairs(self.drawBtnList) do 
        isSelect = index == type
        btn:setEnabled(not isSelect)
        btn:setBright(not isSelect)
    end 

    self.drawType = type
end

function PathEditorUI:GetDrawNode(type)
    local enumDrawType = PathEditorUI.ENUM_DRAW_TYPE
    local drawNode = ""
    local nodeName = ""
    for name,index in pairs(enumDrawType) do 
        if index == type then 
            nodeName = "Drw_"..name
            break
        end 
    end 

    drawNode = self:CreateDrawNode(nodeName)
    return drawNode
end

function PathEditorUI:CreateDrawNode(nodeName)
    local drawList = self.bindNodes["KW_NOD_DrawList"]
    local node = G_UIUtils.GetNode(nodeName,drawList)
    if node == nil then 
        node = cc.DrawNode:create()
        node:setName(nodeName)
        drawList:addChild(node)
    end 
    return node
end

function PathEditorUI:DrawPath(pathNode)
    local _instance = self
    local enumDrawType = _instance.ENUM_DRAW_TYPE
    local enumDrawColor = PathEditorUI.ENUM_DRAW_C4B
    local color = enumDrawColor[enumDrawType.Path]
    local drawNode = _instance:GetDrawNode(enumDrawType.Path)

    local countList = {}
    for _,node in pairs(self.openList) do 
        countList[node.pos.x.."_"..node.pos.y] = countList[node.pos.x.."_"..node.pos.y] or 0
        countList[node.pos.x.."_"..node.pos.y] = countList[node.pos.x.."_"..node.pos.y] + 1
    end 
    for str,count in pairs(countList) do 
        if count > 1 then 
            print(str.."    "..count)
        end 
    end 
    self.openList = {}
    local pathAniListener = {}
    pathAniListener = scheduler:scheduleScriptFunc(function()
        if next(pathNode) == nil then 
            _instance.bindNodes["KW_GRP_Touch"]:setTouchEnabled(true)
            scheduler:unscheduleScriptEntry(pathAniListener)
            return
        end 
        drawNode:drawPoint(pathNode.pos,12,color)
        pathNode = pathNode.parent
    end, 0.05, false)
end

return PathEditorUI