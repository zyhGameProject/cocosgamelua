local AITreePluginHelp = {}

AITreePluginHelp.TYPE = {
    Selector = 1,
    Sequence = 2,
    Parallel = 3,
    Action = 4,
    Condition = 5,
    Decorator = 6,
}

AITreePluginHelp.DECORATOR_TYPE = {
    Repeat = 1,
}


AITreePluginHelp.TabTreeTemp = {
    name = "Selector_Root",
    {
        name = "Selector_Action",
        { 
            name = "Sequence_Sleep",
            { name = "Condition_IsToSleep"},
            {
                name = "Selector_Sleep",
                { name = "Action_MoveToTarget"},--1.移动中 2.抵达目标点
                { name = "Action_Sleep"}
            },
        },
        { 
            name = "Sequence_Eat",
            { name = "Condition_IsToEat"},
            {
                name = "Selector_Eat",
                { name = "Action_MoveToTarget"},
                { name = "Action_Eat"}
            },
        },
        {
            name = "Sequence_Work",
            { name = "Condition_IsToWork"},
            {
                name = "Selector_Work",
                { name = "Action_MoveToTarget"},
                { name = "Action_Work"}
            },
        },
        { name = "Action_Idel"},
    }
}
--解析树表
function AITreePluginHelp.parseTabTree(tabTree,parent)
    local strArray = string.split(tabTree.name,"_")
    local _type = AITreePluginHelp.TYPE[strArray[1]]
    local name = strArray[2]
    local node = AITreePluginHelp.GetNode(_type,name)
    local child = nil 

    if tabTree[1] == nil then return node end 
    if parent == nil then parent = node end 
    for _,_tabTree in ipairs(tabTree) do 
        child = AITreePluginHelp.parseTabTree(_tabTree,node)
        table.insert( node.childs,child)
    end 

    return node
end

function AITreePluginHelp.GetNode(_type,name)
    local node = {}
    node.type = _type
    node.name = name -- 节点名字

    if _type == AITreePluginHelp.TYPE.Selector then 
        node.childs = {}
        node.index = 0 --正在执行的索引
    elseif _type == AITreePluginHelp.TYPE.Sequence then 
        node.childs = {}
        node.index = 0 --正在执行的索引
    elseif _type == AITreePluginHelp.TYPE.Action then 
        node.func = {}
    elseif _type == AITreePluginHelp.TYPE.Condition then 
        node.func = {}
    elseif _type == AITreePluginHelp.TYPE.Decorator then 
        node.func = {}
        node.data = {} --临时数据存储的地方 返回结果时销毁
        node.childs = {} --只存在一个子节点
        node.dType = AITreePluginHelp.DECORATOR_TYPE[name]
    end 

    return node
end

function AITreePluginHelp.ExcuteSelectorNode(node)
    if node.childs[1] == nil then
        return Plugin.AITreePlugin.STATE.FAILUER
    end 
    local ret = false 
    local len = #node.childs
    local lastIndex = node.index == 0 and 0 or node.index-1
    local index = 0
    for i=1,len,1 do
        index = math.fmod(i+lastIndex,len)
        index = index == 0 and len or index
        ret = Plugin.AITreePlugin.ExcuteNode(node.childs[index])
        if ret == Plugin.AITreePlugin.STATE.SUCCESS then
            node.index = 0
            return Plugin.AITreePlugin.STATE.SUCCESS
        elseif ret == Plugin.AITreePlugin.STATE.RUNNING then 
            node.index = index
            return Plugin.AITreePlugin.STATE.RUNNING
        end 
    end 

    return Plugin.AITreePlugin.STATE.FAILUER
end

function AITreePluginHelp.ExcuteSequenceNode(node)
    if node.childs[1] == nil then 
        error("SequenceNode "..node.name.." is Null")
    end 
    local ret = false 
    local index = 0
    for _,_node in ipairs(node.childs) do 
        ret = Plugin.AITreePlugin.ExcuteNode(_node)
        if ret == Plugin.AITreePlugin.STATE.SUCCESS then 
            index = index + 1
        elseif ret == Plugin.AITreePlugin.STATE.FAILUER then 
            return Plugin.AITreePlugin.STATE.FAILUER
        elseif ret == Plugin.AITreePlugin.STATE.RUNNING then 
            return Plugin.AITreePlugin.STATE.RUNNING
        end 
    end 

    return Plugin.AITreePlugin.STATE.SUCCESS
end 

function AITreePluginHelp.ExcuteActionNode(node)
    return AITreePluginHelp.GetFunc(node.name,node.type)()
end

function AITreePluginHelp.ExcuteConditionNode(node)
    return AITreePluginHelp.GetFunc(node.name,node.type)()
end

function AITreePluginHelp.ExcuteDecoratorNode(node)
    -- local childNode = node.childs[1]
    -- local ret = false
    -- if node.dType == AITreePluginHelp.DECORATOR_TYPE.Repeat then 
    --     while ret do
    --         ret = Plugin.AITreePlugin.ExcuteNode(childNode)
    --     end
    -- end 
    return Plugin.AITreePlugin.STATE.FAILUER
end

function AITreePluginHelp.GetFunc(name,typeIndex)
    local func = 0
    local strFunc = ""
    for type,index in pairs(AITreePluginHelp.TYPE) do 
        if typeIndex == index then 
            strFunc = string.format("%s_%s",type,name)
            break
        end 
    end 
    func = Plugin.AITreePlugin.TabFunc[strFunc]
    if func == 0 then 
        error("AITreePluginHelp.GetFunc: func is nil")
    end 
    return handler(Plugin.AITreePlugin.FuncSelf,func)
end 

return AITreePluginHelp