
local G_AITreePluginHelp = require("plugin.AITreePlugin.AITreePluginHelp")

local AITreePlugin = {}

AITreePlugin.STATE = {
    SUCCESS = 1, --数据执行成功后返回
    FAILUER = 2, --数据执行失败后返回
    RUNNING = 3, --需要长时间执行的动作,例如:移动到目标点
}


--@desc 导入行为树表
function AITreePlugin.ParseTabTree(tabTree)
    local node = {} 
    tabTree = tabTree or G_AITreePluginHelp.TabTreeTemp
    node = G_AITreePluginHelp.parseTabTree(tabTree,node)
    return node
end
--@desc 执行行为树
function AITreePlugin.ExcuteNode(node)
    if node.type == G_AITreePluginHelp.TYPE.Selector then 
        return G_AITreePluginHelp.ExcuteSelectorNode(node)
    elseif node.type == G_AITreePluginHelp.TYPE.Sequence then 
        return G_AITreePluginHelp.ExcuteSequenceNode(node)
    elseif node.type == G_AITreePluginHelp.TYPE.Action then 
        return G_AITreePluginHelp.ExcuteActionNode(node)
    elseif node.type == G_AITreePluginHelp.TYPE.Condition then 
        return G_AITreePluginHelp.ExcuteConditionNode(node)
    elseif node.type == G_AITreePluginHelp.TYPE.Decorator then 
        return G_AITreePluginHelp.ExcuteDecoratorNode(node)
    end 
end
--@desc 导入行为树叶子的方法
function AITreePlugin.LoadTabFunc(tabFunc)
    Plugin.AITreePlugin.TabFunc = tabFunc 
end
--@desc 设置行为树叶子的调用对象
function AITreePlugin.SetFuncSelf(funcSelf)
    Plugin.AITreePlugin.FuncSelf = funcSelf
end


return AITreePlugin